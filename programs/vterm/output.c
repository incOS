/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "output.h"

#include <kernel.h>
#include <string.h>

static short *vidmem = 0;
static uint32_t x = 0;
static uint32_t y = 0;
static uint16_t color = 0x0700;

void outInit(void)
{
	// VGA memory
	uintptr_t paddr = 0xB8000;
	keAllocMemory(SYSCALL_MEM_PHYSICAL, &paddr, (uintptr_t*)&vidmem, 4);
	memset(vidmem, 0, 25 * 80 * 2);
}

void outScroll(uint32_t lines)
{
	if (lines > 25) lines = 25;
	if (lines == 0) return;
	memmove(vidmem, vidmem + lines * 80, (25 - lines) * 80 * 2);
	memset(vidmem + (25 - lines) * 80, 0, lines * 80 * 2);
	if (y >= lines)
		y-= lines;
	else
		y = 0;
}

void outPrint(char *data, uint32_t size)
{
	uint32_t i;
	for (i = 0; i < size; i++)
	{
		switch (data[i])
		{
			case '\n':
				x = 0;
				y++;
				if (y == 25) outScroll(1);
				break;
			case '\t':
				x = ((x + 8) / 8) * 8;
				if (x >= 80)
				{
					x = 0;
					y++;
					if (y == 25) outScroll(1);
				}
				break;
			default:
				vidmem[x + y * 80] = color | data[i];
				x++;
				if (x >= 80)
				{
					x = 0;
					y++;
					if (y == 25) outScroll(1);
				}
				break;
		}
	}
}

