/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <kernel.h>
#include <init.h>
#include <rpc.h>
#include <string.h>

#include "output.h"

static uint32_t devOpen(uint32_t caller, void *data, uint32_t size)
{
	keDebug("devOpen.\n");
	return 0;
}
static uint32_t devClose(uint32_t caller, void *data, uint32_t size)
{
	keDebug("devClose.\n");
	return 0;
}
static uint32_t devRead(uint32_t caller, void *data, uint32_t size)
{
	keDebug("devRead.\n");
	return 0;
}
static uint32_t devWrite(uint32_t caller, void *data, uint32_t size)
{
	if (size < 12) return -1;
	uint32_t datalength = ((uint32_t*)data)[2];
	if (size < 12 + datalength) return -1;
	outPrint((char*)&((uint32_t*)data)[3], datalength);
	keDebug("devWrite.\n");
	return 0;
}
static uint32_t devIOCTL(uint32_t caller, void *data, uint32_t size)
{
	keDebug("devIOCTL.\n");
	return -1;
}

void vtermRPCHandler(void)
{
	rpcHandlerSetActive(1);
	while (1) keWaitForRPC(0xFFFFFFFF);
}

int _start(void)
{
	outInit();
	
	// Initialize RPC handlers
	rpcInit();
	rpcSetHandler(RPC_DEVFS_OPEN, devOpen);
	rpcSetHandler(RPC_DEVFS_CLOSE, devClose);
	rpcSetHandler(RPC_DEVFS_READ, devRead);
	rpcSetHandler(RPC_DEVFS_WRITE, devWrite);
	rpcSetHandler(RPC_DEVFS_IOCTL, devIOCTL);
	
	// Create device files
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	
	rpcdata[0] = 0;
	strcpy((char*)&rpcdata[1], "vterm");
	rpcSend(INIT_PID, RPC_INIT_DEVFS_CREATE, rpcdata, 260, &retval);
	
	rpcdata[0] = 1;
	strcpy((char*)&rpcdata[1], "vtinput");
	rpcSend(INIT_PID, RPC_INIT_DEVFS_CREATE, rpcdata, 260, &retval);
	
	rpcdata[0] = 2;
	strcpy((char*)&rpcdata[1], "tty0");
	rpcSend(INIT_PID, RPC_INIT_DEVFS_CREATE, rpcdata, 260, &retval);
	
	keFreeMemory(rpcdata, 1);
	
	keCreateThread(vtermRPCHandler);
	rpcHandlerSetActive(1);
	initSendInitialized();
	while (1) keWaitForRPC(0xFFFFFFFF);
	
	keExit();
	return 0;
}

