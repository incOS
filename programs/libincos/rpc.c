/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "rpc.h"
#include "kernel.h"

RPCHandler handlers[256];

static void rpcHandler(uint32_t caller, uint32_t param, void *data, uint32_t size)
{
	if ((param >= 256) || (handlers[param] == 0))
	{
		keDebug("RPC: No handler present.\n");
		keExitRPC(0);
	}
	uint32_t retval = handlers[param](caller, data, size);
	keFreeMemory(data, (size + 4095) / 0x1000);
	keExitRPC(retval);
}

void rpcInit(void)
{
	uint32_t i;
	for (i = 0; i < 256; i++)
	{
		handlers[i] = 0;
	}
}
void rpcSetHandler(uint8_t index, RPCHandler entry)
{
	handlers[index] = entry;
}
void rpcHandlerSetActive(uint32_t active)
{
	if (active)
	{
		keRegisterRPCHandler(rpcHandler);
	}
	else
	{
		keRegisterRPCHandler(0);
	}
}

uint32_t rpcSend(uint32_t process, uint8_t index, void *data, uint32_t size, uint32_t *retvalue)
{
	return keSendRPC(process, 0xFFFFFFFF, index, data, size, retvalue);
}

