/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <list.h>
#include <stdlib.h>

struct list_entry
{
	struct list_entry *next;
	void *data;
};

struct list_implementation
{
	struct list_entry *first;
	unsigned int entry_count;
};

list_t list_create(void)
{
	list_t list = malloc(sizeof(struct list_implementation));
	list->first = 0;
	list->entry_count = 0;
	return list;
}

void list_destroy(list_t list)
{
	while (list->first)
	{
		struct list_entry *entry = list->first;
		list->first = list->first->next;
		free(entry);
	}
	free(list);
}

list_t list_push(list_t list, void *value)
{
	struct list_entry *newentry = malloc(sizeof(struct list_entry));
	newentry->next = list->first;
	newentry->data = value;
	list->first = newentry;
	list->entry_count++;
	return list;
}

void *list_pop(list_t list)
{
	if (list->entry_count == 0) return 0;
	struct list_entry *entry = list->first;
	void *data = entry->data;
	list->first = list->first->next;
	free(entry);
	list->entry_count--;
	return data;
}

size_t list_empty(list_t list)
{
	return (list->entry_count == 0);
}

void *list_get(list_t list, size_t index)
{
	struct list_entry *entry = list->first;
	if (!entry) return 0;
	uint32_t i;
	for (i = 0; i < index; i++)
	{
		entry = entry->next;
		if (!entry) return 0;
	}
	return entry->data;
}

list_t list_insert(list_t list, size_t index, void *value)
{
	if (index == 0) return list_push(list, value);
	struct list_entry *entry = list->first;
	if (!entry) return 0;
	index--;
	uint32_t i;
	for (i = 0; i < index; i++)
	{
		entry = entry->next;
		if (!entry) return 0;
	}
	
	struct list_entry *newentry = malloc(sizeof(struct list_entry));
	newentry->next = entry->next;
	newentry->data = value;
	entry->next = newentry;
	
	list->entry_count++;
	return list;
}

void *list_remove(list_t list, size_t index)
{
	if (index == 0) return list_pop(list);
	// Find entry before entry to remove
	struct list_entry *entry = list->first;
	if (!entry) return 0;
	index--;
	uint32_t i;
	for (i = 0; i < index; i++)
	{
		entry = entry->next;
		if (!entry) return 0;
	}
	if (entry->next == 0) return 0;
	// Delete entry
	struct list_entry *tobedeleted = entry->next;
	entry->next = entry->next->next;
	void *data = tobedeleted->data;
	free(tobedeleted);
	list->entry_count--;
	return data;
}

size_t list_size(list_t list)
{
	return list->entry_count;
}

