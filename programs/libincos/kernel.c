/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <kernel.h>

void keSleep(uint32_t msecs)
{
	asm volatile("int $0x30" : : "a"(6), "b"(msecs));
}

void keExit(void)
{
	asm volatile("int $0x30" : : "a"(0));
}

int keCreateThread(void *entry)
{
	int result;
	asm volatile("int $0x30" : "=b"(result) : "a"(1), "b"(entry));
	return result;
}
void keStopThread(void)
{
	asm volatile("int $0x30" : : "a"(2));
}

uint32_t keCreateProcess(void)
{
	uint32_t result;
	asm volatile("int $0x30" : "=b"(result) : "a"(3));
	return result;
}
uint32_t keCloneProcess(uint32_t process, uint32_t thread, uintptr_t entry)
{
	uint32_t result;
	asm volatile("int $0x30" : "=b"(result) : "a"(16), "b"(process), "c"(thread), "d"(entry));
	return result;
}
uint32_t keMapProcessMemory(uint32_t process, uintptr_t srcaddr,
	uintptr_t targetaddr, uint32_t pagecount)
{
	uint32_t result;
	asm volatile("int $0x30" : "=b"(result) : "a"(4), "b"(process), "c"(srcaddr), "d"(targetaddr), "S"(pagecount));
	return result;
}
uint32_t keStartProcess(uint32_t process, uintptr_t entry)
{
	uint32_t result;
	asm volatile("int $0x30" : "=b"(result) : "a"(5), "b"(process), "c"(entry));
	return result;
}
void keGetProcessInfo(uint32_t *process, uint32_t *thread)
{
	uint32_t result;
	uint32_t result2;
	asm volatile("int $0x30" : "=b"(result), "=c"(result2) : "a"(17));
	if (process) *process = result;
	if (thread) *thread = result2;
}

void keRegisterRPCHandler(void *entry)
{
	asm volatile("int $0x30" : : "a"(14), "b"(entry));
}

void keWaitForRPC(uint32_t timeout)
{
	asm volatile("int $0x30" : : "a"(7), "b"(timeout));
}
uint32_t keSendRPC(uint32_t process, uint32_t timeout, uint32_t param, void *data,
	uint32_t datalength, uint32_t *returnval)
{
	uint32_t result;
	uint32_t result2;
	asm volatile("int $0x30" : "=b"(result), "=c"(result2) : "a"(8), "b"(process), "c"(timeout), "d"(param), "S"(data), "D"(datalength));
	if (returnval) *returnval = result2;
	return result;
}
void keExitRPC(uint32_t returnval)
{
	asm volatile("int $0x30" : : "a"(9), "b"(returnval));
}
void keExitIRQ(void)
{
	asm volatile("int $0x30" : : "a"(15));
}

uint32_t keRegisterIRQHandler(uint32_t irq, void *entry)
{
	uint32_t result;
	asm volatile("int $0x30" : "=b"(result) : "a"(13), "b"(irq), "c"(entry));
	return result;
}

uint32_t keAllocMemory(uint32_t flags, uintptr_t *paddr, uintptr_t *vaddr, uint32_t pagecount)
{
	uint32_t result;
	uint32_t vaddr2 = 0;
	if (vaddr) vaddr2 = *vaddr;
	uint32_t paddr2 = 0;
	if (paddr) paddr2 = *paddr;
	asm volatile("int $0x30" : "=b"(result), "=c"(paddr2), "=d"(vaddr2) : "a"(10), "b"(flags), "c"(paddr2), "d"(vaddr2), "S"(pagecount));
	if (vaddr) *vaddr = vaddr2;
	if (paddr) *paddr = paddr2;
	return result;
}
uint32_t keFreeMemory(void *vaddr, uint32_t pagecount)
{
	uint32_t result;
	asm volatile("int $0x30" : "=b"(result) : "a"(11), "b"(vaddr), "c"(pagecount));
	return result;
}

void keDebug(const char *str)
{
	asm volatile("int $0x30" : : "a"(12), "b"(str));
}

