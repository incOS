/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <init.h>
#include <rpc.h>
#include <string.h>
#include <kernel.h>

pid_t initGetPID(void)
{
	pid_t pid = 0;
	rpcSend(INIT_PID, RPC_INIT_GET_PID, 0, 0, (uint32_t*)&pid);
	return pid;
}

void initDebug(const char *str)
{
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	strcpy((char*)rpcdata, str);
	rpcSend(INIT_PID, RPC_INIT_DEBUG, rpcdata, strlen(str) + 1, 0);
	keFreeMemory(rpcdata, 1);
}

void initSendInitialized(void)
{
	rpcSend(INIT_PID, RPC_INIT_INITIALIZED, 0, 0, 0);
}

