/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef TESTS_H
#define TESTS_H

#include <stdio.h>
#include <string.h>

void print_message(char *msg);

#define TEST_STRING(module, is, shouldbe) DO_TEST_STRING(module, is, shouldbe, __FILE__, __LINE__)

static void DO_TEST_STRING(char *module, char *is, char *shouldbe, char *file, int line)
{
	char message[256];
	if (strcmp(is, shouldbe))
	{
		snprintf(message, 256, "[tests] %s: FAILED: %s: %d: \"%s\" != \"%s\"\n", module, file, line, is, shouldbe);
		print_message(message);
	}
	else
	{
		snprintf(message, 256, "[tests] %s: PASSED: %s: %d: \"%s\" == \"%s\"\n", module, __FILE__, __LINE__, is, shouldbe); \
		print_message(message);
	}
}

#define TEST_NUMBER(module, is, shouldbe) DO_TEST_NUMBER(module, is, shouldbe, __FILE__, __LINE__)

static void DO_TEST_NUMBER(char *module, unsigned int is, unsigned int shouldbe, char *file, int line)
{
	char message[256];
	if (is != shouldbe)
	{
		snprintf(message, 256, "[tests] %s: FAILED: %s: %d: %d != %d\n", module, file, line, is, shouldbe);
		print_message(message);
	}
	else
	{
		snprintf(message, 256, "[tests] %s: PASSED: %s: %d: %d == %d\n", module, __FILE__, __LINE__, is, shouldbe); \
		print_message(message);
	}
}

void run_stdio_tests(void);

#endif

