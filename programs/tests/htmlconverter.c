
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct test
{
	char *msg;
	int failed;
};

struct submodule
{
	char *name;
	int testcount;
	int failedcount;
	struct test **tests;
};

struct module
{
	char *name;
	int submodcount;
	struct submodule **submods;
};

int modulecount = 0;
struct module **modules = 0;

struct module *get_module(char *name)
{
	int i;
	for (i = 0; i < modulecount; i++)
	{
		if (!strcmp(modules[i]->name, name))
		{
			return modules[i];
		}
	}
	return 0;
}
struct submodule *get_submodule(struct module *mod, char *name)
{
	int i;
	for (i = 0; i < mod->submodcount; i++)
	{
		if (!strcmp(mod->submods[i]->name, name))
		{
			return mod->submods[i];
		}
	}
	return 0;
}

void add_test(char *module, char *submodule, char *status, char *file, char *line, char *msg)
{
	// Get module
	struct module *mod = get_module(module);
	if (!mod)
	{
		// Add module
		modules = realloc(modules, (modulecount + 1) * sizeof(struct module*));
		modules[modulecount] = malloc(sizeof(struct module));
		memset(modules[modulecount], 0, sizeof(struct module));
		modules[modulecount]->name = strdup(module);
		mod = modules[modulecount];
		modulecount++;
	}
	// Get submodule
	struct submodule *submod = get_submodule(mod, submodule);
	if (!submod)
	{
		// Add module
		mod->submods = realloc(mod->submods, (mod->submodcount + 1) * sizeof(struct submodule*));
		mod->submods[mod->submodcount] = malloc(sizeof(struct submodule));
		memset(mod->submods[mod->submodcount], 0, sizeof(struct submodule));
		mod->submods[mod->submodcount]->name = strdup(submodule);
		submod = mod->submods[mod->submodcount];
		mod->submodcount++;
	}
	// Create test
	struct test *test = malloc(sizeof(struct test));
	test->msg = strdup(msg);
	if (strcmp(status, "PASSED"))
	{
		test->failed = 1;
		submod->failedcount++;
	}
	else
		test->failed = 0;
	// Add test
	submod->tests = realloc(submod->tests, (submod->testcount + 1) * sizeof(struct test*));
	submod->tests[submod->testcount] = test;
	submod->testcount++;
}

void create_html_file(void)
{
	printf("<html><head><title>Test results</title></head><body>\n");
	printf("<h1>Test results</h1>\n");
	int i;
	for (i = 0; i < modulecount; i++)
	{
		struct module *module = modules[i];
		printf("<h3>%s</h3>\n", module->name);
		printf("<table border=\"1\">\n");
		printf("<colgroup><col width=\"100\"><col width=\"100\"></colgroup>\n");
		int j;
		for (j = 0; j < module->submodcount; j++)
		{
			printf("<tr>\n");
			float ratio = (float)module->submods[j]->failedcount / (float)module->submods[j]->testcount;
			unsigned char red = ratio * 255;
			unsigned char green = (1.0 - ratio) * 255;
			printf("<td>%s</td><td bgcolor=\"#%02X%02X00\">%d/%d</td>", module->submods[j]->name,
				red, green, module->submods[j]->testcount - module->submods[j]->failedcount,
				module->submods[j]->testcount);
			printf("</tr>\n");
		}
		printf("</table>\n");
	}
	printf("</body></html>\n");
}

int main(int argc, char **argv)
{
	while (!feof(stdin))
	{
		char tmp[1024];
		char *buffer = tmp;
		fgets(buffer, 1024, stdin);
		if (!strncmp("[tests] ", buffer, 6))
		{
			buffer = strchr(buffer, ']');
			if (!buffer) continue;
			buffer += 2;
			char *module = buffer;
			buffer = strchr(buffer, '/');
			if (!buffer) continue;
			*buffer = 0;
			buffer++;
			char *submodule = buffer;
			buffer = strchr(buffer, ':');
			if (!buffer) continue;
			*buffer = 0;
			buffer += 2;
			char *status = buffer;
			buffer = strchr(buffer, ':');
			if (!buffer) continue;
			*buffer = 0;
			buffer += 2;
			char *file = buffer;
			buffer = strchr(buffer, ':');
			if (!buffer) continue;
			*buffer = 0;
			buffer += 2;
			char *line = buffer;
			buffer = strchr(buffer, ':');
			if (!buffer) continue;
			*buffer = 0;
			buffer += 2;
			char *msg = buffer;
			add_test(module, submodule, status, file, line, msg);
		}
	}
	
	create_html_file();
	
	return 0;
}

