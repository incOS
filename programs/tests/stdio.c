/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "tests.h"

void test_sprintf(void)
{
	char buffer[512];
	// Basic input values
	sprintf(buffer, "%c", 'A');
	TEST_STRING("stdio/sprintf", buffer, "A");
	sprintf(buffer, "%d", -10);
	TEST_STRING("stdio/sprintf", buffer, "-10");
	sprintf(buffer, "%d", 10);
	TEST_STRING("stdio/sprintf", buffer, "10");
	sprintf(buffer, "%i", -11);
	TEST_STRING("stdio/sprintf", buffer, "-11");
	sprintf(buffer, "%i", 11);
	TEST_STRING("stdio/sprintf", buffer, "11");
	sprintf(buffer, "%X", 0xDEAD);
	TEST_STRING("stdio/sprintf", buffer, "DEAD");
	sprintf(buffer, "%x", 0xdead);
	TEST_STRING("stdio/sprintf", buffer, "dead");
	sprintf(buffer, "%o", 012);
	TEST_STRING("stdio/sprintf", buffer, "12");
	sprintf(buffer, "%s", "teststring");
	TEST_STRING("stdio/sprintf", buffer, "teststring");
	sprintf(buffer, "%u", 12);
	TEST_STRING("stdio/sprintf", buffer, "12");
	sprintf(buffer, "%p", (void*)0x12345678);
	TEST_STRING("stdio/sprintf", buffer, "0x12345678");
	sprintf(buffer, "%%");
	TEST_STRING("stdio/sprintf", buffer, "%");
	unsigned int n = 0;
	sprintf(buffer, "12345%n", &n);
	TEST_NUMBER("stdio/sprintf", n, 5);
	// Modifiers
	sprintf(buffer, "%5d", -10);
	TEST_STRING("stdio/sprintf", buffer, "  -10");
	int padding = 6;
	sprintf(buffer, "%*d", padding, -10);
	TEST_STRING("stdio/sprintf", buffer, "   -10");
	sprintf(buffer, "%05d", -10);
	TEST_STRING("stdio/sprintf", buffer, "-0010");
	sprintf(buffer, "%05d", 10);
	TEST_STRING("stdio/sprintf", buffer, "00010");
	sprintf(buffer, "%-5d", -10);
	TEST_STRING("stdio/sprintf", buffer, "-10  ");
	sprintf(buffer, "%-5d", 10);
	TEST_STRING("stdio/sprintf", buffer, "10   ");
	sprintf(buffer, "%0d", 10);
	TEST_STRING("stdio/sprintf", buffer, "10");
	sprintf(buffer, "%#x", 0xdead);
	TEST_STRING("stdio/sprintf", buffer, "0xdead");
	sprintf(buffer, "%#X", 0xdead);
	TEST_STRING("stdio/sprintf", buffer, "0XDEAD");
	sprintf(buffer, "%#o", 012);
	TEST_STRING("stdio/sprintf", buffer, "012");
	sprintf(buffer, "%+d", 10);
	TEST_STRING("stdio/sprintf", buffer, "+10");
	sprintf(buffer, "% d", 10);
	TEST_STRING("stdio/sprintf", buffer, " 10");
	sprintf(buffer, "%hx", 0x12345678);
	TEST_STRING("stdio/sprintf", buffer, "5678");
	sprintf(buffer, "%hhx", 0x12345678);
	TEST_STRING("stdio/sprintf", buffer, "78");
	sprintf(buffer, "%ls", L"wide string");
	TEST_STRING("stdio/sprintf", buffer, "wide string");
	sprintf(buffer, "%S", L"wide string");
	TEST_STRING("stdio/sprintf", buffer, "wide string");
	// Changed argument order
	sprintf(buffer, "%2$d-%1$d", 1, 2);
	TEST_STRING("stdio/sprintf", buffer, "2-1");
	// TODO: Floating point numbers
}

void test_snprintf(void)
{
	char buffer[512];
	// Basic input values
	snprintf(buffer, 512, "%c", 'A');
	TEST_STRING("stdio/snprintf", buffer, "A");
	snprintf(buffer, 512, "%d", -10);
	TEST_STRING("stdio/snprintf", buffer, "-10");
	snprintf(buffer, 512, "%d", 10);
	TEST_STRING("stdio/snprintf", buffer, "10");
	snprintf(buffer, 512, "%i", -11);
	TEST_STRING("stdio/snprintf", buffer, "-11");
	snprintf(buffer, 512, "%i", 11);
	TEST_STRING("stdio/snprintf", buffer, "11");
	snprintf(buffer, 512, "%X", 0xDEAD);
	TEST_STRING("stdio/snprintf", buffer, "DEAD");
	snprintf(buffer, 512, "%x", 0xdead);
	TEST_STRING("stdio/snprintf", buffer, "dead");
	snprintf(buffer, 512, "%o", 012);
	TEST_STRING("stdio/snprintf", buffer, "12");
	snprintf(buffer, 512, "%s", "teststring");
	TEST_STRING("stdio/snprintf", buffer, "teststring");
	snprintf(buffer, 512, "%u", 12);
	TEST_STRING("stdio/snprintf", buffer, "12");
	snprintf(buffer, 512, "%p", (void*)0x12345678);
	TEST_STRING("stdio/snprintf", buffer, "0x12345678");
	snprintf(buffer, 512, "%%");
	TEST_STRING("stdio/snprintf", buffer, "%");
	unsigned int n = 0;
	snprintf(buffer, 512, "12345%n", &n);
	TEST_NUMBER("stdio/snprintf", n, 5);
	// Modifiers
	snprintf(buffer, 512, "%5d", -10);
	TEST_STRING("stdio/snprintf", buffer, "  -10");
	int padding = 6;
	snprintf(buffer, 512, "%*d", padding, -10);
	TEST_STRING("stdio/snprintf", buffer, "   -10");
	snprintf(buffer, 512, "%05d", -10);
	TEST_STRING("stdio/snprintf", buffer, "-0010");
	snprintf(buffer, 512, "%05d", 10);
	TEST_STRING("stdio/snprintf", buffer, "00010");
	snprintf(buffer, 512, "%-5d", -10);
	TEST_STRING("stdio/snprintf", buffer, "-10  ");
	snprintf(buffer, 512, "%-5d", 10);
	TEST_STRING("stdio/snprintf", buffer, "10   ");
	snprintf(buffer, 512, "%0d", 10);
	TEST_STRING("stdio/snprintf", buffer, "10");
	snprintf(buffer, 512, "%#x", 0xdead);
	TEST_STRING("stdio/snprintf", buffer, "0xdead");
	snprintf(buffer, 512, "%#X", 0xdead);
	TEST_STRING("stdio/snprintf", buffer, "0XDEAD");
	snprintf(buffer, 512, "%#o", 012);
	TEST_STRING("stdio/snprintf", buffer, "012");
	snprintf(buffer, 512, "%+d", 10);
	TEST_STRING("stdio/snprintf", buffer, "+10");
	snprintf(buffer, 512, "% d", 10);
	TEST_STRING("stdio/snprintf", buffer, " 10");
	snprintf(buffer, 512, "%hx", 0x12345678);
	TEST_STRING("stdio/snprintf", buffer, "5678");
	snprintf(buffer, 512, "%hhx", 0x12345678);
	TEST_STRING("stdio/snprintf", buffer, "78");
	snprintf(buffer, 512, "%ls", L"wide string");
	TEST_STRING("stdio/snprintf", buffer, "wide string");
	snprintf(buffer, 512, "%S", L"wide string");
	TEST_STRING("stdio/snprintf", buffer, "wide string");
	// Changed argument order
	snprintf(buffer, 512, "%2$d-%1$d", 1, 2);
	TEST_STRING("stdio/snprintf", buffer, "2-1");
	// TODO: Floating point numbers
	// Length limit
	snprintf(buffer, 5, "1234567890");
	TEST_STRING("stdio/snprintf", buffer, "1234");
	snprintf(buffer, 5, "%d", 123456789);
	TEST_STRING("stdio/snprintf", buffer, "1234");
	snprintf(buffer, 5, "%s", "teststring");
	TEST_STRING("stdio/snprintf", buffer, "test");
}

void run_stdio_tests(void)
{
	// Files
	// String functions
	test_sprintf();
	test_snprintf();
}

