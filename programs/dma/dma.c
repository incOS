/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <kernel.h>
#include <init.h>
#include <rpc.h>
#include <stdio.h>
#include <io.h>
#include <string.h>

uint32_t channel_used[8] = {0};
uintptr_t channel_paddr[8] = {0};
void *channel_vaddr[8] = {0};
uint32_t channel_buffersize[8] = {0};
uint32_t channel_mode[8] = {0};

uint16_t dma_status[8] = {0x08, 0x08, 0x08, 0x08, 0xD0, 0xD0, 0xD0, 0xD0};
uint16_t dma_command[8] = {0x08, 0x08, 0x08, 0x08, 0xD0, 0xD0, 0xD0, 0xD0};
uint16_t dma_request[8] = {0x09, 0x09, 0x09, 0x09, 0xD2, 0xD2, 0xD2, 0xD2};
uint16_t dma_channel_mask[8] = {0x0A, 0x0A, 0x0A, 0x0A, 0xD4, 0xD4, 0xD4, 0xD4};
uint16_t dma_mode[8] = {0x0B, 0x0B, 0x0B, 0x0B, 0xD6, 0xD6, 0xD6, 0xD6};
uint16_t dma_data[8] = {0x0C, 0x0C, 0x0C, 0x0C, 0xD8, 0xD8, 0xD8, 0xD8};
uint16_t dma_intermediate[8] = {0x0D, 0x0D, 0x0D, 0x0D, 0xDA, 0xDA, 0xDA, 0xDA};
uint16_t dma_mask[8] = {0x0F, 0x0F, 0x0F, 0x0F, 0xDE, 0xDE, 0xDE, 0xDE};
uint16_t dma_addr[8] = {0x00, 0x02, 0x04, 0x06, 0xC0, 0xC4, 0xC8, 0xCC};
uint16_t dma_page[8] = {0x87, 0x83, 0x81, 0x82, 0x8F, 0x8B, 0x89, 0x8A};
uint16_t dma_size[8] = {0x01, 0x03, 0x05, 0x07, 0xC2, 0xC6, 0xCA, 0xCE};

static void dmaSetupBuffer(uint32_t channel, uint32_t size)
{
	// Reserve memory
	if (channel_buffersize[channel] < (size + 4095) / 4096)
	{
		if (channel_buffersize[channel] != 0)
		{
			keFreeMemory(channel_vaddr[channel], channel_buffersize[channel]);
		}
		channel_buffersize[channel] = (size + 4095) / 4096;
		keAllocMemory(SYSCALL_MEM_DMA, (uintptr_t*)&channel_paddr[channel], (uintptr_t*)&channel_vaddr[channel], channel_buffersize[channel]);
	}
}

static void dmaExecute(uint32_t channel, uint32_t size, uint32_t mode)
{
	keDebug("dmaExecute.\n");
	// Mask channel
	outb(dma_channel_mask[channel], channel | 0x4);
	// Set address
	outb(dma_data[channel], 0xFF);
	outb(dma_addr[channel], channel_paddr[channel] & 0xFF);
	outb(dma_addr[channel], (channel_paddr[channel] >> 8) & 0xFF);
	outb(dma_page[channel], (channel_paddr[channel] >> 16) & 0xFF);
	// Set size
	outb(dma_data[channel], 0xFF);
	outb(dma_size[channel], size & 0xFF);
	outb(dma_size[channel], (size >> 8) & 0xFF);
	// Set mode
	outb(dma_mode[channel], (mode & 0xFC) + (channel & 0x3));
	// Unmask channel
	outb(dma_channel_mask[channel], channel);
}

static uint32_t devOpen(uint32_t caller, void *data, uint32_t size)
{
	if (size < 8) return -1;
	uint32_t channel = ((uint32_t*)data)[1];
	if (channel > 7) return -1;
	if (channel_used[channel]) return -1;
	// Mark channel as used
	channel_used[channel] = 1;
	channel_mode[channel] = 0;
	keDebug("dma: devOpen.\n");
	return 0;
}
static uint32_t devClose(uint32_t caller, void *data, uint32_t size)
{
	if (size < 8) return -1;
	uint32_t channel = ((uint32_t*)data)[1];
	if (channel > 7) return -1;
	channel_used[channel] = 0;
	keDebug("dma: devClose.\n");
	return 0;
}
static uint32_t devRead(uint32_t caller, void *data, uint32_t size)
{
	if (size < 12) return -1;
	uint32_t channel = ((uint32_t*)data)[1];
	uint32_t datalength = ((uint32_t*)data)[2];
	if (size < 12 + datalength) return -1;
	// Read data from buffer
	if (datalength > channel_buffersize[channel] * 4096)
	{
		datalength = channel_buffersize[channel] * 4096;
	}
	memcpy((char*)data + 12, channel_vaddr[channel], datalength);
	return datalength;
}
static uint32_t devWrite(uint32_t caller, void *data, uint32_t size)
{
	if (size < 20) return -1;
	uint32_t channel = ((uint32_t*)data)[1];
	uint32_t datalength = ((uint32_t*)data)[2];
	if (size < 12 + datalength) return -1;
	if (channel_mode[channel] == 0)
	{
		if (datalength < 8) return -1;
		// Setup DMA mode
		uint32_t transfersize = ((uint32_t*)data)[3];
		uint32_t mode = ((uint32_t*)data)[4];
		// Write data to DMA buffer
		dmaSetupBuffer(channel, transfersize);
		// Start DMA operation
		dmaExecute(channel, transfersize, mode);
		channel_mode[channel] = mode;
	}
	else
	{
		// Write data to DMA buffer
		if (datalength > channel_buffersize[channel] * 4096)
		{
			datalength = channel_buffersize[channel] * 4096;
		}
		memcpy(channel_vaddr[channel], (char*)data + 12, datalength);
	}
	return datalength;
}
static uint32_t devIOCTL(uint32_t caller, void *data, uint32_t size)
{
	keDebug("dma: devIOCTL.\n");
	return -1;
}

int _start(void)
{
	// Initialize RPC handlers
	rpcInit();
	rpcSetHandler(RPC_DEVFS_OPEN, devOpen);
	rpcSetHandler(RPC_DEVFS_CLOSE, devClose);
	rpcSetHandler(RPC_DEVFS_READ, devRead);
	rpcSetHandler(RPC_DEVFS_WRITE, devWrite);
	rpcSetHandler(RPC_DEVFS_IOCTL, devIOCTL);
	
	// Create device files
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	
	uint32_t i;
	for (i = 0; i < 8; i++)
	{
		rpcdata[0] = i;
		sprintf((char*)&rpcdata[1], "dma%d", i);
		rpcSend(INIT_PID, RPC_INIT_DEVFS_CREATE, rpcdata, 260, &retval);
	}
	
	keFreeMemory(rpcdata, 1);
	
	rpcHandlerSetActive(1);
	initSendInitialized();
	while (1) keWaitForRPC(0xFFFFFFFF);
	
	keExit();
	return 0;
}

