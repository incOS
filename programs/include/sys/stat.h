/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SYS_STAT_H_INCLUDED
#define SYS_STAT_H_INCLUDED

#define S_IRWXO 0007
#define S_IROTH 0001
#define S_IWOTH 0002
#define S_IXOTH 0004

#define S_IRWXG 0070
#define S_IRGRP 0010
#define S_IWGRP 0020
#define S_IXGRP 0040

#define S_IRWXU 0700
#define S_IRUSR 0100
#define S_IWUSR 0200
#define S_IXUSR 0400

#define S_ISUID 01000
#define S_ISGID 02000

#define S_ISVTX 04000

#define S_IFMT 0xF00
#define S_IFBLK 0x100
#define S_IFCHR 0x200
#define S_IFIFO 0x300
#define S_IFREG 0x400
#define S_IFDIR 0x500
#define S_IFLNK 0x600
#define S_IFSOCK 0x700

int mknod(const char *path, mode_t mode, dev_t dev);
int mkdir(const char *path, mode_t mode);

#endif

