/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef LIMITS_H_INCLUDED
#define LIMITS_H_INCLUDED

#define CHAR_BIT 8
#define CHAR_MAX SCHAR_MAX
#define CHAR_MIN SCHAR_MIN
#define INT_MAX 2147483647
#define LONG_BIT 32
#define LONG_MAX 2147483647
#define MB_LEN_MAX 1
#define SCHAR_MAX 127
#define SHRT_MAX 32767
#define SSIZE_MAX 32767
#define UCHAR_MAX 255
#define UINT_MAX 4294967295
#define ULONG_MAX 4294967295
#define USHRT_MAX 65535
#define WORD_BIT 32
#define INT_MIN -2147483647
#define LONG_MIN -2147483647
#define SCHAR_MIN -128
#define SHRT_MIN -32767
#define LLONG_MIN -9223372036854775807
#define LLONG_MAX +9223372036854775807
#define ULLONG_MAX 18446744073709551615 

#endif
