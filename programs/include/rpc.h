/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef RPC_H_INCLUDED
#define RPC_H_INCLUDED

#include <stdint.h>

#define RPC_INIT_GET_PID 0x00
#define RPC_INIT_GET_FS 0x01
#define RPC_INIT_DEBUG 0x02
#define RPC_INIT_INITIALIZED 0x03
#define RPC_INIT_DEVFS_CREATE 0x04
#define RPC_INIT_DEVFS_DELETE 0x05
#define RPC_INIT_REGISTER_FS 0x06
#define RPC_INIT_FORK 0x07
#define RPC_INIT_EXEC 0x08
// File system RPCs
#define RPC_FS_OPEN 0x10
#define RPC_FS_CLOSE 0x11
#define RPC_FS_READ 0x12
#define RPC_FS_WRITE 0x13
#define RPC_FS_RENAME 0x14
#define RPC_FS_DELETE 0x15
#define RPC_FS_IOCTL 0x16
#define RPC_FS_OPENDIR 0x17
#define RPC_FS_SEEK 0x18
#define RPC_FS_TRUNCATE 0x19
#define RPC_FS_MOUNT 0x1A
#define RPC_FS_UNMOUNT 0x1B
#define RPC_FS_MKNOD 0x1C
#define RPC_FS_REWINDDIR 0x1D
#define RPC_FS_CLONE 0x1E
#define RPC_FS_IOCTL_INFO 0x1F
// devfs
#define RPC_DEVFS_OPEN 0x20
#define RPC_DEVFS_CLOSE 0x21
#define RPC_DEVFS_READ 0x22
#define RPC_DEVFS_WRITE 0x23
#define RPC_DEVFS_IOCTL 0x24
#define RPC_DEVFS_IOCTL_INFO 0x25
#define RPC_DEVFS_SEEK 0x26
// Other RPCs
#define RPC_SIGNAL 0x70
// User defined RPCs (everything above this index)
#define RPC_USER 0x80

typedef uint32_t (*RPCHandler)(uint32_t caller, void *data, uint32_t size);

void rpcInit(void);
void rpcSetHandler(uint8_t index, RPCHandler handler);
void rpcHandlerSetActive(uint32_t active);

uint32_t rpcSend(uint32_t process, uint8_t index, void *data, uint32_t size, uint32_t *retvalue);

#endif

