/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef KERNEL_H_INCLUDED
#define KERNEL_H_INCLUDED

#include <stdint.h>

// Syscalls

void keSleep(uint32_t msecs);
void keExit(void);
int keCreateThread(void *entry);
void keStopThread(void);
uint32_t keCreateProcess(void);
uint32_t keCloneProcess(uint32_t process, uint32_t thread, uintptr_t entry);
uint32_t keMapProcessMemory(uint32_t process, uintptr_t srcaddr,
	uintptr_t targetaddr, uint32_t pagecount);
uint32_t keStartProcess(uint32_t process, uintptr_t entry);
void keGetProcessInfo(uint32_t *process, uint32_t *thread);
void keRegisterRPCHandler(void *entry);
void keWaitForRPC(uint32_t timeout);
uint32_t keSendRPC(uint32_t process, uint32_t timeout, uint32_t param, void *data,
	uint32_t datalength, uint32_t *returnval);
void keExitRPC(uint32_t returnval);
void keExitIRQ(void);

uint32_t keRegisterIRQHandler(uint32_t irq, void *entry);

#define SYSCALL_MEM_PHYSICAL 0x1
#define SYSCALL_MEM_VIRTUAL 0x2
#define SYSCALL_MEM_CONTINUOUS 0x4
#define SYSCALL_MEM_LOW 0x8
#define SYSCALL_MEM_DMA 0x10

uint32_t keAllocMemory(uint32_t flags, uintptr_t *paddr, uintptr_t *vaddr,
	uint32_t pagecount);
uint32_t keFreeMemory(void *vaddr, uint32_t pagecount);

void keDebug(const char *str);

#endif

