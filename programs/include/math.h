/*
Copyright (C) 2008  Kai Wagner

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

double cos   (double x);
double sin   (double x);
double tan   (double x);
double acos  (double x);
double asin  (double x);
double atan  (double x);
double atan2 (double x, double y);
double cosh  (double x);
double sinh  (double x);
double tanh  (double x);
double exp   (double x);
double frexp (double x, int *y);
double ldexp (double x, int y);
double log   (double x);
double log10 (double x);
double modf  (double x, double *y);
float  pow   (float x, float y);
float  sqrt  (float x);
double ceil  (double x);
double fabs  (double x);
double floor (double x);
double fmod  (double x, double y);


#endif
