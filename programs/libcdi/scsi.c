/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/scsi.h>
#include <cdi.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <kernel.h>
#include "devfs.h"

typedef struct
{
	DevFSFile file;
	struct cdi_scsi_device *device;
	char *scsibuffer;
	uint32_t scsibufferlength;
} SCSIDevice;

static cdi_list_t scsidevices;

// TODO: offset
static uint32_t cdi_scsi_read(DevFSFile *file, uint32_t offset, uint32_t length, void *buffer)
{
	SCSIDevice *device = (SCSIDevice*)file;
	if (device->scsibufferlength < length) length = device->scsibufferlength;
	memcpy(buffer, device->scsibuffer, length);
	return length;
}
static uint32_t cdi_scsi_write(DevFSFile *file, uint32_t offset, uint32_t length, void *data)
{
	SCSIDevice *device = (SCSIDevice*)file;
	device->scsibuffer = realloc(device->scsibuffer, length);
	memcpy(device->scsibuffer, data, length);
	device->scsibufferlength = length;
	return length;
}
static uint32_t cdi_scsi_ioctl(DevFSFile *file, int request, uint32_t length, void *data)
{
	SCSIDevice *device = (SCSIDevice*)file;
	keDebug("cdi_scsi_ioctl.\n");
	if (request != 0) return -1;
	if (length < 28) return -1;
	// Get data
	uint32_t flags = ((uint32_t*)data)[0];
	uint32_t bufferlength = ((uint32_t*)data)[1];
	uint32_t cmdlength = ((uint32_t*)data)[2];
	device->scsibuffer = realloc(device->scsibuffer, bufferlength);
	printf("Buffer: %p, %d bytes.\n", device->scsibuffer, bufferlength);
	if (!device->scsibuffer) device->scsibuffer = malloc(bufferlength);
	device->scsibufferlength = bufferlength;
	// Send SCSI request
	struct cdi_scsi_packet packet;
	memset(&packet, 0, sizeof(packet));
	packet.buffer = device->scsibuffer;
	packet.bufsize = bufferlength;
	packet.cmdsize = cmdlength;
	if (!length)
	{
		packet.direction = CDI_SCSI_NODATA;
	}
	else if (flags)
	{
		packet.direction = CDI_SCSI_WRITE;
	}
	else
	{
		packet.direction = CDI_SCSI_READ;
	}
	memcpy(&packet.command, &((uint32_t*)data)[3], 16);
	keDebug("Issuing SCSI command.\n");
	if (device->scsibuffer)
	{
		char msg[50];
		snprintf(msg, 50, "Command: %08X%08X, %d\n", ((uint32_t*)data)[4], ((uint32_t*)data)[3], packet.direction);
		keDebug(msg);
		snprintf(msg, 50, "Input: %08X%08X\n", ((uint32_t*)device->scsibuffer)[1], ((uint32_t*)device->scsibuffer)[0]);
		keDebug(msg);
	}
	int retval = ((struct cdi_scsi_driver*)(device->device->dev.driver))->request(device->device, &packet);
	if (device->scsibuffer)
	{
		char msg[50];
		snprintf(msg, 50, "Output: %08X%08X\n", ((uint32_t*)device->scsibuffer)[1], ((uint32_t*)device->scsibuffer)[0]);
		keDebug(msg);
	}
	return retval;
}
static uint32_t cdi_scsi_ioctlinfo(DevFSFile *file, int request, void *data)
{
	if (request != 0) return -1;
	int *ioctldata = data;
	ioctldata[0] = 1;
	ioctldata[1] = 28;
	return 0;
}

void cdi_scsi_packet_free(struct cdi_scsi_packet *packet)
{
	free(packet);
}

void cdi_scsi_driver_init(struct cdi_scsi_driver *driver)
{
	cdi_driver_init((struct cdi_driver*)driver);
}

void cdi_scsi_driver_destroy(struct cdi_scsi_driver *driver)
{
	cdi_driver_destroy((struct cdi_driver*)driver);
}

void cdi_scsi_driver_register(struct cdi_scsi_driver *driver)
{
	uint32_t i;
	for (i = 0; i < cdi_list_size(driver->drv.devices); i++)
	{
		// Create device file
		struct cdi_scsi_device *device = cdi_list_get(driver->drv.devices, i);
		SCSIDevice *sd = malloc(sizeof(SCSIDevice));
		memset(sd, 0, sizeof(SCSIDevice));
		sd->device = device;
		sd->file.path = device->dev.name;
		keDebug("Device: ");
		keDebug(device->dev.name);
		keDebug("\n");
		sd->file.read = cdi_scsi_read;
		sd->file.write = cdi_scsi_write;
		sd->file.seek = 0;
		sd->file.ioctl = cdi_scsi_ioctl;
		sd->file.ioctlinfo = cdi_scsi_ioctlinfo;
		devfsAddFile(&sd->file);
		if (!scsidevices) scsidevices = cdi_list_create();
		scsidevices = cdi_list_push(scsidevices, sd);
	}
	
	cdi_driver_register(&driver->drv);
}

