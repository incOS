
#include <cdi/fs.h>
#include <rpc.h>
#include <kernel.h>
#include <init.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

static uint32_t lastdriverid = 0;
static cdi_list_t fsdrivers = 0;
static uint32_t lastfsid = 0;
static cdi_list_t filesystems = 0;

struct cdi_fs_file
{
	struct cdi_fs_res *res;
	int type;
	uint32_t id;
	int position;
	int mode;
	uint32_t owner;
	cdi_list_t dirlist;
};
#define FILE_TYPE_FILE 0
#define FILE_TYPE_DIR 1

static struct cdi_fs_filesystem *fsGetFileSystem(uint32_t id)
{
	uint32_t i;
	for (i = 0; i < cdi_list_size(filesystems); i++)
	{
		struct cdi_fs_filesystem *fs = cdi_list_get(filesystems, i);
		if (fs->fsid == id) return fs;
	}
	return 0;
}
static struct cdi_fs_file *fsGetFile(struct cdi_fs_filesystem *fs, uint32_t id)
{
	uint32_t i;
	for (i = 0; i < cdi_list_size(fs->files); i++)
	{
		struct cdi_fs_file *file = cdi_list_get(fs->files, i);
		if (file->id == id)
		{
			return file;
		}
	}

	return 0;
}

static struct cdi_fs_res *fsGetFileFromPath(struct cdi_fs_filesystem *fs, char *path)
{
	char *tmppath = strdup(path);
	char *currentpath = tmppath;
	struct cdi_fs_res *res = fs->root_res;
	struct cdi_fs_stream stream;
	stream.fs = fs;
	stream.res = res;
	stream.error = 0;
	// Loop through parts of the path
	do
	{
		char *nextpath = strchr(currentpath, '/');
		if (nextpath)
		{
			*nextpath = 0;
			nextpath++;
		}
		if (!stream.res->loaded) stream.res->res->load(&stream);
		uint32_t i;
		uint32_t found = 0;
		for (i = 0; i < cdi_list_size(res->children); i++)
		{
			struct cdi_fs_res *child = cdi_list_get(res->children, i);
			if (!strcmp(currentpath, child->name))
			{
				res = child;
				stream.res = child;
				currentpath = nextpath;
				found = 1;
				break;
			}
		}
		if (!found) return 0;
	}
	while (currentpath);
	return res;
}

static uint32_t fsOpen(uint32_t caller, void *data, uint32_t size)
{
	if (size < 264)
		return -1;
	// Get data
	uint32_t fsid = ((uint32_t*)data)[0];
	struct cdi_fs_filesystem *fs = fsGetFileSystem(fsid);
	if (!fs) return -1;
	uint32_t flags = ((uint32_t*)data)[1];
	char *path = (char*)data + 8;
	path[255] = 0;
	
	// Get file
	struct cdi_fs_res *res = 0;
	struct cdi_fs_stream stream;
	stream.fs = fs;
	stream.error = 0;
	
	char *filename = 0;
	// Get parent directory
	if (strrchr(path, '/'))
	{
		filename = strrchr(path, '/') + 1;
		*strrchr(path, '/') = 0;
		res = fsGetFileFromPath(fs, path);
		if (!res) return -1;
		stream.res = res;
	}
	else
	{
		filename = path;
		res = fs->root_res;
		stream.res = res;
	}
	
	if (!stream.res->loaded) stream.res->res->load(&stream);
	if (strcmp(filename, ""))
	{
		// Look for file in directory
		uint32_t found = 0;
		uint32_t i;
		for (i = 0; i < cdi_list_size(res->children); i++)
		{
			struct cdi_fs_res *child = cdi_list_get(res->children, i);
			if (!strcmp(path, child->name))
			{
				found = 1;
				res = child;
				if (!res->loaded) res->res->load(&stream);
				break;
			}
		}
		if (!found)
		{
			if (flags & O_CREAT)
			{
				keDebug("Creating file.\n");
				stream.res = 0;
				// Create file
				res->dir->create_child(&stream, path, res);
				if (!stream.res->loaded) stream.res->res->load(&stream);
				stream.res->res->assign_class(&stream, CDI_FS_CLASS_FILE);
				res = stream.res;
			}
			else
			{
				keDebug("cdi_fs: File not found.\n");
				return -1;
			}
		}
	}
	
	// Add file to list
	struct cdi_fs_file *file = malloc(sizeof(struct cdi_fs_file));
	memset(file, 0, sizeof(struct cdi_fs_file));
	file->id = ++fs->last_fh;
	file->res = res;
	file->mode = flags & O_ACCMODE;
	file->type = FILE_TYPE_FILE;
	file->owner = caller;
	fs->files = cdi_list_push(fs->files, file);
	
	((uint32_t*)data)[0] = file->id;
	return 0;
}
static uint32_t fsClose(uint32_t caller, void *data, uint32_t size)
{
	if (size < 8)
		return -1;
	uint32_t fsid = ((uint32_t*)data)[0];
	struct cdi_fs_filesystem *fs = fsGetFileSystem(fsid);
	if (!fs) return -1;
	uint32_t fh = ((uint32_t*)data)[1];
	// Find file
	uint32_t i;
	for (i = 0; i < cdi_list_size(fs->files); i++)
	{
		struct cdi_fs_file *file = cdi_list_get(fs->files, i);
		if (file->id == fh)
		{
			// Close resource and delete file
			struct cdi_fs_stream stream;
			stream.res = file->res;
			stream.fs = fs;
			stream.error = 0;
			file->res->res->unload(&stream);
			if (file->dirlist) cdi_list_destroy(file->dirlist);
			free(file);
			cdi_list_remove(fs->files, i);
			return 0;
		}
	}

	return -1;
}
static uint32_t fsRead(uint32_t caller, void *data, uint32_t size)
{
	if (size < 12)
		return -1;
	
	// Validate data
	uint32_t fsid = ((uint32_t*)data)[0];
	struct cdi_fs_filesystem *fs = fsGetFileSystem(fsid);
	if (!fs) keDebug("cdi_fs: File system not found!\n");
	if (!fs) return -1;
	uint32_t fh = ((uint32_t*)data)[1];
	uint32_t datalength = ((uint32_t*)data)[2];
	if (size < datalength + 12) keDebug("cdi_fs: Not enough data!\n");
	if (size < datalength + 12) return -1;
	struct cdi_fs_file *file = fsGetFile(fs, fh);
	if (!file) keDebug("cdi_fs: File not found!\n");
	if (!file) return -1;
	if (file->type == FILE_TYPE_FILE)
	{
		if (file->mode == O_WRONLY) return -1;
	
		// Read data from file
		struct cdi_fs_stream stream;
		stream.res = file->res;
		stream.fs = fs;
		stream.error = 0;
		ssize_t ret = file->res->file->read(&stream, file->position, datalength, &((uint32_t*)data)[3]);
		if (ret <= 0)
		{
			if ((stream.error == CDI_FS_ERROR_EOF) || (stream.error == 0))
				return 0;
			else
				return -1;
		}
		else
		{
			file->position += ret;
			return ret;
		}
	}
	else if (file->type == FILE_TYPE_DIR)
	{
		struct cdi_fs_stream stream;
		stream.res = file->res;
		stream.fs = fs;
		stream.error = 0;
		if (datalength < 256) return -1;
		if (!file->dirlist)
		{
			if (file->res->dir == 0) return -1;
			file->dirlist = file->res->dir->list(&stream);
		}
		if (file->position < 2)
		{
			strcpy((char*)&((uint32_t*)data)[3], file->position?"..":".");
			file->position++;
			return 1;
		}
		struct cdi_fs_res *child = cdi_list_get(file->dirlist, file->position - 2);
		if (child)
		{
			strcpy((char*)&((uint32_t*)data)[3], child->name);
			file->position++;
			return 1;
		}
		else return 0;
	}
	return -1;
}
static uint32_t fsWrite(uint32_t caller, void *data, uint32_t size)
{
	if (size < 12)
		return -1;
	
	// Validate data
	uint32_t fsid = ((uint32_t*)data)[0];
	struct cdi_fs_filesystem *fs = fsGetFileSystem(fsid);
	if (!fs) return -1;
	uint32_t fh = ((uint32_t*)data)[1];
	uint32_t datalength = ((uint32_t*)data)[2];
	if (size < datalength + 12) return -1;
	struct cdi_fs_file *file = fsGetFile(fs, fh);
	if (!file) return -1;
	if (file->mode == O_RDONLY) return -1;
	if (file->type != FILE_TYPE_FILE) return -1;
	
	// Write data to file
	struct cdi_fs_stream stream;
	stream.res = file->res;
	stream.fs = fs;
	stream.error = 0;
	ssize_t ret = file->res->file->write(&stream, file->position, datalength, &((uint32_t*)data)[3]);
	if (ret == 0)
	{
		return -1;
	}
	else
	{
		file->position += ret;
		return ret;
	}
}
static uint32_t fsRename(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
static uint32_t fsDelete(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
static uint32_t fsIOCTL(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
static uint32_t fsOpendir(uint32_t caller, void *data, uint32_t size)
{
	if (size < 264)
		return -1;
	// Get data
	uint32_t fsid = ((uint32_t*)data)[0];
	struct cdi_fs_filesystem *fs = fsGetFileSystem(fsid);
	if (!fs) return -1;
	char *path = (char*)data + 8;
	path[255] = 0;
	if ((strlen(path) > 0) && (path[strlen(path) - 1] == '/'))
		path[strlen(path) - 1] = 0;
	
	// Get directory
	struct cdi_fs_res *res = 0;
	if (strcmp(path, ""))
	{
		res = fsGetFileFromPath(fs, path);
	}
	else
	{
		res = fs->root_res;
	}
	if (!res) return -1;
	if (!res->dir) return -1;
	
	// Add directory to list
	struct cdi_fs_file *file = malloc(sizeof(struct cdi_fs_file));
	memset(file, 0, sizeof(struct cdi_fs_file));
	file->id = ++fs->last_fh;
	file->res = res;
	file->type = FILE_TYPE_DIR;
	file->owner = caller;
	fs->files = cdi_list_push(fs->files, file);
	
	((uint32_t*)data)[0] = file->id;
	return 0;
}
static uint32_t fsRewinddir(uint32_t caller, void *data, uint32_t size)
{
	if (size < 8)
		return -1;
	// Get data
	uint32_t fsid = ((uint32_t*)data)[0];
	struct cdi_fs_filesystem *fs = fsGetFileSystem(fsid);
	if (!fs) return -1;
	uint32_t fh = ((uint32_t*)data)[1];
	struct cdi_fs_file *file = fsGetFile(fs, fh);
	if (!file) return -1;
	if (file->type != FILE_TYPE_DIR) return -1;
	
	// Reload directory list
	struct cdi_fs_stream stream;
	stream.res = file->res;
	stream.fs = fs;
	stream.error = 0;
	if (file->dirlist) cdi_list_destroy(file->dirlist);
	file->dirlist = file->res->dir->list(&stream);
	file->position = 0;
	return 0;
}
static uint32_t fsSeek(uint32_t caller, void *data, uint32_t size)
{
	if (size < 16)
		return -1;
	// Get data
	uint32_t fsid = ((uint32_t*)data)[0];
	struct cdi_fs_filesystem *fs = fsGetFileSystem(fsid);
	if (!fs) return -1;
	uint32_t fh = ((uint32_t*)data)[1];
	struct cdi_fs_file *file = fsGetFile(fs, fh);
	if (!file) return -1;
	int position = ((uint32_t*)data)[2];
	uint32_t whence = ((uint32_t*)data)[3];
	if (file->type == FILE_TYPE_FILE)
	{
		// Change position
		struct cdi_fs_stream stream;
		stream.res = file->res;
		stream.fs = fs;
		stream.error = 0;
		int filesize = file->res->res->meta_read(&stream,CDI_FS_META_SIZE);
		if (whence == SEEK_SET)
		{
			if (filesize < position) position = filesize;
			file->position = position;
			return file->position;
		}
		else if (whence == SEEK_CUR)
		{
			file->position += position;
			if (filesize < file->position) file->position = filesize;
			if (file->position < 0) file->position = 0;
			return file->position;
		}
		else if (whence == SEEK_END)
		{
			file->position = filesize + position;
			if (filesize < file->position) file->position = filesize;
			if (file->position < 0) file->position = 0;
			return file->position;
		}
		else
			return -1;
	}
	else if (file->type == FILE_TYPE_DIR)
	{
		struct cdi_fs_stream stream;
		stream.res = file->res;
		stream.fs = fs;
		stream.error = 0;
		// Create file list if not already there
		if (!file->dirlist)
		{
			if (file->res->dir == 0) return -1;
			file->dirlist = file->res->dir->list(&stream);
		}
		// Change position in file
		if (whence == SEEK_SET)
		{
			file->position = position;
			if (file->position > (int)cdi_list_size(file->dirlist) + 2)
				file->position = cdi_list_size(file->dirlist) + 2;
		}
		else if (whence == SEEK_CUR)
		{
			file->position += position;
			if (file->position > (int)cdi_list_size(file->dirlist) + 2)
				file->position = cdi_list_size(file->dirlist) + 2;
			if (file->position < 0) file->position = 0;
		}
		else if (whence == SEEK_END)
		{
			file->position = cdi_list_size(file->dirlist) + 2 + position;
			if (file->position > (int)cdi_list_size(file->dirlist) + 2)
				file->position = cdi_list_size(file->dirlist) + 2;
			if (file->position < 0) file->position = 0;
		}
		return file->position;
	}
	return -1;
}
static uint32_t fsTruncate(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
static uint32_t fsMount(uint32_t caller, void *data, uint32_t size)
{
	keDebug("Mounting file system.\n");
	// Get data
	if (size < 520) return -1;
	uint32_t driverid = ((uint32_t*)data)[0];
	struct cdi_fs_driver *driver = cdi_list_get(fsdrivers, driverid);
	if (!driver) return -1;
	uint32_t readonly = ((uint32_t*)data)[1];
	char *path = (char*)data + 8;
	char *source = (char*)data + 264;
	// Create file system
	struct cdi_fs_filesystem *filesystem = malloc(sizeof(struct cdi_fs_filesystem));
	filesystem->driver = driver;
	filesystem->error = 0;
	filesystem->read_only = readonly;
	filesystem->fsid = ++lastfsid;
	filesystem->last_fh = 0;
	filesystem->files = cdi_list_create();
	if (!strcmp(source, ""))
	{
		filesystem->data_fh = 0;
	}
	else
	{
		filesystem->data_fh = open(source, O_RDWR);
		if (filesystem->data_fh == -1)
		{
			free(filesystem);
			return -1;
		}
	}
	filesystem->mountpoint = strdup(path);
	filesystem->data_dev = strdup(source);
	
	// Initialize file system
	if (driver->fs_init(filesystem))
	{
		filesystems = cdi_list_push(filesystems, filesystem);
	}
	else
	{
		free(filesystem->mountpoint);
		free(filesystem->data_dev);
		free(filesystem);
		return -1;
	}
	return filesystem->fsid;
}
static uint32_t fsUnmount(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
static uint32_t fsMknod(uint32_t caller, void *data, uint32_t size)
{
	if (size < 264)
	return -1;
	// Get data
	uint32_t fsid = ((uint32_t*)data)[0];
	struct cdi_fs_filesystem *fs = fsGetFileSystem(fsid);
	if (!fs) return -1;
	uint32_t mode = ((uint32_t*)data)[1];
	char *path = (char*)&((uint32_t*)data)[2];
	path[255] = 0;
	if (path[strlen(path) - 1] == '/') path[strlen(path) - 1] = 0;
	
	// Get file
	struct cdi_fs_res *res = fs->root_res;
	struct cdi_fs_stream stream;
	stream.fs = fs;
	stream.res = res;
	stream.error = 0;
	
	// Search parent directory
	while (strchr(path, '/'))
	{
		char *nextpath = strchr(path, '/') + 1;
		*strchr(path, '/') = 0;
		if (!stream.res->loaded) stream.res->res->load(&stream);
		uint32_t i;
		uint32_t found = 0;
		for (i = 0; i < cdi_list_size(res->children); i++)
		{
			struct cdi_fs_res *child = cdi_list_get(res->children, i);
			if (!strcmp(path, child->name))
			{
				res = child;
				stream.res = child;
				path = nextpath;
				found = 1;
				break;
			}
		}
		if (!found) return -1;
	}
	if (!stream.res->loaded) stream.res->res->load(&stream);
	
	// Look for file in directory
	uint32_t found = 0;
	uint32_t i;
	for (i = 0; i < cdi_list_size(res->children); i++)
	{
		struct cdi_fs_res *child = cdi_list_get(res->children, i);
		if (!strcmp(path, child->name))
		{
			found = 1;
			break;
		}
	}
	if (found)
	{
		return -1;
	}
	
	// Create file
	stream.res = 0;
	// Create file
	res->dir->create_child(&stream, path, res);
	if (!stream.res->loaded) stream.res->res->load(&stream);
	switch (mode & S_IFMT)
	{
		case S_IFREG:
			stream.res->res->assign_class(&stream, CDI_FS_CLASS_FILE);
			break;
		case S_IFDIR:
			stream.res->res->assign_class(&stream, CDI_FS_CLASS_DIR);
			break;
		case S_IFLNK:
			stream.res->res->assign_class(&stream, CDI_FS_CLASS_LINK);
			break;
		default:
			return -1;
	}
	
	return 0;
}
static uint32_t fsClone(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
void fsInit(void)
{
	fsdrivers = cdi_list_create();
	filesystems = cdi_list_create();
	// Register FS handlers
	rpcSetHandler(RPC_FS_OPEN, fsOpen);
	rpcSetHandler(RPC_FS_CLOSE, fsClose);
	rpcSetHandler(RPC_FS_READ, fsRead);
	rpcSetHandler(RPC_FS_WRITE, fsWrite);
	rpcSetHandler(RPC_FS_RENAME, fsRename);
	rpcSetHandler(RPC_FS_DELETE, fsDelete);
	rpcSetHandler(RPC_FS_IOCTL, fsIOCTL);
	rpcSetHandler(RPC_FS_OPENDIR, fsOpendir);
	rpcSetHandler(RPC_FS_REWINDDIR, fsRewinddir);
	rpcSetHandler(RPC_FS_SEEK, fsSeek);
	rpcSetHandler(RPC_FS_TRUNCATE, fsTruncate);
	rpcSetHandler(RPC_FS_MOUNT, fsMount);
	rpcSetHandler(RPC_FS_UNMOUNT, fsUnmount);
	rpcSetHandler(RPC_FS_MKNOD, fsMknod);
	rpcSetHandler(RPC_FS_CLONE, fsClone);
}

void cdi_fs_driver_init(struct cdi_fs_driver *driver)
{
	cdi_driver_init(&driver->drv);
}
void cdi_fs_driver_destroy(struct cdi_fs_driver *driver)
{
	cdi_driver_destroy(&driver->drv);
}
void cdi_fs_driver_register(struct cdi_fs_driver *driver)
{
	fsdrivers = cdi_list_push(fsdrivers, driver);
	cdi_driver_register(&driver->drv);
	keDebug("Filesystem: ");
	keDebug(driver->drv.name);
	keDebug("\n");
	// Send register message to init
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = lastdriverid++;
	strcpy((char*)&rpcdata[1], driver->drv.name);
	if (rpcSend(INIT_PID, RPC_INIT_REGISTER_FS, rpcdata, 5 + strlen(driver->drv.name), 0) != 1)
	{
		keDebug("Could not register file system.\n");
	}
	keFreeMemory(rpcdata, 1);
}


size_t cdi_fs_data_read(struct cdi_fs_filesystem *fs, uint64_t start,
	size_t size, void *buffer)
{
	keDebug("cdi_fs_data_read unimplemented!\n");
	return 0;
}

size_t cdi_fs_data_write(struct cdi_fs_filesystem* fs, uint64_t start,
	size_t size, const void* buffer)
{
	keDebug("cdi_fs_data_write unimplemented!\n");
	return 0;
}

