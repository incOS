/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/dma.h>
#include <fcntl.h>
#include <stdio.h>

int cdi_dma_open(struct cdi_dma_handle* handle, uint8_t channel, uint8_t mode,
	size_t length, void* buffer)
{
	// Open DMA channel
	char filename[20];
	sprintf(filename, "/dev/dma%d", channel);
	int dmafile = open(filename, O_RDWR);
	if (dmafile == -1)
	{
		return -1;
	}
	
	// Save info in struct
	handle->channel = channel;
	handle->length = length;
	handle->mode = mode;
	handle->buffer = buffer;
	handle->file = dmafile;
	// Setup DMA channel
	uint32_t command[2];
	command[0] = handle->length;
	command[1] = handle->mode | 0x2;
	if (write(handle->file, command, 8) != 8)
	{
		return -1;
	}
	return 0;
}

int cdi_dma_read(struct cdi_dma_handle* handle)
{
	if (read(handle->file, handle->buffer, handle->length) != (int)handle->length)
	{
		return -1;
	}
	return 0;
}

int cdi_dma_write(struct cdi_dma_handle* handle)
{
	if (write(handle->file, handle->buffer, handle->length) != (int)handle->length)
	{
		return -1;
	}
	return 0;
}

int cdi_dma_close(struct cdi_dma_handle* handle)
{
	return close(handle->file);
}

