/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "devfs.h"
#include <cdi/lists.h>
#include <kernel.h>
#include <rpc.h>
#include <init.h>
#include <string.h>

static cdi_list_t files;
static uint32_t lastid = 0;

DevFSFile *devGetFile(uint32_t id)
{
	uint32_t i;
	for (i = 0; i < cdi_list_size(files); i++)
	{
		DevFSFile *file = cdi_list_get(files, i);
		if (file->id == id) return file;
	}
	return 0;
}

static uint32_t devOpen(uint32_t caller, void *data, uint32_t size)
{
	keDebug("devOpen.\n");
	return 0;
}
static uint32_t devClose(uint32_t caller, void *data, uint32_t size)
{
	keDebug("devClose.\n");
	return 0;
}
static uint32_t devRead(uint32_t caller, void *data, uint32_t size)
{
	// Get data from message
	if (size < 12) return -1;
	uint32_t datalength = ((uint32_t*)data)[2];
	uint32_t id = ((uint32_t*)data)[1];
	if (size < 12 + datalength) return -1;
	// Get file and read data
	DevFSFile *file = devGetFile(id);
	if (!file) return -1;
	if (!file->read) return 0;
	return file->read(file, file->offset, datalength, &((uint32_t*)data)[3]);
}
static uint32_t devWrite(uint32_t caller, void *data, uint32_t size)
{
	// Get data from message
	if (size < 12) return -1;
	uint32_t datalength = ((uint32_t*)data)[2];
	uint32_t id = ((uint32_t*)data)[1];
	if (size < 12 + datalength) return -1;
	// Get file and read data
	DevFSFile *file = devGetFile(id);
	if (!file) return -1;
	if (!file->write) return 0;
	return file->write(file, file->offset, datalength, &((uint32_t*)data)[3]);
}
static uint32_t devIOCTL(uint32_t caller, void *data, uint32_t size)
{
	keDebug("cdi: devIOCTL 0.\n");
	if (size < 12)
		return -1;
	
	int request = ((uint32_t*)data)[2];
	uint32_t id = ((uint32_t*)data)[1];
	// Get file and call ioctl function
	DevFSFile *file = devGetFile(id);
	if (!file) return -1;
	if (!file->ioctl) return -1;
	keDebug("cdi: devIOCTL.\n");
	return file->ioctl(file, request, size - 12, &((uint32_t*)data)[3]);
}
static uint32_t devIOCTLInfo(uint32_t caller, void *data, uint32_t size)
{
	if (size < 0x1000)
		return -1;
	
	int request = ((uint32_t*)data)[2];
	uint32_t id = ((uint32_t*)data)[1];
	// Get file and call ioctl function
	DevFSFile *file = devGetFile(id);
	if (!file) return -1;
	if (!file->ioctl) return -1;
	keDebug("cdi: devIOCTLInfo.\n");
	return file->ioctlinfo(file, request, &((uint32_t*)data)[3]);
}
static uint32_t devSeek(uint32_t caller, void *data, uint32_t size)
{
	// Get data from message
	if (size < 16) return -1;
	uint32_t position = ((uint32_t*)data)[2];
	uint32_t whence = ((uint32_t*)data)[3];
	uint32_t id = ((uint32_t*)data)[1];
	// Get file and change position
	DevFSFile *file = devGetFile(id);
	if (!file) return -1;
	if (file->seek)
	{
		file->offset = file->seek(file, position, whence);
	}
	else
	{
		file->offset = position;
	}

	return file->offset;
}
void devfsInit(void)
{
	rpcSetHandler(RPC_DEVFS_OPEN, devOpen);
	rpcSetHandler(RPC_DEVFS_CLOSE, devClose);
	rpcSetHandler(RPC_DEVFS_READ, devRead);
	rpcSetHandler(RPC_DEVFS_WRITE, devWrite);
	rpcSetHandler(RPC_DEVFS_IOCTL, devIOCTL);
	rpcSetHandler(RPC_DEVFS_SEEK, devSeek);
	rpcSetHandler(RPC_DEVFS_IOCTL_INFO, devIOCTLInfo);
	
	files = cdi_list_create();
}
void devfsDestroy(void)
{
	// TODO
}

void devfsAddFile(DevFSFile *file)
{
	file->offset = 0;
	file->id = ++lastid;
	cdi_list_push(files, file);
	
	// Create devfs file
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = file->id;
	strcpy((char*)&rpcdata[1], file->path);
	if (rpcSend(INIT_PID, RPC_INIT_DEVFS_CREATE, rpcdata, 260, &retval) != 1)
	{
		keDebug("CDI: Could not create file.\n");
	}
	keFreeMemory(rpcdata, 1);
}
void devfsRemoveFile(DevFSFile *file)
{
	// TODO
	file = 0;
}
void devfsRemoveAllFiles(void)
{
	// TODO
}

