/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef DEVFS_H
#define DEVFS_H

#include <stdint.h>

typedef struct DevFSFile
{
	const char *path;
	uint32_t id;
	uint32_t (*read)(struct DevFSFile *file, uint32_t offset, uint32_t length, void *buffer);
	uint32_t (*write)(struct DevFSFile *file, uint32_t offset, uint32_t length, void *buffer);
	uint32_t (*seek)(struct DevFSFile *file, uint32_t offset, uint32_t whence);
	uint32_t (*ioctl)(struct DevFSFile *file, int request, uint32_t length, void *buffer);
	uint32_t (*ioctlinfo)(struct DevFSFile *file, int request, void *buffer);
	uint32_t offset;
} DevFSFile;

void devfsInit(void);
void devfsDestroy(void);

void devfsAddFile(DevFSFile *file);
void devfsRemoveFile(DevFSFile *file);
void devfsRemoveAllFiles(void);

#endif

