/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/storage.h>
#include <stdlib.h>
#include <string.h>
#include <kernel.h>
#include "devfs.h"

typedef struct
{
	DevFSFile file;
	struct cdi_storage_device *device;
} MassStorageDevice;

static cdi_list_t msdevices = 0;

static uint32_t cdi_storage_read(DevFSFile *file, uint32_t offset, uint32_t length, void *buffer)
{
	MassStorageDevice *msd = (MassStorageDevice*)file;
	struct cdi_storage_device *device = msd->device;
	struct cdi_storage_driver *driver = (struct cdi_storage_driver*)device->dev.driver;
	
	size_t block_size = device->block_size;
	// Blocks to be read
	uint64_t block_read_start = offset / block_size;
	uint64_t block_read_end = (offset + length) / block_size;
	uint64_t block_read_count = block_read_end - block_read_start;
	
	if (((offset % block_size) == 0) && (((offset + length) %  block_size) == 0))
	{
		// Only read whole blocks
		if (driver->read_blocks(device, block_read_start, block_read_count, buffer) != 0)
		{
			keDebug("Could not read blocks.\n");
			return -1;
		}
		return length;
	}
	else
	{
		// TODO: Optimize this
		block_read_count++;
		uint8_t buffer[block_read_count * block_size];
		
		// Buffer data
		if (driver->read_blocks(device, block_read_start, block_read_count, buffer) != 0)
		{
			keDebug("Could not read blocks.\n");
			return -1;
		}
		
		// Copy data from buffer
		memcpy(buffer, buffer + (offset % block_size), length);
	}
	return length;
}
static uint32_t cdi_storage_write(DevFSFile *file, uint32_t offset, uint32_t length, void *data)
{
	MassStorageDevice *msd = (MassStorageDevice*)file;
	struct cdi_storage_device *device = msd->device;
	struct cdi_storage_driver *driver = (struct cdi_storage_driver*)device->dev.driver;

	size_t block_size = device->block_size;
	uint64_t block_write_start = offset / block_size;
	uint8_t buffer[block_size];
	size_t blockoffset;
	size_t tmp_size;

	// Wenn die Startposition nicht auf einer Blockgrenze liegt, muessen wir
	// hier zuerst den ersten Block laden, die gewuenschten Aenderungen machen,
	// und den Block wieder Speichern.
	blockoffset = (offset % block_size);
	if (blockoffset != 0)
	{
		tmp_size = block_size - blockoffset;
		tmp_size = (tmp_size > length ? length : tmp_size);

		if (driver->read_blocks(device, block_write_start, 1, buffer) != 0) {
			return -1;
		}
		memcpy(data + blockoffset, data, tmp_size);

		// Buffer abspeichern
		if (driver->write_blocks(device, block_write_start, 1, buffer) != 0)
		{
			return -1;
		}
		
		length -= tmp_size;
		data += tmp_size;
		block_write_start++;
	}
	
	// Jetzt wird die Menge der ganzen Blocks auf einmal geschrieben, falls
	// welche existieren
	tmp_size = length / block_size;
	if (tmp_size != 0) {
		// Buffer abspeichern
		if (driver->write_blocks(device, block_write_start, tmp_size, data) != 0) 
		{
			return -1;
		}
		length -= tmp_size * block_size;
		data += tmp_size * block_size;
		block_write_start += block_size;        
	}

	// Wenn der letzte Block nur teilweise beschrieben wird, geschieht das hier
	if (length != 0) {
		// Hier geschieht fast das Selbe wie oben beim ersten Block
		if (driver->read_blocks(device, block_write_start, 1, buffer) != 0) {
			return -1;
		}
		memcpy(data, buffer, length);

		// Buffer abspeichern
		if (driver->write_blocks(device, block_write_start, 1, buffer) != 0) {
			return -1;
		}
	}
	return 0;
}
static uint32_t cdi_storage_seek(DevFSFile *file, uint32_t offset, uint32_t whence)
{
	// TODO
	return offset;
}

void cdi_storage_driver_init(struct cdi_storage_driver *driver)
{
	cdi_driver_init(&driver->drv);
}

void cdi_storage_driver_destroy(struct cdi_storage_driver *driver)
{
	cdi_driver_destroy(&driver->drv);
}

void cdi_storage_driver_register(struct cdi_storage_driver *driver)
{
	uint32_t i;
	for (i = 0; i < cdi_list_size(driver->drv.devices); i++)
	{
		// Create device file
		struct cdi_storage_device *device = cdi_list_get(driver->drv.devices, i);
		MassStorageDevice *msd = malloc(sizeof(MassStorageDevice));
		msd->device = device;
		memset(&msd->file, 0, sizeof(msd->file));
		msd->file.path = device->dev.name;
		msd->file.read = cdi_storage_read;
		msd->file.write = cdi_storage_write;
		msd->file.seek = cdi_storage_seek;
		devfsAddFile(&msd->file);
		if (!msdevices) msdevices = cdi_list_create();
		msdevices = cdi_list_push(msdevices, msd);
	}
	
	cdi_driver_register(&driver->drv);
}

