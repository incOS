
#include <unistd.h>

int main(int argc, char **argv)
{
	printf("Fork test.\n");
	printf("==========\n");
	
	int pid = fork();
	if (pid == -1)
	{
		keDebug("Fork error.\n");
		printf("Could not fork program.\n");
		return -1;
	}
	
	if (!pid)
	{
		keDebug("Fork child.\n");
		printf("Child process.\n");
		int i;
		for (i = 0; i < 10; i++)
		{
			printf("Child: %d\n", i);
			sleep(1);
		}
	}
	else
	{
		keDebug("Fork parent.\n");
		printf("Parent process.\n");
		int i;
		for (i = 0; i < 10; i++)
		{
			printf("Parent: %d\n", i);
			sleep(1);
		}
	}
	return 0;
}

