/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "elf.h"

#include <string.h>
#include <kernel.h>

int elfIsValid(void *addr, uintptr_t size)
{
	// TODO
	return 1;
}
uintptr_t elfMapProgram(uint32_t process, void *addr, uintptr_t size)
{
	// Load program info
	ElfHeader header;
	memcpy(&header, addr, sizeof(ElfHeader));
	
	// Load sections into task address space
	/*ElfSectionHeader *sheaders = (ElfSectionHeader*)((uintptr_t)addr + header.e_shoff);
	uint32_t i;
	for (i = 0; i < header.e_shnum; i++)
	{
		if (sheaders[i].sh_flags & SHF_ALLOC)
		{
			if (sheaders[i].sh_type == SHT_NOBITS)
				memset((char*)addr + sheaders[i].sh_offset, 0, sheaders[i].sh_size);

			uintptr_t secstart = sheaders[i].sh_addr & ~0xFFF;
			uintptr_t secend = (sheaders[i].sh_addr + sheaders[i].sh_size + 4095) & ~0xFFF;
			uintptr_t secpagecount = (secend - secstart) / 4096;
			keMapProcessMemory(process, (((uintptr_t)addr + sheaders[i].sh_offset) & ~0xFFF), (sheaders[i].sh_addr & ~0xFFF), secpagecount);
		}
	}*/
	ElfProgramHeader *pheaders = (ElfProgramHeader*)((uintptr_t)addr + header.e_phoff);
	uint32_t i;
	for (i = 0; i < header.e_phnum; i++)
	{
		if (pheaders[i].p_type == PT_LOAD)
		{
			uintptr_t secstart = pheaders[i].p_vaddr & ~0xFFF;
			uintptr_t secend = (pheaders[i].p_vaddr + pheaders[i].p_memsz + 4095) & ~0xFFF;
			uintptr_t secpagecount = (secend - secstart) / 4096;
			/*if (pheaders[i].p_memsz == pheaders[i].p_filesz)
			{
				// FIXME: If we share the data pages between the programs, this might cause errors with fork(). Code can be shared though
				keMapProcessMemory(process, (((uintptr_t)addr + pheaders[i].p_offset) & ~0xFFF), (pheaders[i].p_vaddr & ~0xFFF), secpagecount);
			}
			else
			{*/
				char *data;
				keAllocMemory(0, 0, (uintptr_t*)&data, secpagecount);
				memset(data, 0, 4096);
				memcpy(data + (pheaders[i].p_vaddr & 0xFFF), (void*)((uintptr_t)addr + pheaders[i].p_offset), pheaders[i].p_filesz);
				keMapProcessMemory(process, data, (pheaders[i].p_vaddr & ~0xFFF), secpagecount);
				keFreeMemory(data, secpagecount);
			//}
		}
	}
	
	return header.e_entry;
}

uintptr_t elfGetFirstFreePage(void *addr)
{
	uintptr_t maxaddr = 0;
	// Load program info
	ElfHeader header;
	memcpy(&header, addr, sizeof(ElfHeader));
	
	// Read section size/position
	ElfProgramHeader *pheaders = (ElfProgramHeader*)((uintptr_t)addr + header.e_phoff);
	uint32_t i;
	for (i = 0; i < header.e_phnum; i++)
	{
		if (pheaders[i].p_type == PT_LOAD)
		{
			uintptr_t secend = (pheaders[i].p_vaddr + pheaders[i].p_memsz + 4095) & ~0xFFF;
			if (secend > maxaddr) maxaddr = secend;
		}
	}
	
	return maxaddr;
}

