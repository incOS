
#include <kernel.h>
#include <rpc.h>
#include <init.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <stdio.h>
#include <list.h>
#include <dirent.h>

#include "elf.h"
#include "devfs.h"

extern void *brk;

void printText(unsigned int x, unsigned int y, char *text);
void printPtr(unsigned int x, unsigned int y, unsigned int ptr);

typedef struct FileSystem
{
	char *path;
	uint32_t pid;
	uint32_t id;
} FileSystem;

typedef struct FileSystemDriver
{
	char *name;
	uint32_t pid;
	uint32_t id;
} FileSystemDriver;

uint32_t fscount = 0;
FileSystem **filesystems = 0;
uint32_t drivercount = 0;
FileSystemDriver **fsdrivers = 0;

void initpRegisterFS(char *path, uint32_t pid, uint32_t id)
{
	// Create list entry
	FileSystem *newfs = malloc(sizeof(FileSystem));
	newfs->path = strdup(path);
	newfs->pid = pid;
	newfs->id = id;
	// Add to list
	filesystems = realloc(filesystems, (fscount + 1) * sizeof(FileSystem*));
	filesystems[fscount] = newfs;
	fscount++;
}
FileSystemDriver *initpGetFSDriver(const char *name)
{
	uint32_t i;
	for (i = 0; i < drivercount; i++)
	{
		if (!strcmp(fsdrivers[i]->name, name)) return fsdrivers[i];
	}
	return 0;
}

uint32_t initpGetPID(uint32_t caller, void *data, uint32_t size)
{
	return caller;
}
uint32_t initpGetFS(uint32_t caller, void *data, uint32_t size)
{
	if (size < 264) return -1;
	char *path = (char*)data + 8;
	// Get file system
	FileSystem *fs = 0;
	uint32_t i;
	for (i = 0; i < fscount; i++)
	{
		if (!strncmp(filesystems[i]->path, path, strlen(filesystems[i]->path)))
		{
			fs = filesystems[i];
			break;
		}
	}
	if (!fs) return -1;
	// Write fs data back
	((uint32_t*)data)[0] = fs->pid;
	((uint32_t*)data)[1] = fs->id;
	memmove(path, path + strlen(fs->path), strlen(path) - strlen(fs->path) + 1);
	return 0;
}
uint32_t initpDebug(uint32_t caller, void *data, uint32_t size)
{
	caller = 0;
	if (data && size)
	{
		char *str = data;
		str[size - 1] = 0;
		keDebug(str);
		keDebug("\n");
	}
	return 1;
}
list_t tobeinitialized = 0;
uint32_t initpInitialized(uint32_t caller, void *data, uint32_t size)
{
	size_t i;
	for (i = 0; i < list_size(tobeinitialized); i++)
	{
		// Mark process as initialized
		uint32_t *process = list_get(tobeinitialized, i);
		if (*process == caller) *process = 0;
	}
	return 0;
}
uint32_t initpRegisterFSDriver(uint32_t caller, void *data, uint32_t size)
{
	keDebug("initpRegisterFSDriver\n");
	// Create list entry
	FileSystemDriver *driver = malloc(sizeof(FileSystemDriver));
	driver->pid = caller;
	if (size < 5) return 0;
	driver->id = *((uint32_t*)data);
	((char*)data)[size - 1] = 0;
	driver->name = strdup((char*)data + 4);
	// Add to list
	fsdrivers = realloc(fsdrivers, (drivercount + 1) * sizeof(FileSystemDriver*));
	fsdrivers[drivercount] = driver;
	drivercount++;
	
	return 1;
}
uint32_t initpFork(uint32_t caller, void *data, uint32_t size)
{
	keDebug("initpFork\n");
	if (size < 8) return -1;
	uint32_t thread = ((uint32_t*)data)[0];
	uint32_t entry = ((uint32_t*)data)[1];
	uint32_t initialized = 0;
	tobeinitialized = list_push(tobeinitialized, &initialized);
	// Create process
	volatile uint32_t newtask = keCloneProcess(caller, thread, entry);
	// Wait for process to initialize
	initialized = newtask;
	int j;
	for (j = 0; j < 50; j++)
	{
		keSleep(10);
		if (!initialized) break;
	}
	for (j = 0; j < (int)list_size(tobeinitialized); j++)
	{
		if (list_get(tobeinitialized, j) == &initialized)
		{
			list_remove(tobeinitialized, j);
			break;
		}
	}
	if (initialized) return -1;
	return newtask;
}
uint32_t initpExec(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
volatile uint32_t rpc_handler_activated = 0;

void initpRPCHandler(void)
{
	rpcHandlerSetActive(1);
	rpc_handler_activated = 1;
	while (1) keWaitForRPC(0xFFFFFFFF);
}

int _start(int *programs)
{
	// Initialize memory manager
	brk = (void*)elfGetFirstFreePage((void*)programs[1]);
	
	tobeinitialized = list_create();
	
	// Initialize RPC handlers
	rpcInit();
	rpcSetHandler(RPC_INIT_GET_PID, initpGetPID);
	rpcSetHandler(RPC_INIT_GET_FS, initpGetFS);
	rpcSetHandler(RPC_INIT_DEBUG, initpDebug);
	rpcSetHandler(RPC_INIT_INITIALIZED, initpInitialized);
	rpcSetHandler(RPC_INIT_REGISTER_FS, initpRegisterFSDriver);
	rpcSetHandler(RPC_INIT_FORK, initpFork);
	rpcSetHandler(RPC_INIT_EXEC, initpExec);
	
	// Create RPC handler thread
	rpc_handler_activated = 0;
	keCreateThread(initpRPCHandler);
	keCreateThread(initpRPCHandler);
	keCreateThread(initpRPCHandler);
	while (!rpc_handler_activated) keSleep(10);
	
	// Initialize /dev
	devfsInit();
	
	// Start initial programs
	int modcount = *programs;
	int i;
	for (i = 1; i < modcount; i++)
	{
		uint32_t process = keCreateProcess();
		uintptr_t entry = elfMapProgram(process, (void*)programs[1 + i * 3], programs[2 + i * 3]);
		volatile uint32_t initialized = process;
		tobeinitialized = list_push(tobeinitialized, &initialized);
		keStartProcess(process, entry);
		int j;
		for (j = 0; j < 50; j++)
		{
			keSleep(10);
			if (!initialized) break;
		}
		for (j = 0; j < (int)list_size(tobeinitialized); j++)
		{
			if (list_get(tobeinitialized, j) == &initialized)
			{
				list_remove(tobeinitialized, j);
				break;
			}
		}
		if (!strcmp((char*)programs[3 + i * 3], "/bin/ramdisk"))
		{
			// Mount root file system
			int file = open("/dev/tty0", O_RDWR);
			write(file, "  Hello world.\n", 15);
	
			FileSystemDriver *ramdisk = initpGetFSDriver("ramdisk");
			if (!ramdisk)
			{
				write(file, "Ramdisk driver not found!\n", 26);
			}
			else
			{
				write(file, "Ramdisk driver found!\n", 22);
				uint32_t *rpcdata;
				keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
				rpcdata[0] = ramdisk->id;
				rpcdata[1] = 0;
				strcpy((char*)&rpcdata[2], "/");
				strcpy((char*)&rpcdata[2] + 256, "");
				uint32_t retval;
				rpcSend(ramdisk->pid, RPC_FS_MOUNT, rpcdata, 520, &retval);
				if ((int)retval == -1)
				{
					write(file, "Could not mount ramdisk!\n", 25);
				}
				else
				{
					write(file, "Mounted ramdisk!\n", 17);
					initpRegisterFS("/", ramdisk->pid, retval);
				}
				keFreeMemory(rpcdata, 1);
				
				// Create initial directory structure
				mknod("/bin", S_IFDIR, 0);
				mknod("/usr", S_IFDIR, 0);
				mknod("/tmp", S_IFDIR, 0);
				mknod("/tmp/test", S_IFDIR, 0);
				mknod("/var", S_IFDIR, 0);
				mknod("/etc", S_IFDIR, 0);
			}
	
			close(file);
		}
	}
	
	// Some RPC tests
	keSleep(100);
	
	// Floppy dist test
	/*file = open("/dev/fd0", O_RDWR);
	char buffer[512];
	lseek(file, 512, 0);
	read(file, buffer, 512);
	lseek(file, 0, 0);
	memset(buffer, 'A', 512);
	write(file, buffer, 512);
	char buffer2[512];
	read(file, buffer2, 512);
	close(file);
	file = open("/dev/tty0", O_RDWR);
	write(file, buffer2, 512);
	close(file);*/
	
	// File system test
	stdout = fopen("/dev/tty0", "w");
	int file = open("/tmp/test/test", O_RDWR | O_CREAT);
	if (file == -1)
	{
		printf("Could not open /tmp/test/test.\n");
	}
	else
	{
		char test[] = "Hello world!\n";
		write(file, test, strlen(test));
		close(file);
		file = open("/tmp/test/test", O_RDWR);
		lseek(file, 4, SEEK_CUR);
		char test2[50];
		int size = read(file, test2, 49);
		if (size != -1) test2[size] = 0;
		close(file);
	
		if (size != -1)
		{
			printf("Data read from file: \"%s\"\n", test2);
		}
		else
		{
			//write(file, "Could not read data from file.\n", 31);
			printf("Could not read data from file.\n");
		}
	}
	
	printf("/dev/*:\n");
	printf("======================\n");
	DIR *dir = opendir("/dev");
	if (!dir)
	{
		printf("Could not open directory.\n");
	}
	else
	{
		struct dirent *dirent;
		while ((dirent = readdir(dir)) != 0)
		{
			printf("%s\n", dirent->d_name);
		}
	}
	printf("======================\n");
	
	keStopThread();
	return 0;
}

