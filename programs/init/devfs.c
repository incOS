/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "devfs.h"

#include <rpc.h>
#include <string.h>
#include <stdlib.h>
#include <init.h>
#include <kernel.h>
#include <fcntl.h>

// TODO: Directories

void initpRegisterFS(char *path, uint32_t pid, uint32_t id);

typedef struct DevfsFile
{
	char *path;
	uint32_t owner;
	uint32_t id;
} DevfsFile;

typedef struct DevfsFileHandle
{
	DevfsFile *file;
	uint32_t fh;
	uint32_t owner;
	int position;
} DevfsFileHandle;

uint32_t filecount = 0;
DevfsFile **files = 0;
uint32_t fhcount = 0;
DevfsFileHandle **filehandles = 0;
uint32_t lastfh = 0;

DevfsFile *devfsGetFileFromPath(const char *path)
{
	uint32_t i;
	for (i = 0; i < filecount; i++)
	{
		if (!strcmp(files[i]->path, path)) return files[i];
	}
	return 0;
}

DevfsFileHandle *devfsGetFileHandle(uint32_t owner, uint32_t fh)
{
	uint32_t i;
	for (i = 0; i < fhcount; i++)
	{
		if (filehandles[i]->fh == fh)
		{
			if (filehandles[i]->owner == owner)
			{
				return filehandles[i];
			}
			else return 0;
		}
	}
	return 0;
}

uint32_t devfsOpen(uint32_t caller, void *data, uint32_t size)
{
	if (size < 264)
		return -1;
	// Get data
	//uint32_t flags = ((uint32_t*)data)[1];
	char *path = (char*)data + 8;
	path[255] = 0;
	DevfsFile *file = devfsGetFileFromPath(path);
	if (!file) return -1;
	// Check permissions
	// TODO
	// Call service which created the file
	((uint32_t*)data)[0] = caller;
	((uint32_t*)data)[1] = file->id;
	uint32_t retval;
	if ((rpcSend(file->owner, RPC_DEVFS_OPEN, data, size, &retval) != 1) || ((int)retval == -1))
	{
		return -1;
	}
	// Open file
	uint32_t fh = ++lastfh;
	DevfsFileHandle *handle = malloc(sizeof(DevfsFileHandle));
	memset(handle, 0, sizeof(DevfsFileHandle));
	handle->file = file;
	handle->fh = fh;
	handle->owner = caller;
	filehandles = realloc(filehandles, (fhcount + 1) * sizeof(DevfsFileHandle*));
	filehandles[fhcount] = handle;
	fhcount++;
	// Write back file handle
	*((uint32_t*)data) = fh;
	keDebug("Opened devfs file.\n");
	return 0;
}
uint32_t devfsClose(uint32_t caller, void *data, uint32_t size)
{
	if (size < 8)
		return -1;
	
	uint32_t fh = ((uint32_t*)data)[1];
	DevfsFileHandle *handle = devfsGetFileHandle(caller, fh);
	if (!handle) return -1;
	
	uint32_t retval = 0;
	if (handle->file)
	{
		// Call service which created the file
		((uint32_t*)data)[0] = caller;
		((uint32_t*)data)[1] = handle->file->id;
		rpcSend(handle->file->owner, RPC_DEVFS_CLOSE, data, size, &retval);
	}
	
	// Delete handle from list
	uint32_t i;
	for (i = 0; i < fhcount; i++)
	{
		if (filehandles[i] == handle)
		{
			for (; i < fhcount - 1; i++)
			{
				filehandles[i] = filehandles[i + 1];
			}
			filehandles = realloc(filehandles, (fhcount - 1) * sizeof(DevfsFileHandle*));
			fhcount--;
			break;
		}
	}
	free(handle);
	
	keDebug("devfsClose.\n");
	if ((int)retval == -1) return -1;
	return 0;
}
uint32_t devfsRead(uint32_t caller, void *data, uint32_t size)
{
	if (size < 12)
		return -1;
	
	// Validate data
	uint32_t fh = ((uint32_t*)data)[1];
	uint32_t datalength = ((uint32_t*)data)[2];
	if (size < datalength + 12) return -1;
	DevfsFileHandle *handle = devfsGetFileHandle(caller, fh);
	if (!handle) return -1;
	if (!handle->file)
	{
		// This is a directory
		if (handle->position < 2)
		{
			strcpy((char*)&((uint32_t*)data)[3], handle->position?"..":".");
			handle->position++;
			return 1;
		}
		if (handle->position - 2 < (int)filecount)
		{
			strcpy((char*)&((uint32_t*)data)[3], files[handle->position - 2]->path);
			handle->position++;
			return 1;
		}
		return -1;
	}

	// Call device service
	uint32_t retval;
	((uint32_t*)data)[0] = caller;
	((uint32_t*)data)[1] = handle->file->id;
	if (rpcSend(handle->file->owner, RPC_DEVFS_READ, data, size, &retval) != 1)
	{
		return -1;
	}
	
	return retval;
}
uint32_t devfsWrite(uint32_t caller, void *data, uint32_t size)
{
	if (size < 12)
		return -1;
	
	// Validate data
	uint32_t fh = ((uint32_t*)data)[1];
	uint32_t datalength = ((uint32_t*)data)[2];
	if (size < datalength + 12) return -1;
	DevfsFileHandle *handle = devfsGetFileHandle(caller, fh);
	if (!handle) return -1;
	if (!handle->file) return -1;

	// Call device service
	uint32_t retval;
	((uint32_t*)data)[0] = caller;
	((uint32_t*)data)[1] = handle->file->id;
	if (rpcSend(handle->file->owner, RPC_DEVFS_WRITE, data, size, &retval) != 1)
	{
		return -1;
	}
	
	return retval;
}
uint32_t devfsRename(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
uint32_t devfsDelete(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
uint32_t devfsIOCTL(uint32_t caller, void *data, uint32_t size)
{
	if (size < 0x1000)
		return -1;
	
	// Validate data
	uint32_t fh = ((uint32_t*)data)[1];
	//uint32_t request = ((uint32_t*)data)[2];
	DevfsFileHandle *handle = devfsGetFileHandle(caller, fh);
	if (!handle) return -1;
	if (!handle->file) return -1;
	
	// Call device service
	uint32_t retval;
	((uint32_t*)data)[0] = caller;
	((uint32_t*)data)[1] = handle->file->id;
	if (rpcSend(handle->file->owner, RPC_DEVFS_IOCTL, data, size, &retval) != 1)
	{
		return -1;
	}
	
	return retval;
}
uint32_t devfsIOCTLInfo(uint32_t caller, void *data, uint32_t size)
{
	if (size < 0x1000)
		return -1;
	
	// Validate data
	uint32_t fh = ((uint32_t*)data)[1];
	//uint32_t request = ((uint32_t*)data)[2];
	DevfsFileHandle *handle = devfsGetFileHandle(caller, fh);
	if (!handle) return -1;
	if (!handle->file) return -1;
	
	// Call device service
	uint32_t retval;
	((uint32_t*)data)[0] = caller;
	((uint32_t*)data)[1] = handle->file->id;
	if (rpcSend(handle->file->owner, RPC_DEVFS_IOCTL_INFO, data, size, &retval) != 1)
	{
		return -1;
	}
	
	return retval;
}
uint32_t devfsOpendir(uint32_t caller, void *data, uint32_t size)
{
	if (size < 264)
		return -1;
	// Get data
	//uint32_t flags = ((uint32_t*)data)[1];
	char *path = (char*)data + 8;
	path[255] = 0;
	if ((strlen(path) > 0) && (path[strlen(path) - 1] == '/'))
		path[strlen(path) - 1] = 0;
	if (strcmp(path, "")) return -1;
	
	// Open directory
	uint32_t fh = ++lastfh;
	DevfsFileHandle *handle = malloc(sizeof(DevfsFileHandle));
	memset(handle, 0, sizeof(DevfsFileHandle));
	handle->fh = fh;
	handle->owner = caller;
	filehandles = realloc(filehandles, (fhcount + 1) * sizeof(DevfsFileHandle*));
	filehandles[fhcount] = handle;
	fhcount++;
	// Write back file handle
	*((uint32_t*)data) = fh;
	keDebug("Opened devfs directory.\n");
	return 0;
}
static uint32_t devfsRewinddir(uint32_t caller, void *data, uint32_t size)
{
	if (size < 8)
		return -1;
	// Get data
	uint32_t fh = ((uint32_t*)data)[1];
	DevfsFileHandle *handle = devfsGetFileHandle(caller, fh);
	if (!handle) return -1;
	if (handle->file) return -1;
	// Set position to the beginning of the directory
	handle->position = 0;
	return 0;
}
uint32_t devfsSeek(uint32_t caller, void *data, uint32_t size)
{
	if (size < 16)
		return -1;
	
	// Validate data
	uint32_t fh = ((uint32_t*)data)[1];
	DevfsFileHandle *handle = devfsGetFileHandle(caller, fh);
	if (!handle) return -1;

	if (!handle->file)
	{
		// This is a directory
		int position = ((uint32_t*)data)[2];
		uint32_t whence = ((uint32_t*)data)[3];
		if (whence == SEEK_SET)
		{
			handle->position = position;
			if (handle->position > (int)filecount + 2)
				handle->position = filecount + 2;
			if (handle->position < 0) handle->position = 0;
		}
		else if (whence == SEEK_CUR)
		{
			handle->position += position;
			if (handle->position > (int)filecount + 2)
				handle->position = filecount + 2;
			if (handle->position < 0) handle->position = 0;
		}
		else if (whence == SEEK_END)
		{
			handle->position = filecount + 2 + position;
			if (handle->position > (int)filecount + 2)
				handle->position = filecount + 2;
			if (handle->position < 0) handle->position = 0;
		}
		else return -1;
		return handle->position;
	}

	// Call device service
	uint32_t retval;
	((uint32_t*)data)[0] = caller;
	((uint32_t*)data)[1] = handle->file->id;
	if (rpcSend(handle->file->owner, RPC_DEVFS_SEEK, data, 16, &retval) != 1)
	{
		return -1;
	}
	
	return retval;
}
uint32_t devfsTruncate(uint32_t caller, void *data, uint32_t size)
{
	return -1;
}
uint32_t devfsClone(uint32_t caller, void *data, uint32_t size)
{
	if (size < 8)
		return -1;
	// Get file
	uint32_t fh = ((uint32_t*)data)[1];
	DevfsFileHandle *handle = 0;
	uint32_t i;
	for (i = 0; i < fhcount; i++)
	{
		if (filehandles[i]->fh == fh)
		{
			handle = filehandles[i];
		}
	}
	if (!handle) return -1;
	// Check permissions
	// TODO
	// TODO: Call driver about this
	// Open file
	fh = ++lastfh;
	DevfsFileHandle *newhandle = malloc(sizeof(DevfsFileHandle));
	newhandle->file = handle->file;
	newhandle->fh = fh;
	newhandle->owner = caller;
	filehandles = realloc(filehandles, (fhcount + 1) * sizeof(DevfsFileHandle*));
	filehandles[fhcount] = newhandle;
	fhcount++;
	// Write back file handle
	((uint32_t*)data)[1] = newhandle->fh;
	keDebug("Cloned devfs file.\n");
	return 0;
}

uint32_t devfsFileCreate(uint32_t caller, void *data, uint32_t size)
{
	if (size < 260)
		return 0;
	
	// Get data
	uint32_t id = *((uint32_t*)data);
	char *path = (char*)data + 4;
	path[255] = 0;
	if (strchr(path, '/')) return -1;
	if (devfsGetFileFromPath(path)) return -1;
	// Add file to file list
	DevfsFile *newfile = malloc(sizeof(DevfsFile));
	newfile->path = malloc(strlen(path) + 1);
	strcpy(newfile->path, path);
	newfile->owner = caller;
	newfile->id = id;
	files = realloc(files, (filecount + 1) * sizeof(DevfsFile*));
	files[filecount] = newfile;
	filecount++;
	return 1;
}
uint32_t devfsFileDelete(uint32_t caller, void *data, uint32_t size)
{
	if (size < 4)
		return 0;
	
	// Search for file
	uint32_t id = *((uint32_t*)data);
	uint32_t i;
	DevfsFile *file = 0;
	uint32_t fileindex = 0;
	for (i = 0; i < filecount; i++)
	{
		if ((files[i]->id == id) && (files[i]->owner == caller))
		{
			file = files[i];
			fileindex = i;
			break;
		}
	}
	if (!file) return 0;
	
	// Remove file entry
	for (i = fileindex; i < filecount - 1; i++)
	{
		files[i] = files[i + 1];
	}
	files = realloc(files, (filecount - 1) * sizeof(DevfsFile*));
	filecount--;
	// Close open file handles
	for (i = 0; i < fhcount; i++)
	{
		if (filehandles[i]->file == file)
		{
			filehandles[i]->file = 0;
		}
	}
	// Remove file
	free(file->path);
	free(file);
	return 0;
}

void devfsInit(void)
{
	// Register FS handlers
	rpcSetHandler(RPC_FS_OPEN, devfsOpen);
	rpcSetHandler(RPC_FS_CLOSE, devfsClose);
	rpcSetHandler(RPC_FS_READ, devfsRead);
	rpcSetHandler(RPC_FS_WRITE, devfsWrite);
	rpcSetHandler(RPC_FS_RENAME, devfsRename);
	rpcSetHandler(RPC_FS_DELETE, devfsDelete);
	rpcSetHandler(RPC_FS_IOCTL, devfsIOCTL);
	rpcSetHandler(RPC_FS_IOCTL_INFO, devfsIOCTLInfo);
	rpcSetHandler(RPC_FS_OPENDIR, devfsOpendir);
	rpcSetHandler(RPC_FS_REWINDDIR, devfsRewinddir);
	rpcSetHandler(RPC_FS_SEEK, devfsSeek);
	rpcSetHandler(RPC_FS_TRUNCATE, devfsTruncate);
	rpcSetHandler(RPC_FS_CLONE, devfsClone);
	// Register devfs handlers
	rpcSetHandler(RPC_INIT_DEVFS_CREATE, devfsFileCreate);
	rpcSetHandler(RPC_INIT_DEVFS_DELETE, devfsFileDelete);
	
	initpRegisterFS("/dev/", INIT_PID, 0);
}

