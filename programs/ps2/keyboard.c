#include <stdio.h>
#include <stdint.h>
#include <cdi/io.h>
#include <kernel.h>
#include "keyboard.h"

static void kbIRQHandler()
{
	uint8_t data;
	data = cdi_inb(0x60);
	
	// DEBUG:
	printf("%02X\n", data);

	keExitIRQ();	
}

void kbRegisterIRQ()
{
	// Register IRQ
	keRegisterIRQHandler(0x01, kbIRQHandler);
}

int main(void)
{
	kbRegisterIRQ();
	while(1);
	return 0;
}

