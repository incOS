
#include <kernel.h>
#include <stdio.h>

int main(int argc, char **argv);

int _start(void)
{
	stdout = fopen("/dev/tty0", "w");
	if (!stdout) keDebug("Could not open stdout.\n");
	stderr = fopen("/dev/tty0", "w");
	stdin = fopen("/dev/tty0", "r");
	main(0, 0);
	fclose(stdout);
	fclose(stderr);
	fclose(stdin);
	keExit();
	return 0;
}
