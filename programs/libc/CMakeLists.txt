
set (LIBC_SRC 
bsdmalloc.c
files.c
stdio.c
stdlib.c
string.c
time.c
fork.c
)

add_library(c STATIC ${LIBC_SRC})

add_program(c)

