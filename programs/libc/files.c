/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <fcntl.h>
#include <unistd.h>
#include <rpc.h>
#include <init.h>
#include <kernel.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>

typedef struct FileHandle
{
	uint32_t fh;
	uint32_t fsfh;
	pid_t fs_pid;
	uint32_t fs;
} FileHandle;

struct DIR
{
	uint32_t fsfh;
	pid_t fs_pid;
	uint32_t fs;
	struct dirent dirent;
};

static uint32_t fhcount = 0;
static FileHandle **filehandles = 0;
static uint32_t lastfh = 0;

void forkCloneAllFiles(void)
{
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	
	uint32_t i;
	for (i = 0; i < fhcount; i++)
	{
		rpcdata[0] = filehandles[i]->fs;
		rpcdata[1] = filehandles[i]->fsfh;
		rpcSend(filehandles[i]->fs_pid, RPC_FS_CLONE, rpcdata, 8, &retval);
		filehandles[i]->fsfh = rpcdata[1];
	}
	
	keFreeMemory(rpcdata, 1);
}

static FileHandle *getFileHandle(int fd)
{
	uint32_t i;
	for (i = 0; i < fhcount; i++)
	{
		if (filehandles[i]->fh == (uint32_t)fd) return filehandles[i];
	}
	return 0;
}

int open(const char *path, int oflag, ...)
{
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	
	// Get file system
	strncpy((char*)&rpcdata[2], path, 256);
	((char*)&rpcdata[2])[255] = 0;
	if ((rpcSend(INIT_PID, RPC_INIT_GET_FS, rpcdata, 264, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, 1);
		return -1;
	}
	
	// Open file
	pid_t fspid = rpcdata[0];
	pid_t fs = rpcdata[1];
	rpcdata[0] = fs;
	rpcdata[1] = oflag;
	if ((rpcSend(fspid, RPC_FS_OPEN, rpcdata, 264, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, 1);
		return -1;
	}
	
	// Add to file list
	FileHandle *newfh = malloc(sizeof(FileHandle));
	newfh->fh = ++lastfh;
	newfh->fsfh = rpcdata[0];
	newfh->fs_pid = fspid;
	newfh->fs = fs;
	filehandles = realloc(filehandles, (fhcount + 1) * sizeof(FileHandle*));
	filehandles[fhcount] = newfh;
	fhcount++;
	
	keFreeMemory(rpcdata, 1);
	
	return newfh->fh;
}

int creat(const char *path, mode_t mode)
{
	return open(path, O_CREAT | O_TRUNC | O_WRONLY, mode);
}

ssize_t write(int fildes, const void *buf, size_t nbyte)
{
	FileHandle *fh = getFileHandle(fildes);
	if (!fh) return -1;
	
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, (nbyte + 12 + 4095) / 0x1000);
	
	rpcdata[0] = fh->fs;
	rpcdata[1] = fh->fsfh;
	rpcdata[2] = nbyte;
	memcpy(&rpcdata[3], buf, nbyte);
	if ((rpcSend(fh->fs_pid, RPC_FS_WRITE, rpcdata, 12 + nbyte, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, (nbyte + 12 + 4095) / 0x1000);
		return -1;
	}
	
	keFreeMemory(rpcdata, (nbyte + 12 + 4095) / 0x1000);
	
	return retval;
}

ssize_t read(int fildes, void *buf, size_t nbyte)
{
	FileHandle *fh = getFileHandle(fildes);
	if (!fh) return -1;
	
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, (nbyte + 12 + 4095) / 0x1000);
	
	rpcdata[0] = fh->fs;
	rpcdata[1] = fh->fsfh;
	rpcdata[2] = nbyte;
	if ((rpcSend(fh->fs_pid, RPC_FS_READ, rpcdata, 12 + nbyte, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, (nbyte + 12 + 4095) / 0x1000);
		return -1;
	}
	memcpy(buf, &rpcdata[3], nbyte);
	
	keFreeMemory(rpcdata, (nbyte + 12 + 4095) / 0x1000);
	
	return retval;
}

int close(int fildes)
{
	FileHandle *fh = getFileHandle(fildes);
	if (!fh) return -1;
	
	// Send close message to file system service
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = fh->fs;
	rpcdata[1] = fh->fsfh;
	rpcSend(fh->fs_pid, RPC_FS_CLOSE, rpcdata, 8, &retval);
	keFreeMemory(rpcdata, 1);
	
	// Delete item from list
	uint32_t i;
	for (i = 0; i < fhcount; i++)
	{
		if (filehandles[i] == fh)
		{
			for (; i < fhcount - 1; i++)
			{
				filehandles[i] = filehandles[i + 1];
			}
			filehandles = realloc(filehandles, (fhcount - 1) * sizeof(FileHandle*));
			fhcount--;
			break;
		}
	}
	free(fh);
	
	return 0;
}

off_t lseek(int fildes, off_t offset, int whence)
{
	FileHandle *fh = getFileHandle(fildes);
	if (!fh) return -1;

	// Send seek message to file system service
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = fh->fs;
	rpcdata[1] = fh->fsfh;
	rpcdata[2] = offset;
	rpcdata[3] = whence;
	if (rpcSend(fh->fs_pid, RPC_FS_SEEK, rpcdata, 16, &retval) != 1)
	{
		keFreeMemory(rpcdata, 1);
		return -1;
	}
	keFreeMemory(rpcdata, 1);
	
	return retval;
}

int mknod(const char *path, mode_t mode, dev_t dev)
{
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	
	// Get file system
	strncpy((char*)&rpcdata[2], path, 256);
	((char*)&rpcdata[2])[255] = 0;
	if ((rpcSend(INIT_PID, RPC_INIT_GET_FS, rpcdata, 264, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, 1);
		return -1;
	}
	pid_t fspid = rpcdata[0];
	pid_t fs = rpcdata[1];
	
	// Create node
	rpcdata[0] = fs;
	rpcdata[1] = mode;
	if (rpcSend(fspid, RPC_FS_MKNOD, rpcdata, 264, &retval) != 1)
	{
		keFreeMemory(rpcdata, 1);
		return -1;
	}
	
	keFreeMemory(rpcdata, 1);
	dev = 0;
	return retval;
}
int mkdir(const char *path, mode_t mode)
{
	return mknod(path, mode | S_IFDIR, 0);
}

DIR *opendir(const char *path)
{
	if (!strcmp(path, "")) return 0;
	char *tmppath = malloc(strlen(path) + 1);
	strcpy(tmppath, path);
	if (tmppath[strlen(path) - 1] != '/')
	{
		tmppath[strlen(path)] = '/';
		tmppath[strlen(path) + 1] = 0;
	}
	
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	
	// Get file system
	strncpy((char*)&rpcdata[2], tmppath, 256);
	free(tmppath);
	((char*)&rpcdata[2])[255] = 0;
	if ((rpcSend(INIT_PID, RPC_INIT_GET_FS, rpcdata, 264, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, 1);
		return 0;
	}
	
	// Open file
	pid_t fspid = rpcdata[0];
	pid_t fs = rpcdata[1];
	rpcdata[0] = fs;
	if ((rpcSend(fspid, RPC_FS_OPENDIR, rpcdata, 264, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, 1);
		return 0;
	}
	
	// Create struct
	struct DIR *dir = malloc(sizeof(struct DIR));
	dir->fsfh = rpcdata[0];
	dir->fs_pid = fspid;
	dir->fs = fs;
	
	keFreeMemory(rpcdata, 1);
	return dir;
}
int closedir(DIR *dir)
{
	if (!dir) return -1;
	// Send close message to file system service
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = dir->fs;
	rpcdata[1] = dir->fsfh;
	rpcSend(dir->fs_pid, RPC_FS_CLOSE, rpcdata, 8, &retval);
	keFreeMemory(rpcdata, 1);
	
	free(dir);
	return 0;
}
struct dirent *readdir(DIR *dir)
{
	if (!dir) return 0;
	
	// Read data from directory file
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	
	rpcdata[0] = dir->fs;
	rpcdata[1] = dir->fsfh;
	rpcdata[2] = NAME_MAX + 1;
	if ((rpcSend(dir->fs_pid, RPC_FS_READ, rpcdata, 12 + NAME_MAX + 1, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, 1);
		return 0;
	}
	memcpy(dir->dirent.d_name, &rpcdata[3], NAME_MAX + 1);
	
	keFreeMemory(rpcdata, 1);
	if (retval != 1) return 0;
	return &dir->dirent;
}
void rewinddir(DIR *dir)
{
	// Send rewind message to file system service
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = dir->fs;
	rpcdata[1] = dir->fsfh;
	rpcSend(dir->fs_pid, RPC_FS_REWINDDIR, rpcdata, 8, 0);
	keFreeMemory(rpcdata, 1);
}
void seekdir(DIR *dir, long index)
{
	// Send seek message to file system service
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = dir->fs;
	rpcdata[1] = dir->fsfh;
	rpcdata[2] = index;
	rpcdata[3] = SEEK_SET;
	rpcSend(dir->fs_pid, RPC_FS_SEEK, rpcdata, 16, &retval);
	keFreeMemory(rpcdata, 1);
}
long telldir(DIR *dir)
{
	// Send seek message to file system service
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = dir->fs;
	rpcdata[1] = dir->fsfh;
	rpcdata[2] = 0;
	rpcdata[3] = SEEK_CUR;
	if (rpcSend(dir->fs_pid, RPC_FS_SEEK, rpcdata, 16, &retval) != 1)
	{
		keFreeMemory(rpcdata, 1);
		return -1;
	}
	keFreeMemory(rpcdata, 1);
	return retval;
}

int ioctl(int fd, int request, ...)
{
	FileHandle *fh = getFileHandle(fd);
	if (!fh) return -1;
	
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	
	// Get ioctl parameter info
	rpcdata[0] = fh->fs;
	rpcdata[1] = fh->fsfh;
	rpcdata[2] = request;
	if ((rpcSend(fh->fs_pid, RPC_FS_IOCTL_INFO, rpcdata, 0x1000, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, 1);
		return -1;
	}
	int paraminfo[1021];
	memcpy(paraminfo, &rpcdata[3], 1021 * 4);
	keFreeMemory(rpcdata, 1);
	
	// Get parameter size
	int paramcount = paraminfo[0];
	int *params = &request + 1;
	int paramsize = 0;
	int i;
	for (i = 0; i < paramcount; i++)
	{
		if (paraminfo[i + 1])
			paramsize += paraminfo[i + 1];
		else
			paramsize += 4;
	}
	
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, (paramsize + 12 + 4095) / 0x1000);
	// Fill ioctl request data
	char *paramdata = (char*)&rpcdata[3];
	rpcdata[0] = fh->fs;
	rpcdata[1] = fh->fsfh;
	rpcdata[2] = request;
	for (i = 0; i < paramcount; i++)
	{
		if (paraminfo[i + 1])
		{
			memcpy(paramdata, (void*)params[i], paraminfo[i + 1]);
			paramdata += paraminfo[i + 1];
		}
		else
		{
			*((int*)paramdata) = params[i];
			paramdata += 4;
		}
	}
	
	// Call ioctl RPC
	if ((rpcSend(fh->fs_pid, RPC_FS_IOCTL, rpcdata, paramsize + 12, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, (paramsize + 12 + 4095) / 0x1000);
		return -1;
	}
	
	// Copy result back
	paramdata = (char*)&rpcdata[3];
	for (i = 0; i < paramcount; i++)
	{
		if (paraminfo[i + 1])
		{
			memcpy((void*)params[i], paramdata, paraminfo[i + 1]);
			paramdata += paraminfo[i + 1];
		}
		else
		{
			paramdata += 4;
		}
	}
	
	keFreeMemory(rpcdata, (paramsize + 12 + 4095) / 0x1000);
	return retval;
}

int unlink(const char *filename)
{
	// TODO
	return -1;
}

int pipe(int fd[2])
{
	return -1;
}

