/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <unistd.h>
#include <init.h>
#include <rpc.h>
#include <kernel.h>

void forkCloneAllFiles(void);

static pid_t forkEntry(void)
{
	keDebug("Forked process!\n");
	// Duplicate file descriptors
	forkCloneAllFiles();
	// Register forked process with init
	initSendInitialized();
	asm volatile(
		"movl $0x0, 0x1c(%ebp)\n"
		"leave\n"
		"ret\n");
	return 0;
}

pid_t fork(void)
{
	// Get thread ID
	uint32_t thread = 0;
	keGetProcessInfo(0, &thread);
	// Fork process
	uint32_t retval;
	uint32_t *rpcdata;
	keAllocMemory(0, 0, (uintptr_t*)&rpcdata, 1);
	rpcdata[0] = thread;
	rpcdata[1] = (uint32_t)forkEntry + 3;
	if ((rpcSend(INIT_PID, RPC_INIT_FORK, rpcdata, 8, &retval) != 1) || ((int)retval == -1))
	{
		keFreeMemory(rpcdata, 1);
		return -1;
	}
	keFreeMemory(rpcdata, 1);

	return retval;
}

