/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <kernel.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

#define ERROR_EOF 1
#define ERROR_ERROR 2

struct FILE
{
	int fd;
	int append;
	int error;
};

FILE *stderr = 0;
FILE *stdout = 0;
FILE *stdin = 0;

FILE *fopen(const char *filename, const char *mode)
{
	// Open file
	int openmode = 0;
	if (strchr(mode, 'r')) openmode = O_RDONLY;
	else if (strchr(mode, 'w')) openmode = O_WRONLY | O_CREAT;
	else if (strchr(mode, 'a')) openmode = O_WRONLY | O_CREAT;
	if (strchr(mode, '+')) openmode = (openmode & ~O_ACCMODE) | O_RDWR;
	int fd = open(filename, openmode);
	if (fd == -1) return 0;
	// Return file structure
	FILE *file = malloc(sizeof(FILE));
	memset(file, 0, sizeof(FILE));
	file->fd = fd;
	if (strchr(mode, 'a')) file->append = 1;
	return file;
}
int fclose(FILE *file)
{
	if (!file) return EOF;
	close(file->fd);
	free(file);
	return 0;
}
FILE *freopen(const char *filename, const char *mode, FILE *file)
{
	if (fclose(file) == EOF) return 0;
	return fopen(filename, mode);
}
int feof(FILE *file)
{
	return (file->error & ERROR_EOF) != 0;
}
int ferror(FILE *file)
{
	return (file->error & ERROR_ERROR) != 0;
}
int fseek(FILE *file, long int offset, int origin)
{
	if (!file) return EOF;
	file->error = 0;
	if (lseek(file->fd, offset, origin) == (off_t)-1) return EOF;
	return 0;
}
long int ftell(FILE *file)
{
	if (!file) return EOF;
	long int pos = lseek(file->fd, 0, SEEK_CUR);
	return pos;
}
void rewind(FILE *file)
{
	fseek(file, 0, SEEK_SET);
}
void clearerr(FILE *file)
{
	file->error = 0;
}

int fgetpos(FILE *file, fpos_t *pos)
{
	if (!file || !pos) return EOF;

	long int position = lseek(file->fd, 0, SEEK_CUR);
	if (position == -1)
	{
		*pos = 0;
		return EOF;
	}
	*pos = position;
	return 0;
}
int fsetpos(FILE *file, const fpos_t *pos)
{
	if (!file || !pos) return EOF;
	file->error = 0;
	if (lseek(file->fd, *pos, SEEK_SET) == (off_t)-1) return -1;
	return 0;
}

int fgetc(FILE *file)
{
	if (!file) return EOF;
	char c = 0;
	int size = read(file->fd, &c, 1);
	if (size == 0)
	{
		file->error = ERROR_EOF;
		return EOF;
	}
	else if (size == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return c;
}
char *fgets(char *str, int num, FILE *file)
{
	int n = 0;
	while (n < num)
	{
		char c = fgetc(file);
		if ((c == EOF) && ferror(file)) return 0;
		if ((c == EOF) && (n == 0))
		{
			return 0;
		}
		else if ((c == EOF) || (c == '\n') || (c == 0))
		{
			str[n] = 0;
			return str;
		}
		else
		{
			str[n] = c;
		}
		n++;
	}
	str[n - 1] = 0;
	return str;
}
int fscanf(FILE *file, const char *fmt, ...)
{
	return -1;
}
size_t fread(void *ptr, size_t size, size_t count, FILE *file)
{
	if (!file) return 0;
	int bytecount = read(file->fd, ptr, size * count);
	if (bytecount == -1)
	{
		file->error = ERROR_ERROR;
		return 0;
	}
	if (bytecount != (int)(size * count))
	{
		file->error = ERROR_EOF;
	}
	return bytecount;
}

int fputc(int c, FILE *file)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	int size = write(file->fd, &c, 1);
	if (size == 0)
	{
		file->error = ERROR_EOF;
		return EOF;
	}
	else if (size == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return c;
}
int fputs(const char *s, FILE *file)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	int size = write(file->fd, s, strlen(s));
	if (size == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return size;
}
int vfprintf(FILE *file, const char *fmt, va_list args)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	// FIXME: Greater length?
	char buffer[1024];
	vsnprintf(buffer, 1024, fmt, args);

	int written = write(file->fd, buffer, strlen(buffer));
	if (written == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return written;
}
int fprintf(FILE *file, const char *fmt, ...)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	// FIXME: Greater length?
	char buffer[1024];
	va_list args;
	va_start(args, fmt);
	vsnprintf(buffer, 1024, fmt, args);
	va_end(args);

	int written = write(file->fd, buffer, strlen(buffer));
	if (written == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return written;
}
size_t fwrite(const void *ptr, size_t size, size_t count, FILE *file)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	int written = write(file->fd, ptr, size * count);
	if (written == -1) return EOF;
	return written;
}

FILE *tmpfile(void)
{
	// TODO
	return 0;
}
char *tmpnam(char *s)
{
	// TODO
	return s;
}

int ungetc(int character, FILE *file)
{
	return EOF;
}

int rename(const char *oldname, const char *newname)
{
	return -1;
}
int remove(const char *filename)
{
	return -1;
}

int printf(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int status = vfprintf(stdout, fmt, args);
	va_end(args);
	return status;
}
int puts(const char *s)
{
	printf("%s\n", s);
	return 0;
}

int snprintf(char *buf, size_t n, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int retval = vsnprintf(buf, n, fmt, args);
	va_end(args);
	return retval;
}
int sprintf(char *buf, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int retval = vsprintf(buf, fmt, args);
	va_end(args);
	return retval;
}

inline void write_char(char **buf, const char **fmt, char c)
{
	**buf = c;
	(*buf)++;
	if (fmt) (*fmt)++;
}

static uint32_t write_integer(char **buf, size_t n, int i, int base, int prefix, int leftjustify, int forcesign, int insertspace, int zeros, int minimumwidth)
{
	int negative = 0;
	if (i < 0)
	{
		negative = 1;
		i = -i;
	}
	char tmp[20];
	tmp[0] = 0;
	int numberlength = 0;
	do
	{
		tmp[numberlength] = (i % base) + '0';
		numberlength++;
		tmp[numberlength] = 0;
		i /= base;
	}
	while (i);
	if (prefix && (base == 8))
	{
		tmp[numberlength] = '0';
		numberlength++;
		tmp[numberlength] = 0;
	}
	int needed = numberlength;
	if (forcesign || insertspace || negative) needed++;
	
	uint32_t written = 0;
	if (leftjustify || ((minimumwidth <= needed) && !zeros))
	{
		// Just print number
		if (negative)
		{
			write_char(buf, 0, '-');
			written++;
			if (written == n) return n;
		}
		else if (insertspace)
		{
			write_char(buf, 0, ' ');
			written++;
			if (written == n) return n;
		}
		else if (forcesign)
		{
			write_char(buf, 0, '+');
			written++;
			if (written == n) return n;
		}
		int idx;
		for (idx = 0; idx < numberlength; idx++)
		{
			write_char(buf, 0, tmp[numberlength - idx - 1]);
			written++;
			if (written == n) return n;
		}
		
		if (minimumwidth > needed)
		{
			for (idx = 0; idx < minimumwidth - needed; idx++)
			{
				write_char(buf, 0, ' ');
				written++;
				if (written == n) return n;
			}
			return minimumwidth;
		}
		else
		{
			return needed;
		}
	}
	else
	{
		if (zeros)
		{
			if (negative)
			{
				write_char(buf, 0, '-');
				written++;
				if (written == n) return n;
			}
			else if (insertspace)
			{
				write_char(buf, 0, ' ');
				written++;
				if (written == n) return n;
			}
			else if (forcesign)
			{
				write_char(buf, 0, '+');
				written++;
				if (written == n) return n;
			}
			
			int idx;
			for (idx = 0; idx < minimumwidth - needed; idx++)
			{
				write_char(buf, 0, '0');
				written++;
				if (written == n) return n;
			}
			for (idx = 0; idx < numberlength; idx++)
			{
				write_char(buf, 0, tmp[numberlength - idx - 1]);
				written++;
				if (written == n) return n;
			}
		}
		else
		{
			int idx;
			for (idx = 0; idx < minimumwidth - needed; idx++)
			{
				write_char(buf, 0, ' ');
				written++;
				if (written == n) return n;
			}
			if (negative)
			{
				write_char(buf, 0, '-');
				written++;
				if (written == n) return n;
			}
			else if (insertspace)
			{
				write_char(buf, 0, ' ');
				written++;
				if (written == n) return n;
			}
			else if (forcesign)
			{
				write_char(buf, 0, '+');
				written++;
				if (written == n) return n;
			}
			for (idx = 0; idx < numberlength; idx++)
			{
				write_char(buf, 0, tmp[numberlength - idx - 1]);
				written++;
				if (written == n) return n;
			}
		}
		return minimumwidth;
	}
}

static char digits[] = "0123456789abcdef";
static char digits_capital[] = "0123456789ABCDEF";

static uint32_t write_unsigned_integer(char **buf, size_t n, unsigned int i,
	int base, int prefix, int capital, int leftjustify, int forcesign, int insertspace, int zeros, int minimumwidth)
{
	if (!i)
	{
		write_char(buf, 0, '0');
		return 1;
	}
	char tmp[20];
	tmp[0] = 0;
	uint32_t written = 0;
	int numberlength = 0;
	while (i)
	{
		if (capital)
		{
			tmp[numberlength] = digits_capital[i % base];
		}
		else
		{
			tmp[numberlength] = digits[i % base];
		}
		numberlength++;
		tmp[numberlength] = 0;
		i /= base;
	}
	if (prefix && (base == 8))
	{
		tmp[numberlength] = '0';
		numberlength++;
		tmp[numberlength] = 0;
	}
	else if (prefix && (base == 16) && !zeros)
	{
		tmp[numberlength] = capital?'X':'x';
		numberlength++;
		tmp[numberlength] = '0';
		numberlength++;
		tmp[numberlength] = 0;

	}
	else if (prefix && (base == 16))
	{
		**buf = '0';
		(*buf)++;
		written++;
		if (written == n) return n;
		**buf = capital?'X':'x';
		(*buf)++;
		written++;
		if (written == n) return n;
		minimumwidth -= 2;
	}
	int needed = numberlength;
	if (forcesign || insertspace) needed++;
	
	if (leftjustify || ((minimumwidth <= needed) && !zeros))
	{
		// Just print number
		if (insertspace)
		{
			**buf = ' ';
			(*buf)++;
			written++;
			if (written == n) return n;
		}
		else if (forcesign)
		{
			**buf = '+';
			(*buf)++;
			written++;
			if (written == n) return n;
		}
		int idx;
		for (idx = 0; idx < numberlength; idx++)
		{
			**buf = tmp[numberlength - idx - 1];
			(*buf)++;
			written++;
			if (written == n) return n;
		}
		
		if (minimumwidth > needed)
		{
			for (idx = 0; idx < minimumwidth - needed; idx++)
			{
				**buf = ' ';
				(*buf)++;
				written++;
				if (written == n) return n;
			}
			return minimumwidth;
		}
		else
		{
			return needed;
		}
	}
	else
	{
		if (zeros)
		{
			if (insertspace)
			{
				**buf = ' ';
				(*buf)++;
				written++;
				if (written == n) return n;
			}
			else if (forcesign)
			{
				**buf = '+';
				(*buf)++;
				written++;
				if (written == n) return n;
			}
			
			int idx;
			for (idx = 0; idx < minimumwidth - needed; idx++)
			{
				**buf = '0';
				(*buf)++;
				written++;
				if (written == n) return n;
			}
			for (idx = 0; idx < numberlength; idx++)
			{
				**buf = tmp[numberlength - idx - 1];
				(*buf)++;
				written++;
				if (written == n) return n;
			}
		}
		else
		{
			int idx;
			for (idx = 0; idx < minimumwidth - needed; idx++)
			{
				**buf = ' ';
				(*buf)++;
				written++;
				if (written == n) return n;
			}
			if (insertspace)
			{
				**buf = ' ';
				(*buf)++;
				written++;
				if (written == n) return n;
			}
			else if (forcesign)
			{
				**buf = '+';
				(*buf)++;
				written++;
				if (written == n) return n;
			}
			for (idx = 0; idx < numberlength; idx++)
			{
				**buf = tmp[numberlength - idx - 1];
				(*buf)++;
				written++;
				if (written == n) return n;
			}
		}
		return minimumwidth;
	}
}

static uint32_t parse_expression(char **buf, const char **fmt, size_t n, size_t oldn, va_list *ap)
{
	if (n == 0) return 0;
	
	int leftjustify = 0;
	int forcesign = 0;
	int prefix = 0;
	int zeros = 0;
	int insertspace = 0;
	int minimumwidth = 0;
	int minimumwidth2 = 0;
	int length = 0;
	while (**fmt)
	{
		switch (**fmt)
		{
			case '-':
				leftjustify = 1;
				break;
			case '+':
				forcesign = 1;
				break;
			case '#':
				prefix = 1;
				break;
			case ' ':
				insertspace = 1;
				break;
			case '0':
				if (!minimumwidth)
					zeros = 1;
				else
					minimumwidth = minimumwidth * 10;
				break;
			case '1' ... '9':
				minimumwidth = minimumwidth * 10 + (**fmt - '0');
				break;
			case '*':
				minimumwidth2 = va_arg(*ap, int);
				break;
			case 'h':
				length--;
				break;
			case 'l':
				length++;
				break;
			case '%':
				write_char(buf, fmt, '%');
				return 1;
			case 'c':
				write_char(buf, fmt, va_arg(*ap, char));
				return 1;
			case 'd':
			case 'i':
				{
					(*fmt)++;
					int i = 0;
					if (length == 0) i = va_arg(*ap, int);
					else if (length == -1) i = (short)va_arg(*ap, int);
					else if (length == -2) i = (char)va_arg(*ap, int);
					else i = va_arg(*ap, int);
					
					if (minimumwidth2) minimumwidth = minimumwidth2;
					return write_integer(buf, n, i, 10, 0, leftjustify, forcesign, insertspace, zeros, minimumwidth);
				}
			case 'o':
				{
					(*fmt)++;
					int i = 0;
					if (length == 0) i = va_arg(*ap, int);
					else if (length == -1) i = (short)va_arg(*ap, int);
					else if (length == -2) i = (char)va_arg(*ap, int);
					else i = va_arg(*ap, int);
					
					if (minimumwidth2) minimumwidth = minimumwidth2;
					return write_integer(buf, n, i, 8, prefix, leftjustify, forcesign, insertspace, zeros, minimumwidth);
				}
			case 's':
				{
					(*fmt)++;
					char *s = va_arg(*ap, char*);
					uint32_t written = 0;
					while (*s && written < n)
					{
						**buf = *s;
						(*buf)++;
						written++;
						s++;
					}
					return written;
				}
			case 'u':
				{
					(*fmt)++;
					unsigned int i = 0;
					if (length == 0) i = va_arg(*ap, int);
					else if (length == -1) i = (unsigned short)va_arg(*ap, int);
					else if (length == -2) i = (unsigned char)va_arg(*ap, int);
					else i = va_arg(*ap, int);
					
					if (minimumwidth2) minimumwidth = minimumwidth2;
					return write_unsigned_integer(buf, n, i, 10, 0, 0, leftjustify, forcesign, insertspace, zeros, minimumwidth);
				}
			case 'x':
				{
					(*fmt)++;
					unsigned int i = 0;
					if (length == 0) i = va_arg(*ap, int);
					else if (length == -1) i = (unsigned short)va_arg(*ap, int);
					else if (length == -2) i = (unsigned char)va_arg(*ap, int);
					else i = va_arg(*ap, int);
					
					if (minimumwidth2) minimumwidth = minimumwidth2;
					return write_unsigned_integer(buf, n, i, 16, prefix, 0, leftjustify, forcesign, insertspace, zeros, minimumwidth);
				}
			case 'X':
				{
					(*fmt)++;
					unsigned int i = 0;
					if (length == 0) i = va_arg(*ap, int);
					else if (length == -1) i = (unsigned short)va_arg(*ap, int);
					else if (length == -2) i = (unsigned char)va_arg(*ap, int);
					else i = va_arg(*ap, int);
					
					if (minimumwidth2) minimumwidth = minimumwidth2;
					return write_unsigned_integer(buf, n, i, 16, prefix, 1, leftjustify, forcesign, insertspace, zeros, minimumwidth);
				}
			case 'p':
				{
					(*fmt)++;
					unsigned int i = va_arg(*ap, unsigned int);
					return write_unsigned_integer(buf, n, i, 16, 1, 0, 0, 0, 0, 1, 10);
				}
			case 'n':
				{
					(*fmt)++;
					int *target = va_arg(*ap, int*);
					*target = oldn - n;
					return 0;
				}
			default:
				return 0;
		}
		(*fmt)++;
	}
	
	return 0;
}

int vsnprintf(char *buf, size_t n, const char *fmt, va_list arg)
{
	if (n == 0) return 0;
	uint32_t written = 0;
	while (*fmt && (written < n))
	{
		switch (*fmt)
		{
			case '%':
				fmt++;
				// TODO
				written += parse_expression(&buf, &fmt, n - written, n, &arg);
				break;
			default:
				*buf = *fmt;
				buf++;
				fmt++;
				written++;
				break;
		}
	}
	if (written < n)
	{
		*buf = 0;
		written++;
	}
	else
	{
		buf--;
		*buf = 0;
	}
	return written;
}
int vsprintf(char *buf, const char *fmt, va_list arg)
{
	return vsnprintf(buf, 0xFFFFFFFF, fmt, arg);
}

int asprintf(char **buf, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int retval = vasprintf(buf, fmt, args);
	va_end(args);
	return retval;
}
int vasprintf(char **buf, const char *fmt, va_list arg)
{
	va_list tmparg = arg;
	char *buffer = malloc(16);
	if (!buffer) return -1;
	int buffersize = 16;
	int written = vsnprintf(buffer, 16, fmt, arg);
	while (written == buffersize)
	{
		buffer = realloc(buffer, buffersize + 16);
		if (!buffer) return -1;
		buffersize += 16;
		written = vsnprintf(buffer, buffersize, fmt, tmparg);
	}
	*buf = buffer;
	return written;
}

