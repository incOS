/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <kernel.h>

unsigned int errno;

void *brk = 0;

void abort(void)
{
	keExit();
}

void *sbrk(int increment)
{
	keDebug("sbrk.\n");
	char *oldbrk = brk;
	char *newbrk = (char*)brk + increment;
	// Round up pages
	uintptr_t firstpage = ((uintptr_t)oldbrk + 4095) / 0x1000;
	uintptr_t lastpage = ((uintptr_t)newbrk + 4095) / 0x1000;
	
	if (firstpage == lastpage)
	{
		brk = newbrk;
		return oldbrk;
	}
	else if (lastpage > firstpage)
	{
		// Allocate memory
		uintptr_t vaddr = firstpage * 0x1000;
		if (!keAllocMemory(SYSCALL_MEM_VIRTUAL, 0, &vaddr, lastpage - firstpage))
		{
			return (void*)-1;
		}
		brk = newbrk;
		return oldbrk;
	}
	else
	{
		// Free memory
		if (!keFreeMemory((void*)(lastpage * 0x1000), firstpage - lastpage))
		{
			return (void*)-1;
		}
		brk = newbrk;
		return oldbrk;
	}
}

void exit(int value)
{
	// TODO: Return value
	// TODO: Close files
	abort();
}

