/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <pthread.h>
#include <errno.h>
#include <unistd.h>

int pthread_spin_destroy(pthread_spinlock_t *spinlock)
{
	if (!spinlock) return EINVAL;
	if (pthread_spin_trylock(spinlock)) return EBUSY;
	return 0;
}
int pthread_spin_init(pthread_spinlock_t *spinlock, int type)
{
	if (!spinlock) return EINVAL;
	*spinlock = 0;
	return 0;
}
int pthread_spin_lock(pthread_spinlock_t *spinlock)
{
	if (!spinlock) return EINVAL;
	while (pthread_spin_trylock(spinlock) == EBUSY)
	{
		sleep(0);
	}
	return 0;
}
int pthread_spin_trylock(pthread_spinlock_t *spinlock)
{
	if (!spinlock) return EINVAL;
	int value = 1;
	asm volatile("lock xchgl %%eax, (%%edx)" : "=a"(value) : "a"(value), "d"(spinlock));
	if (value) return EBUSY;
	return 0;
}
int pthread_spin_unlock(pthread_spinlock_t *spinlock)
{
	if (!spinlock) return EINVAL;
	if (!*spinlock) return EPERM;
	*spinlock = 0;
	return 0;
}

