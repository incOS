
#ifndef SYS_TIME_H_INCLUDED
#define SYS_TIME_H_INCLUDED

#include "rtl/rtl.h"

void sysTick(uint32_t msecs);
uint32_t sysGetTime(void);

#endif

