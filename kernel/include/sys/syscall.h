/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SYS_SYSCALL_H_DEFINED
#define SYS_SYSCALL_H_DEFINED

#include "rtl/rtl.h"
#include "hal/thread.h"

#define SYSCALL_EXIT 0

#define SYSCALL_CREATE_THREAD 1
#define SYSCALL_STOP_THREAD 2

#define SYSCALL_CREATE_TASK 3
#define SYSCALL_MAP_TASK_MEMORY 4
#define SYSCALL_START_TASK 5
#define SYSCALL_CLONE_TASK 16
#define SYSCALL_GET_TASK_INFO 17

#define SYSCALL_SLEEP 6

#define SYSCALL_WAIT_FOR_RPC 7
#define SYSCALL_SEND_RPC 8
#define SYSCALL_RPC_ENDED 9

#define SYSCALL_ALLOC_MEMORY 10
#define SYSCALL_FREE_MEMORY 11

#define SYSCALL_REGISTER_RPC_HANDLER 14

#define SYSCALL_REGISTER_IRQ_HANDLER 13
#define SYSCALL_IRQ_ENDED 15

#define SYSCALL_DEBUG 12

#define SYSCALL_42 42

void sysSyscall(Thread *thread, uintptr_t index, uintptr_t *param1,
	uintptr_t *param2, uintptr_t *param3, uintptr_t *param4, uintptr_t *param5);

uint32_t sysAllocMemory(Task *task, uint32_t flags, uintptr_t *paddr, uintptr_t *vaddr, uint32_t *size);
uint32_t sysFreeMemory(Task *task, uintptr_t vaddr, uint32_t size);

int sysCreateTask(void);
int sysCloneTask(uint32_t id, uint32_t thread, uintptr_t entry);
int sysStartTask(int id, uintptr_t entry);
int sysMapTaskMemory(Thread *thread, int id, uintptr_t src, uintptr_t dest, uint32_t count);
Thread *sysGetRPCTarget(uint32_t taskid);
void sysSendRPC(Thread *source, Thread *target);

#endif

