/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_THREAD_H_INCLUDED
#define HAL_THREAD_H_INCLUDED

#include "rtl/rtl.h"
#include "hal/task.h"

typedef enum ThreadStatus
{
	THREAD_STATUS_RUNNING,
	THREAD_STATUS_SLEEP,
	THREAD_STATUS_WAITRPC,
	THREAD_STATUS_WAITRPCANSWER,
	THREAD_STATUS_WAITRPCTARGET,
} ThreadStatus;

typedef struct Thread
{
	uint32_t id;
	Task *task;
	ThreadStatus status;
	uint32_t timeout;
	uintptr_t rpchandler;
	struct Thread *rpccaller;
} Thread;

Thread *halCreateThread(Task *task, uintptr_t entry, uint32_t paramcount, ...);
void halDestroyThread(Thread *thread);

void halThreadCallFunction(Thread *thread, uintptr_t entry, uint32_t paramcount, ...);
void halThreadRemoveFunctionCall(Thread *thread);

void halScheduleThread(Thread *thread);

void halThreadSetParam(Thread *thread, uint8_t index, uint32_t value);
uint32_t halThreadGetParam(Thread *thread, uint8_t index);

#endif

