/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_X86_PAGING_H_INCLUDED
#define HAL_X86_PAGING_H_INCLUDED

#include "hal/multiboot.h"
#include "rtl/rtl.h"

extern uint32_t kernel_directory[1024];

#define KERNEL_PAGE_TABLES 0xF0000000
#define USER_PAGE_TABLES 0xFD000000

void halInitPaging(void);

void halInitKernelDirectory(void);
uintptr_t halInitUserDirectory(void);
uintptr_t halCloneUserDirectory(uintptr_t pagedir);
void halDeleteUserDirectory(uintptr_t pagedir);
int halMapPageRange(uint32_t *page_directory, uint32_t vaddr, uint32_t paddr, uint32_t flags, uint32_t num_pages);
int halUnmapPageRange(uint32_t *page_directory, uint32_t paddr, uint32_t num_pages);
int halMapPage(uint32_t *page_directory, uint32_t vaddr, uint32_t paddr, uint32_t flags);
int halUnmapPage(uint32_t *page_directory, uint32_t vaddr);
uint32_t halResolveVAddr(uint32_t *page_directory, uint32_t vaddr);
uint32_t halFindContinuousPages(uint32_t *page_directory, uint32_t num, uint32_t lower_limit, uint32_t upper_limit);
uint32_t *halGetKernelDirectory(void);
void *halAllocKernelPages(uint32_t count);
void halFreeKernelPages(void *vaddr, uint32_t count);

int halResolveVaddr(uint32_t *page_directory, uintptr_t vaddr, uintptr_t *paddr);

#endif

