/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_X86_PIC_H_DEFINED
#define HAL_X86_PIC_H_DEFINED

#include "rtl/rtl.h"

// Exception handlers
void halInt00(void);
void halInt01(void);
void halInt02(void);
void halInt03(void);
void halInt04(void);
void halInt05(void);
void halInt06(void);
void halInt07(void);
void halInt08(void);
void halInt09(void);
void halInt0a(void);
void halInt0b(void);
void halInt0c(void);
void halInt0d(void);
void halInt0e(void);
void halInt0f(void);
void halInt10(void);
void halInt11(void);
void halInt12(void);
void halInt13(void);
void halInt14(void);
void halInt15(void);
void halInt16(void);
void halInt17(void);
void halInt18(void);
void halInt19(void);
void halInt1a(void);
void halInt1b(void);
void halInt1c(void);
void halInt1d(void);
void halInt1e(void);
void halInt1f(void);

// IRQ handlers
void halInt20(void);
void halInt21(void);
void halInt22(void);
void halInt23(void);
void halInt24(void);
void halInt25(void);
void halInt26(void);
void halInt27(void);
void halInt28(void);
void halInt29(void);
void halInt2a(void);
void halInt2b(void);
void halInt2c(void);
void halInt2d(void);
void halInt2e(void);
void halInt2f(void);

// Syscall
void halInt30(void);

typedef struct IDTEntry
{
	uint16_t offset_low;
	uint16_t selector;
	uint8_t zero;
	uint8_t type;
	uint16_t offset_high;
} __attribute__((packed)) IDTEntry;

typedef struct IDTDescriptor
{
	uint16_t size;
	uint32_t base;
} __attribute__((packed)) IDTDescriptor;

extern IDTEntry idt[256];

typedef struct IntStackFrame
{
	uint32_t gs;
	uint32_t fs;
	uint32_t es;
	uint32_t ds;
	uint32_t edi;
	uint32_t esi;
	uint32_t ebp;
	uint32_t esp_;
	uint32_t ebx;
	uint32_t edx;
	uint32_t ecx;
	uint32_t eax;
	uint32_t intno;
	uint32_t error;
	uint32_t eip;
	uint32_t cs;
	uint32_t eflags;
	uint32_t esp;
	uint32_t ss;
} IntStackFrame;

void halInitPIC(void);
void halInitPIC2(void);

#endif
 
