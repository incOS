/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_MULTIBOOT_H_DEFINED
#define HAL_MULTIBOOT_H_DEFINED

#include "rtl/rtl.h"

#define MULTIBOOT_HEADER_MAGIC 0x1BADB002
#define MULTIBOOT_HEADER_FLAGS 0x00000003
#define MULTIBOOT_BOOTLOADER_MAGIC 0x2BADB002

typedef struct AoutSymbolTable
{
	uint32_t tabsize;
	uint32_t strsize;
	uint32_t addr;
	uint32_t reserved;
} AoutSymbolTable;

typedef struct ElfSectionHeaderTable
{
	uint32_t num;
	uint32_t size;
	uint32_t addr;
	uint32_t shindex;
} ElfSectionHeaderTable;

typedef struct MbModule
{
	uint32_t start;
	uint32_t end;
	uint32_t string;
	uint32_t reserved;
} MbModule;

typedef struct MbMemoryMap
{
	uint32_t size;
	uint32_t baseaddrlow;
	uint32_t baseaddrhigh;
	uint32_t lengthlow;
	uint32_t lengthhigh;
	uint32_t type;
} MbMemoryMap;

typedef struct MultibootInfo
{
	uint32_t flags;
	uint32_t lowermem;
	uint32_t uppermem;
	uint32_t bootdevice;
	uint32_t cmdline;
	uint32_t modcount;
	uint32_t modaddr;
	union
	{
		AoutSymbolTable aoutsym;
		ElfSectionHeaderTable elfsections;
	} u;
	uint32_t mmaplength;
	uint32_t mmapaddr;
} MultibootInfo;

#define CHECK_FLAG(flags,bit) ((flags) & (1 << (bit)))

#endif

