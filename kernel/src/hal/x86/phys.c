/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "hal/phys.h"
#include "hal/x86/phys.h"
#include "sys/debug.h"

uint8_t memfree[0x20000] = {0};
uint8_t memrefcount[0x100000] = {0};

void kernel_phys_start(void);
void kernel_phys_end(void);

static void halpMarkPage(uint32_t page, int used)
{
	// Mark single bit in bitmap
	uint8_t tmp = memfree[page / 8];
	if (used)
		tmp |= 1 << (7 - (page % 8));
	else
		tmp &= ~((unsigned char)(1 << (7 - (page % 8))));
	memfree[page / 8] = tmp;
}
uint8_t halGrabPage(uint32_t page)
{
	// Increase reference counter
	uint8_t count = memrefcount[page];
	if ((count & 0x80) == 0)
	{
		halpMarkPage(page, 1);
	}
	count = (count & 0x80) + (((count & 0x7F) + 1) & 0x7F);
	memrefcount[page] = count;
	return count;
}
uint8_t halDropPage(uint32_t page)
{
	// Decrease reference counter
	uint8_t count = memrefcount[page];
	count = (count & 0x80) + (((count & 0x7F) - 1) & 0x7F);
	memrefcount[page] = count;
	if (count == 0)
	{
		halpMarkPage(page, 0);
	}
	return count;

}
static void halpMarkMemory(uint32_t start, uint32_t count, int used)
{
	uint32_t i;
	for (i = start; i < count + start; i++)
	{
		halpMarkPage(i, used);
	}
}

void halpInitPhysMemory(MultibootInfo *mbinfo)
{
	// Mark pages as used
	rtlSetMemory(memfree, 0xFF, 0x20000);
	rtlSetMemory(memrefcount, 0x00, 0x20000);
	// Check for memory map
	if (CHECK_FLAG(mbinfo->flags, 6))
	{
		MbMemoryMap *mmap = (MbMemoryMap*)mbinfo->mmapaddr;
		// Loop through mmaps
		for (mmap = (MbMemoryMap*)mbinfo->mmapaddr;
			(uint32_t)mmap < mbinfo->mmapaddr + mbinfo->mmaplength;
			mmap = (MbMemoryMap*)((uint32_t)mmap + mmap->size + sizeof (mmap->size)))
		{
			if (mmap->type == 1)
			{
				uint32_t start = (mmap->baseaddrlow + 4095) / 4096;
				uint32_t end = (mmap->baseaddrlow + mmap->lengthlow) / 4096;
				uint32_t size = end - start;
				// Mark free pages
				halpMarkMemory(start, size, 0);
			}
			else
			{
				uint32_t start = (mmap->baseaddrlow + 4095) / 4096;
				uint32_t end = (mmap->baseaddrlow + mmap->lengthlow) / 4096;
				uint32_t size = end - start;
				// Mark special pages
				rtlSetMemory(memrefcount + start, 0x80, size);
			}
		}
	}
	else
	{
		// We need this, so panic here
		rtlSetMemory((void*)(0xE00B8000), 0x01, 80 * 25 * 2);
		sysDbgPrint("Error: No memory map!\n");
		for(;;);
	}
	
	// Mark used pages
	// Kernel:
	/*halMarkMemory((uintptr_t)kernel_phys_start / 4096,
		(((uintptr_t)kernel_phys_end + 4095) / 4096) - (uintptr_t)kernel_phys_start / 4096,
		1);*/
	// TODO: Optimize this
	uint32_t i;
	for (i = (uintptr_t)kernel_phys_start / 4096; i < ((uintptr_t)kernel_phys_end + 4095) / 4096; i++)
	{
		halGrabPage(i);
	}
	// Multiboot info:
	//halMarkPage(((uintptr_t)mbinfo - 0xE0000000) / 4096, 1);
	halGrabPage(((uintptr_t)mbinfo - 0xE0000000) / 4096);
	// Modules
	if (CHECK_FLAG(mbinfo->flags, 3))
	{
		MbModule *mod = (MbModule*)mbinfo->modaddr;
		//halMarkPage(((uintptr_t)mod - 0xE0000000) / 4096, 1);
		halGrabPage(((uintptr_t)mod - 0xE0000000) / 4096);
		//halMarkPage((uintptr_t)mod->string / 4096, 1);
		halGrabPage((uintptr_t)mod->string / 4096);
		for (i = 0; i < mbinfo->modcount; i++)
		{
			halpMarkMemory(mod->start / 4096,
				((mod->end + 4095) / 4096) - mod->start / 4096, 1);
			uint32_t page;
			for (page = mod->start / 4096; page < (mod->end + 4095) / 4096; page++)
			{
				halGrabPage(page);
			}
			mod++;
		}
	}
	
	//halMarkPage(0, 1);
	halGrabPage(0);
	//halMarkPage(7, 1);
	halGrabPage(1);
}

uintptr_t halGetFreePhysPage(void) // PopUp
{
	// >16M
	int i, j;
	for (i = 512; i < 0x20000; i = i + 4)
	{
		
		uint32_t tmp = *((uint32_t*)&memfree[i]);
		if (tmp != 0xFFFFFFFF)
		{
			
			// First byte
			for (j = 0; j < 7; j++) 
			{
				if (!(tmp & (1 << (7 - j))) && !(memrefcount[i * 8 + j] & 0x80))
				{
					uintptr_t page = i * 8 + j;
					halGrabPage(page);
					return page * 4096;
				}
			}
			
			// Second byte
			for (j = 8; j < 15; j++) 
			{
				if (!(tmp & (1 << (15 - j))) && !(memrefcount[i * 8 + j] & 0x80))
				{
					uintptr_t page = i * 8 + j;
					halGrabPage(page);
					return page * 4096;
				}
			}
			
			// Third byte
			for (j = 16; j < 23; j++)
			{
				if (!(tmp & (1 << (23 - j))) && !(memrefcount[i * 8 + j] & 0x80))
				{
					uintptr_t page = i * 8 + j;
					halGrabPage(page);
					return page * 4096;
				}
			}
			
			// Fourth byte
			for (j = 24; j < 31; j++) 
			{
				if (!(tmp & (1 << (31 - j))) && !(memrefcount[i * 8 + j] & 0x80))
				{
					uintptr_t page = i * 8 + j;
					halGrabPage(page);
					return page * 4096;
				}
			}
			
		}
		
	}	
	return halGetFreePhysPageLow();
}

uintptr_t halGetFreePhysPageLow(void)
{
	// <16M (also used for DMA etc)
	int i, j;
	for (i = 0; i < 512; i++)
	{
		uint8_t tmp = memfree[i];
		for (j = 0; j < 8; j++)
		{
			if (!(tmp & (1 << (7 - j))) && !(memrefcount[i * 8 + j] & 0x80))
			{
				uintptr_t page = i * 8 + j;
				halGrabPage(page);
				return page * 4096;
			}
		}
	}
	return 0;
}

static uintptr_t halPhysPageIsFree(uint32_t page)
{
	uint8_t tmp = memfree[page / 8];
	if (tmp & (1 << (7 - (page % 8)))) return 0;
	return 1;
}

uintptr_t halAllocPhysPagesDMA(uint32_t count)
{
	if (count > 16) return 0;
	
	uint32_t i;
	uint32_t free = 0;
	// Loop through all pages
	// TODO: This is SLOW
	for (i = 0; i < 512; i++)
	{
		// DMA stops at 64k boundaries
		if ((i % 16) == 0) free = 0;
		// Continue to count if page is free
		if (halPhysPageIsFree(i))
		{
			free++;
			if (free == count)
			{
				uint32_t startaddr = i - count + 1;
				for (i = 0; i < count; i++)
				{
					halGrabPage(startaddr + i);
				}
				return startaddr;
			}
		}
		else
		{
			// Reset counter
			free = 0;
		}
	}
	
	return 0;
}

uintptr_t halAllocPhysPage(void)
{
	uintptr_t page = halGetFreePhysPage();
	return page;
}

uintptr_t halCountFreePages(void)
{
	int count = 0;
	int i, j;
	for (i = 0; i < 0x20000; i++)
	{
		uint8_t tmp = memfree[i];
		for (j = 0; j < 8; j++)
		{
			if (!(tmp & (1 << j)))
				count++;
		}
	}
	return count;
}
uintptr_t halCountFreePagesDMA(void)
{
	int count = 0;
	int i, j;
	for (i = 0; i < 512; i++)
	{
		uint8_t tmp = memfree[i];
		for (j = 0; j < 8; j++)
		{
			if (!(tmp & (1 << j)))
				count++;
		}
	}
	return count;
}

uint8_t halpGetReferenceCount(uintptr_t paddr)
{
	uint32_t page = paddr / 0x1000;
	return memrefcount[page] & 0x7F;
}

