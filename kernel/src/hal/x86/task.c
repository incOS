/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "hal/x86/task.h"
#include "hal/x86/thread.h"
#include "hal/x86/paging.h"
#include "hal/thread.h"
#include "sys/debug.h"

uint32_t taskcount = 0;
X86Task **tasks = 0;

uint32_t lasttaskid = 0;

Task *halCreateTask(void)
{
	// Create struct
	X86Task *task = rtlAlloc(sizeof(X86Task));
	rtlZeroMemory(task, sizeof(X86Task));
	task->task.id = ++lasttaskid;
	task->laststack = 0xE0000000;
	
	// Create page directory
	task->pagedir = halInitUserDirectory();
	
	// Add task to list
	taskcount++;
	tasks = rtlRealloc(tasks, taskcount * sizeof(Task*));
	tasks[taskcount - 1] = task;
	
	return (Task*)task;
}
Task *halCloneTask(Task *task, uint32_t thread, uintptr_t entry)
{
	// Look for thread to copy
	uint32_t i;
	uint32_t threadindex = 0xFFFFFFFF;
	for (i = 0; i < task->threadcount; i++)
	{
		sysDbgPrint("Thread %d.\n", task->threads[i]->id);
		if (task->threads[i]->id == thread)
		{
			threadindex = i;
			break;
		}
	}
	if (threadindex == 0xFFFFFFFF)
	{
		sysDbgPrint("halCloneTask: Thread %d not found.\n", thread);
		return 0;
	}
	
	// Create struct
	X86Task *newtask = rtlAlloc(sizeof(X86Task));
	rtlZeroMemory(newtask, sizeof(X86Task));
	newtask->task.id = ++lasttaskid;
	newtask->laststack = 0xE0000000;
	
	// Create page directory
	newtask->pagedir = halCloneUserDirectory(((X86Task*)task)->pagedir);
	
	// Create initial thread
	halpCloneThread(newtask, (X86Thread*)task->threads[threadindex], entry);
	
	// Add task to list
	taskcount++;
	tasks = rtlRealloc(tasks, taskcount * sizeof(Task*));
	tasks[taskcount - 1] = newtask;
	
	return (Task*)newtask;
}
void halDestroyTask(Task *task)
{
	X86Task *x86task = (X86Task*)task;
	// Search for task
	uint32_t i;
	for (i = 0; i < taskcount; i++)
	{
		if (tasks[i] == x86task)
		{
			// Delete threads
			while (task->threadcount > 0)
			{
				// TODO: This way of deleting threads is inefficient
				halDestroyThread(task->threads[0]);
			}
			// Delete thread from list
			for (; i < taskcount - 1; i++)
			{
				tasks[i] = tasks[i + 1];
			}
			taskcount--;
			tasks = rtlRealloc(tasks, taskcount * sizeof(X86Task*));
			
			// Delete userspace memory
			halDeleteUserDirectory(x86task->pagedir);
			rtlFree(task);
			return;
		}
	}
}

Task *halGetTask(uint32_t id)
{
	uint32_t i;
	for (i = 0; i < taskcount; i++)
	{
		if (tasks[i]->task.id == id) return &tasks[i]->task;
	}
	return 0;
}

