/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "hal/x86/paging.h"
#include "hal/x86/gdt.h"
#include "hal/x86/pic.h"
#include "hal/x86/phys.h"
#include "hal/x86/smp.h"
#include "hal/x86/apic.h"
#include "hal/x86/thread.h"
#include "rtl/rtl.h"
#include "sys/debug.h"
#include "sys/start.h"

MultibootInfo *mbinfo;

void halInit(int magic, MultibootInfo *info)
{
	// Clear screen
	rtlSetMemoryW((void*)(0xE00B8000), 0x0700, 80 * 25 * 2);
	
	if (magic != 0x2BADB002)
	{
		// TODO: Display error message? Continue without these infos (needs another
		// way to load initial programs)?
	}
	mbinfo = (MultibootInfo*)((char*)info + 0xE0000000);
	mbinfo->modaddr += 0xE0000000;
	mbinfo->mmapaddr += 0xE0000000;
	
	sysDbgInit();
	sysDbgPrint("incOS HAL (x86) initializing\n");
	sysDbgPrint("============================\n");
	sysDbgPrint("\n");
	if (mbinfo->modcount == 0)
	{
		sysDbgPrint("Error: No modules available.\n");
		while (1);
	}
	// Initialize interrupts
	halInitPIC();
	// Reset GDT
	halInstallGDT();
	// Init physical memory
	halpInitPhysMemory(mbinfo);
	// Initialize paging
	halInitPaging();
	// Init APIC
	halAPICInit();
	
	// Init other processors
	//halInitSMP();
	
	// Create idle thread
	halpCreateIdleThread();
	
	// Map multiboot info struct
	mbinfo = (MultibootInfo*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uintptr_t)mbinfo, (uintptr_t)info & ~0xFFF, 0x3);
	mbinfo = (MultibootInfo*)((uintptr_t)mbinfo + ((uintptr_t)info & 0xFFF));
	mbinfo->modaddr -= 0xE0000000;
	mbinfo->mmapaddr -= 0xE0000000;
	sysDbgPrint("Loading %d modules.\n", mbinfo->modcount);
	
	// Create module info struct
	ModuleInfo *modinfo = rtlAlloc(4 + mbinfo->modcount * (8 + 64));
	modinfo->modulecount = mbinfo->modcount;
	uint32_t i;
	
	// Load module info
	MbModule *mod = (MbModule*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uintptr_t)mod, mbinfo->modaddr & ~0xFFF, 0x3);
	mod = (MbModule*)((uintptr_t)mod + (mbinfo->modaddr & 0xFFF));
	for (i = 0; i < modinfo->modulecount; i++)
	{
		modinfo->modules[i].modulestart = mod[i].start;
		modinfo->modules[i].modulesize = mod[i].end - mod[i].start;
		sysDbgPrint("- Module from %X to %X.\n", mod[i].start, mod[i].end);
		// Copy module string
		char *strpage = (char*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
		halMapPage(kernel_directory, (uintptr_t)strpage, mod[i].string & ~0xFFF, 0x3);
		strpage = (char*)((uintptr_t)strpage + (mod[i].string & 0xFFF));
		uint32_t j;
		for (j = 0; j < 63; j++)
		{
			modinfo->modules[i].path[j] = strpage[j];
			if (!strpage[j]) break;
		}
		modinfo->modules[i].path[63] = 0;
		halMapPage(kernel_directory, (uintptr_t)strpage & ~0xFFF, 0, 0x0);
	}
	
	// Unmap unnecessary pages
	halMapPage(kernel_directory, (uintptr_t)mbinfo & ~0xFFF, 0, 0x0);
	halMapPage(kernel_directory, (uintptr_t)mod & ~0xFFF, 0, 0x0);
	
	sysStart(modinfo);
}

