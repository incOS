/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// To be rewritten

#include "hal/x86/paging.h"
#include "hal/x86/task.h"
#include "hal/x86/phys.h"
#include "rtl/rtl.h"
#include "hal/phys.h"
#include "sys/debug.h"

void kernel_phys_start();
void kernel_phys_end();

int use_phys_tables = 0;

uint32_t kernel_directory[1024] __attribute__ ((aligned (4096)));
extern uint32_t startup_page_table[1024];

void halInitPaging(void)
{
	use_phys_tables = 1;
	
	// Create kernel page directory
	halInitKernelDirectory();
	
	use_phys_tables = 0;
	
	// Enable paging
	asm volatile("mov %0, %%eax\n"
	             "mov %%eax, %%cr3" :: "r" ((uintptr_t)kernel_directory - 0xE0000000));
}

void halInitKernelDirectory(void)
{
	int i;
	for (i = 0; i < 1024; i++)
	{
		kernel_directory[i] = 0;
	}
	
	// Map the directory onto itself
	kernel_directory[KERNEL_PAGE_TABLES >> 22] = ((uintptr_t)kernel_directory - 0xE0000000) | 0x3;
	
	for (i = 896; i < 1024; i++)
	{
		if (kernel_directory[i] == 0)
		{
			uintptr_t page_table_phys = halAllocPhysPage();
			uint32_t *page_table = (uint32_t*)0x1000;
			startup_page_table[1] = page_table_phys | 0x3;
			asm volatile("invlpg 0x1000");
			rtlZeroMemory(page_table, 4096);

			kernel_directory[i] = (uintptr_t)page_table_phys | 0x3;
		}
	}
	
	// Map kernel
	halMapPageRange(kernel_directory, (uint32_t)kernel_phys_start + 0xE0000000,
	    (uint32_t)kernel_phys_start, 0x3, (kernel_phys_end - kernel_phys_start + 4095) / 4096);
	// Map BIOS (for vm86)
	//halMapPage_range(kernel_directory, IVT, 0, 0x3, 1);
	//halMapPage_range(kernel_directory, 0x90000, 0, 0x3, 1);
	//halMapPage_range(kernel_directory, 0xC0000, 0xC0000, 0x3, 64);
	halMapPageRange(kernel_directory, 0xE00B8000, 0xB8000, 0x3, 4);
}

uintptr_t halInitUserDirectory(void)
{
	// Create empty page directory
	uint32_t page_dir_phys = halAllocPhysPage();
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, page_dir_phys, 0x7);
	rtlZeroMemory(page_dir, 4096);
	
	// Map kernel space
	uint32_t i;
	for (i = 0x380; i < 1024; i++) {
		page_dir[i] = kernel_directory[i];
	}
	page_dir[USER_PAGE_TABLES >> 22] = (uint32_t)page_dir_phys | 0x7;
	
	halUnmapPage(kernel_directory, (uint32_t)page_dir);
	return page_dir_phys;
}
uintptr_t halCloneUserDirectory(uintptr_t other)
{
	// Create empty page directory
	uint32_t page_dir_phys = halAllocPhysPage();
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, page_dir_phys, 0x7);
	rtlZeroMemory(page_dir, 4096);
	
	// Map kernel space
	uint32_t i;
	for (i = 0x380; i < 1024; i++) {
		page_dir[i] = kernel_directory[i];
	}
	
	// Copy process page directory
	uint32_t *other_page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)other_page_dir, other, 0x3);
	for (i = 0; i < 0x380; i++)
	{
		uintptr_t pagetable_phys = other_page_dir[i] & ~0xFFF;
		//if (i == 8048000 / 1024 / 1024) sysDbgPrint("PT 8048000: %X.\n", pagetable_phys);
		if (pagetable_phys) sysDbgPrint("PT %X: %X.\n", i * 1024 * 0x1000, pagetable_phys);
		if (pagetable_phys)
		{
			// Map page table
			uint32_t *page_table = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
			halMapPage(kernel_directory, (uint32_t)page_table, pagetable_phys, 0x3);
			
			// Copy page table entries
			uint32_t j;
			for (j = 0; j < 1024; j++)
			{
				uint32_t vaddr = j * 0x1000 + i * 0x1000 * 1024;
				uint32_t paddr = page_table[j] & ~0xFFF;
				if (paddr)
				{
					if (vaddr == 0x8048000) sysDbgPrint("Creating 8048000.\n");
					uint32_t refcount = halpGetReferenceCount(paddr);
					if (refcount == 1)
					{
						sysDbgPrint("Non-shared memory: %X (%d).\n", vaddr, refcount);
						// Copy page as it isn't shared with other processes
						uint32_t page_phys = halAllocPhysPage();
						uint32_t *page = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
						halMapPage(kernel_directory, (uint32_t)page, page_phys, 0x7);
						uint32_t *page_orig = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
						halMapPage(kernel_directory, (uint32_t)page_orig, paddr, 0x7);
						rtlCopyMemory(page, page_orig, 0x1000);
						halUnmapPage(kernel_directory, (uint32_t)page);
						halUnmapPage(kernel_directory, (uint32_t)page_orig);
						halMapPage(page_dir, vaddr, page_phys, 0x7);
					}
					else
					{
						sysDbgPrint("Shared memory: %X (%d).\n", vaddr, refcount);
						// Map page directly, it could be shared memory
						halMapPage(page_dir, vaddr, paddr, 0x7);
					}
				}
			}
			// Unmap page table
			halMapPage(kernel_directory, (uint32_t)page_table, 0, 0x0);
		}
	}
	halUnmapPage(kernel_directory, (uint32_t)other_page_dir);
	
	page_dir[USER_PAGE_TABLES >> 22] = (uint32_t)page_dir_phys | 0x7;
	halUnmapPage(kernel_directory, (uint32_t)page_dir);
	return page_dir_phys;

}
void halDeleteUserDirectory(uintptr_t pagedir)
{
	// Map task page dir
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, pagedir, 0x3);
	
	uint32_t i;
	for (i = 0; i < (0xE0000000 >> 22); i++)
	{
		uintptr_t pagetable_phys = page_dir[i] & ~0xFFF;
		if (pagetable_phys)
		{
			// Map page table
			uint32_t *page_table = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
			halMapPage(kernel_directory, (uint32_t)page_table, pagetable_phys, 0x3);
			
			// Delete page table entries
			uint32_t j;
			for (j = 0; j < 1024; j++)
			{
				if (page_table[j] & ~0xFFF)
				{
					halDropPage(page_table[j] / 4096);
				}
			}
			
			// Delete page table
			halMapPage(kernel_directory, (uint32_t)page_table, 0, 0x0);
			halDropPage(pagetable_phys / 4096);
		}
	}
	
	// Clean up
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
	halDropPage(pagedir / 4096);
}

int halMapPageRange(uint32_t *page_directory, uint32_t vaddr, uint32_t paddr, uint32_t flags, uint32_t num_pages)
{
	uint32_t i;
	for (i = 0; i < num_pages; i++) {
		halMapPage(page_directory, vaddr + i * 0x1000, paddr + i * 0x1000, flags);
	}
	return 1;
}
int halUnmapPageRange(uint32_t *page_directory, uint32_t paddr, uint32_t num_pages)
{
	uint32_t i;
	for (i = 0; i < num_pages; i++) {
		halMapPage(page_directory, 0, paddr + i * 0x1000, 0);
	}
	return 1;
}
int halMapPage(uint32_t *page_directory, uint32_t vaddr, uint32_t paddr, uint32_t flags)
{
	//if ((vaddr == 0) || (vaddr & 0xFFF) || (paddr & 0xFFF)) {
	//	return FALSE;
	//}
	//dprint("Mapping %X to %X\n", paddr, vaddr);
	uint32_t page = vaddr / 4096;
	uint32_t *page_table = 0;
	if (page_directory[page / 1024] == 0) {
		// Create new page table
		page_table = (uint32_t*)halAllocPhysPage();
		page_directory[page / 1024] = (uint32_t)page_table | 0x7;
		if (!use_phys_tables) {
			if (page_directory == kernel_directory) {
				page_table = (uint32_t*)(KERNEL_PAGE_TABLES + (page / 1024) * 4096);
			} else {
				// TODO
				uint32_t page_table_phys = (uint32_t)page_table;
				page_table = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
				halMapPage(kernel_directory, (uint32_t)page_table, page_table_phys, 0x7);
			}
		} else {
			//page_table = (uint32_t*)((uint32_t)page_table + 0xE0000000);
			uintptr_t page_table_phys = (intptr_t)page_table;
			page_table = (uint32_t*)0x1000;
			startup_page_table[1] = page_table_phys | 0x3;
			asm volatile("invlpg 0x1000");
		}
		rtlZeroMemory(page_table, 4096);
	} else {
		if (page_directory == kernel_directory) {
			if (!use_phys_tables) {
				page_table = (uint32_t*)(KERNEL_PAGE_TABLES + (page / 1024) * 4096);
			} else {
				uintptr_t page_table_phys = page_directory[page / 1024] & ~0xFFF;
				page_table = (uint32_t*)0x1000;
				startup_page_table[1] = page_table_phys | 0x3;
				asm volatile("invlpg 0x1000");
			}
		} else {
			// TODO
			uint32_t page_table_phys = page_directory[page / 1024] & ~0xFFF;
			page_table = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
			halMapPage(kernel_directory, (uint32_t)page_table, page_table_phys, 0x7);
		}
	}
	
	page_table[page % 1024] = paddr | flags;
	if (!use_phys_tables && (page_directory == kernel_directory)) {
		 asm volatile("invlpg %0" :: "m" (*(char*)vaddr));
	} else {
		// TODO
	}
	return 1;
}
int halUnmapPage(uint32_t *page_directory, uint32_t vaddr)
{
	return halMapPage(page_directory, vaddr, 0, 0);
}
uint32_t halResolveVAddr(uint32_t *page_directory, uint32_t vaddr)
{
	uint32_t page = vaddr / 4096;
	uint32_t *page_table;
	if (page_directory == kernel_directory) {
		page_table = (uint32_t*)(KERNEL_PAGE_TABLES + (page / 1024) * 4096);
		if (page_directory[page / 1024] == 0) {
			return 0;
		} else if (page_table[page % 1024] == 0) {
			return 0;
		} else {
			return (page_table[page % 1024] & ~0xFFF) + (vaddr & 0xFFF);
		}page_table = (uint32_t*)(KERNEL_PAGE_TABLES + (page / 1024) * 4096);
	} else {
		uint32_t page_table_phys = page_directory[page / 1024] & ~0xFFF;
		if (!page_table_phys) {
			return 0;
		}
		page_table = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
		halMapPage(kernel_directory, (uint32_t)page_table, page_table_phys, 0x7);
		uint32_t addr = page_table[page % 1024] & ~0xFFF;
		halUnmapPage(kernel_directory, (uint32_t)page_table);
		if (!addr) {
			return 0;
		} else {
			return addr + (vaddr & 0xFFF);
		}
	}
}
uint32_t halFindContinuousPages(uint32_t *page_directory, uint32_t num, uint32_t lower_limit, uint32_t upper_limit)
{
	uint32_t startposition = lower_limit / 4096;
	if (startposition == 0) startposition = 1;
	
	uint32_t pagetableidx = startposition % 1024;
	uint32_t length = 0;
	uint32_t pagediridx = startposition / 1024;
	uint32_t *page_table = 0;
	
	// Loop as long as there is space left
	while (startposition <= upper_limit / 4096 - num) {
		if (page_directory[pagediridx] == 0) {
			// If the page table is empty, it's free
			length += 1024 - pagetableidx;
			pagetableidx = 0;
			pagediridx++;
			if (length >= num) {
				return startposition * 4096;
			}
		} else {
			// Get page table
			if (page_directory == kernel_directory) {
				page_table = (uint32_t*)(KERNEL_PAGE_TABLES + pagediridx * 4096);
			} else {
				// TODO
				page_table = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xF0000000, 0xFFFFFFFF);
				halMapPage(kernel_directory, (uint32_t)page_table, page_directory[pagediridx] & ~0xFFF, 0x7);
			}
			if (page_table[pagetableidx] == 0) {
				// Increment length on empty pages
				length++;
				if (length >= num) {
					if (page_directory != kernel_directory) {
						halUnmapPage(kernel_directory, (uint32_t)page_table);
					}
					return startposition * 4096;
				}
				pagetableidx++;
				if (pagetableidx == 1024) {
					pagetableidx = 0;
					pagediridx++;
				}
			} else {
				// Start to count again at the current position
				pagetableidx++;
				if (pagetableidx == 1024) {
					pagetableidx = 0;
					pagediridx++;
				}
				startposition = pagediridx * 1024 + pagetableidx;
				length = 0;
			}
			if (page_directory != kernel_directory) {
				halUnmapPage(kernel_directory, (uint32_t)page_table);
			}
		}
	}
	return 0;
}

uint32_t *halGetKernelDirectory(void)
{
	return kernel_directory;
}

void *halAllocKernelPages(uint32_t count)
{
	uint32_t vaddr = halFindContinuousPages(kernel_directory, count, 0xE0000000, 0xFFFFFFFF);
	uint32_t i;
	for (i = 0; i < count; i++) {
		halMapPage(kernel_directory, vaddr + i * 0x1000, halAllocPhysPage(), 0x3);
	}
	return (void*)vaddr;
}
void halFreeKernelPages(void *vaddr, uint32_t count)
{
	uint32_t i;
	for (i = 0; i < count; i++) {
		halDropPage(halResolveVAddr(kernel_directory, (uint32_t)vaddr + i * 0x1000) / 4096);
		halUnmapPage(kernel_directory, (uint32_t)vaddr + i * 0x1000);
	}
}

void *halReserveKernelPages(uint32_t count)
{
	return (void*)halFindContinuousPages(kernel_directory, count, 0xE0000000, 0xFFFFFFFF);
}
void halMapKernelPage(void *vaddr, uintptr_t paddr)
{
	halMapPage(kernel_directory, (uintptr_t)vaddr, paddr, 0x3);
}
void halUnmapKernelPage(void *vaddr)
{
	halMapPage(kernel_directory, (uintptr_t)vaddr, 0, 0);
}

uintptr_t halReserveTaskPages(Task *task, uint32_t count)
{
	// Map page directory
	X86Task *x86task = (X86Task*)task;
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, x86task->pagedir, 0x3);
	
	uintptr_t addr = halFindContinuousPages(page_dir, count, 0x0001000, 0xDF000000);
	
	// Clean up again
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
	return addr;
}
void halMapTaskPage(Task *task, uintptr_t vaddr, uintptr_t paddr)
{
	if (vaddr >= 0xE0000000) return;
	// Map page directory
	X86Task *x86task = (X86Task*)task;
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, x86task->pagedir, 0x3);
	
	// Map page
	halMapPage(page_dir, vaddr, paddr, 0x7);
	asm volatile("invlpg %0" :: "m" (*(char*)vaddr));
	
	// Clean up again
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
}
void halUnmapTaskPage(Task *task, uintptr_t vaddr)
{
	if (vaddr >= 0xE0000000) return;
	// Map page directory
	X86Task *x86task = (X86Task*)task;
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, x86task->pagedir, 0x3);
	
	// Map page
	halMapPage(page_dir, vaddr, 0, 0x0);
	
	// Clean up again
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
}

uintptr_t halTaskGetPhysPage(Task *task, uintptr_t vaddr)
{
	if (vaddr >= 0xE0000000) return 0;
	vaddr &= ~0xFFF;
	uint32_t page = vaddr / 4096;
	// Map page directory
	X86Task *x86task = (X86Task*)task;
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, x86task->pagedir, 0x3);
	
	uint32_t dirindex = vaddr >> 22;
	
	uint32_t *page_table = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_table, page_dir[dirindex] & ~0xFFF, 0x3);
	
	uintptr_t physaddr = page_table[page % 1024] & ~0xFFF;
	
	// Clean up again
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
	halMapPage(kernel_directory, (uint32_t)page_table, 0, 0x0);
	
	return physaddr;
}

int halResolveVaddr(uint32_t *page_directory, uintptr_t vaddr, uintptr_t *paddr)
{
	uint32_t page = vaddr / 4096;
	uint32_t dirindex = vaddr >> 22;
	
	// Map page table
	uint32_t *page_table = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_table, page_directory[dirindex] & ~0xFFF, 0x3);
	
	// Get physical address
	*paddr = page_table[page % 1024] & ~0xFFF;
	int status = page_table[page % 1024] & 0xFFF;
	// Clean up again
	halMapPage(kernel_directory, (uint32_t)page_table, 0, 0x0);
	
	return status;
}


