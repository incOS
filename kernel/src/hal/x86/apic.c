/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "hal/x86/apic.h"
#include "hal/x86/cpu.h"
#include "hal/x86/paging.h"
#include "sys/debug.h"

uintptr_t apic_base_phys = 0;
uintptr_t apic_base = 0;

int halAPICInit(void)
{
	// Get physical address
	//halWriteMSR(0x1B, 0xFEE00000);
	apic_base_phys = halReadMSR(0x1B) & ~0xFFF;
	sysDbgPrint("APIC: Address: %X\n", apic_base_phys);
	
	// Map mmio space into virtual address space
	apic_base = halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, apic_base, apic_base_phys & ~0xFFF, 0x3);
	apic_base = apic_base + (apic_base_phys & 0xFFF);
	
	sysDbgPrint("ID: %X, version: %X\n", halAPICRead(0x20), halAPICRead(0x30));
	
	halAPICWrite(0x80, 0x20);
	//halAPICWrite(0x320, 0x10000);
	//halAPICWrite(0x340, 0x10000);
	halAPICWrite(0x350, 0x08700);
	halAPICWrite(0x360, 0x400);
	halAPICWrite(0x370, 0x10000);
	halAPICWrite(0xF0, 0x10F);
	halAPICWrite(0x350, 0x08700);
	halAPICWrite(0x360, 0x400);
	
	// Timer
	/*halAPICWrite(0x3e0, 0xb);
	halAPICWrite(0x320, 0x20040);
	halAPICWrite(0x380, 0x100);
	
	int i;
	for (i = 0; i < 100000; i++)
	{
		halAPICRead(0x380);
	}
	sysDbgPrint("APIC Timer: 0x%X\n", halAPICRead(0x390));
	*/
	
	return 1;
}

void halAPICWrite(uint16_t offset, uint32_t value)
{
	*((volatile uint32_t*)(apic_base + offset)) = value;
}
uint32_t halAPICRead(uint16_t offset)
{
	return *((volatile uint32_t*)(apic_base + offset));
}

void halAPICWriteStartup(uintptr_t entry)
{
	halAPICWrite(0x310, 0);
	halAPICWrite(0x300, 0xC4600 | ((entry / 0x1000) && 0xFF));
	sysDbgPrint("0x300: %X\n", halAPICRead(0x300));
}

