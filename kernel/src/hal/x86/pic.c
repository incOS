/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "hal/x86/pic.h"
#include "hal/x86/thread.h"
#include "hal/ports.h"
#include "sys/debug.h"
#include "sys/syscall.h"
#include "sys/scheduler.h"
#include "hal/x86/gdt.h"
#include "hal/x86/paging.h"
#include "hal/phys.h"

IDTEntry idt[256];

struct
{
	Thread *thread;
	uintptr_t entry;
} irq_handler_user[16];

void halSetIDTEntry(int intno, void (*handler)(void), int dpl)
{
	IDTEntry entry;
	entry.zero = 0;
	entry.offset_low = (uint32_t)handler & 0x0000FFFF;
	entry.offset_high = (uint32_t)handler >> 16;
	entry.selector = 0x8;
	entry.type = 0x8e + ((dpl & 0x3) << 5);
	idt[intno] = entry;
}

void halInitPIC(void)
{
	rtlZeroMemory(idt, sizeof(IDTEntry) * 256);
	rtlZeroMemory(irq_handler_user, sizeof(irq_handler_user[0]) * 16);
	
	// Remap PIC
	halPortWriteByte(0x20, 0x11);
	halPortWriteByte(0xa0, 0x11);
	halPortWriteByte(0x21, 0x20);
	halPortWriteByte(0xa1, 0x28);
	halPortWriteByte(0x21, 0x04);
	halPortWriteByte(0xa1, 0x02);
	halPortWriteByte(0x21, 0x01);
	halPortWriteByte(0xa1, 0x01);
	halPortWriteByte(0x21, 0xff);
	halPortWriteByte(0xa1, 0xff);
	
	// Fill IDT
	halSetIDTEntry(0x00, halInt00, 0);
	halSetIDTEntry(0x01, halInt01, 0);
	halSetIDTEntry(0x02, halInt02, 0);
	halSetIDTEntry(0x03, halInt03, 0);
	halSetIDTEntry(0x04, halInt04, 0);
	halSetIDTEntry(0x05, halInt05, 0);
	halSetIDTEntry(0x06, halInt06, 0);
	halSetIDTEntry(0x07, halInt07, 0);
	halSetIDTEntry(0x08, halInt08, 0);
	halSetIDTEntry(0x09, halInt09, 0);
	halSetIDTEntry(0x0a, halInt0a, 0);
	halSetIDTEntry(0x0b, halInt0b, 0);
	halSetIDTEntry(0x0c, halInt0c, 0);
	halSetIDTEntry(0x0d, halInt0d, 0);
	halSetIDTEntry(0x0e, halInt0e, 0);
	halSetIDTEntry(0x0f, halInt0f, 0);
	halSetIDTEntry(0x10, halInt10, 0);
	halSetIDTEntry(0x11, halInt11, 0);
	halSetIDTEntry(0x12, halInt12, 0);
	halSetIDTEntry(0x13, halInt13, 0);
	halSetIDTEntry(0x14, halInt14, 0);
	halSetIDTEntry(0x15, halInt15, 0);
	halSetIDTEntry(0x16, halInt16, 0);
	halSetIDTEntry(0x17, halInt17, 0);
	halSetIDTEntry(0x18, halInt18, 0);
	halSetIDTEntry(0x19, halInt19, 0);
	halSetIDTEntry(0x1a, halInt1a, 0);
	halSetIDTEntry(0x1b, halInt1b, 0);
	halSetIDTEntry(0x1c, halInt1c, 0);
	halSetIDTEntry(0x1d, halInt1d, 0);
	halSetIDTEntry(0x1e, halInt1e, 0);
	halSetIDTEntry(0x1f, halInt1f, 0);

	halSetIDTEntry(0x20, halInt20, 0);
	halSetIDTEntry(0x21, halInt21, 0);
	halSetIDTEntry(0x22, halInt22, 0);
	halSetIDTEntry(0x23, halInt23, 0);
	halSetIDTEntry(0x24, halInt24, 0);
	halSetIDTEntry(0x25, halInt25, 0);
	halSetIDTEntry(0x26, halInt26, 0);
	halSetIDTEntry(0x27, halInt27, 0);
	halSetIDTEntry(0x28, halInt28, 0);
	halSetIDTEntry(0x29, halInt29, 0);
	halSetIDTEntry(0x2a, halInt2a, 0);
	halSetIDTEntry(0x2b, halInt2b, 0);
	halSetIDTEntry(0x2c, halInt2c, 0);
	halSetIDTEntry(0x2d, halInt2d, 0);
	halSetIDTEntry(0x2e, halInt2e, 0);
	halSetIDTEntry(0x2f, halInt2f, 0);

	halSetIDTEntry(0x30, halInt30, 3);
	
	// Enable IDT
	IDTDescriptor desc;
	desc.size = 256 * sizeof(IDTEntry) - 1;
	desc.base = (uint32_t)idt;
	
	asm("lidt %0"::"m"(desc));
}

void halInitPIC2(void)
{
	// Enable IDT
	IDTDescriptor desc;
	desc.size = 256 * sizeof(IDTEntry) - 1;
	desc.base = (uint32_t)idt;
	
	asm("lidt %0"::"m"(desc));
}

extern X86Thread *current_thread;

void halExceptionHandler(IntStackFrame *isf)
{
	if (isf->cs == 0x8)
	{
		// TODO: Don't kill kernel on non-critical exceptions
		// Print register info
		if (isf->intno == 14) {
			uint32_t cr2;
			asm("mov %%cr2, %0":"=r"(cr2));
			sysDbgPrint("\nPanic:\nPage fault at %x, error code: %x\n\n", cr2, isf->error);
			if (current_thread && current_thread->thread.task)
			{
				sysDbgPrint("Task: %d, thread: %d\n\n", current_thread->thread.task->id, current_thread->thread.id);
			}
		} else {
			sysDbgPrint("\n\nPanic:\nException %d, error code: %x\n\n", isf->intno, isf->error);
		}
		sysDbgPrint("eax: %x\tebx: %x\tecx: %x\tedx: %x\n", isf->eax, isf->ebx, isf->ecx, isf->edx);
		sysDbgPrint("ds: %x\tes: %x\tfs: %x\tgs: %x\n", isf->ds, isf->es, isf->fs, isf->gs);
		sysDbgPrint("ss: %x\tesp: %x\tcs: %x\teip: %x\n", isf->ss, isf->esp, isf->cs, isf->eip);
		sysDbgPrint("eflags: %x\n", isf->eflags);
		asm("cli");
		asm("hlt");
		while(1);
	}
	else
	{
		// Print register info
		if (isf->intno == 14) {
			uint32_t cr2;
			asm("mov %%cr2, %0":"=r"(cr2));
			if (current_thread && (cr2 < current_thread->userstack + 0x400000) && (cr2 > current_thread->userstack))
			{
				// Just grow stack
				uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
				halMapPage(kernel_directory, (uint32_t)page_dir, ((X86Task*)current_thread->thread.task)->pagedir, 0x3);
				uintptr_t userstack_phys = halAllocPhysPage();
				halMapPage(page_dir, cr2 & ~0xFFF, userstack_phys, 0x7);
				halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
				return;
			}
			sysDbgPrint("\nProcess killed:\nPage fault at %x, error code: %x\n\n", cr2, isf->error);
			if (current_thread && current_thread->thread.task)
			{
				sysDbgPrint("Task: %d, thread: %d\n\n", current_thread->thread.task->id, current_thread->thread.id);
			}
		} else {
			sysDbgPrint("\n\nProcess killed:\nException %d, error code: %x\n\n", isf->intno, isf->error);
		}
		sysDbgPrint("eax: %x\tebx: %x\tecx: %x\tedx: %x\n", isf->eax, isf->ebx, isf->ecx, isf->edx);
		sysDbgPrint("ds: %x\tes: %x\tfs: %x\tgs: %x\n", isf->ds, isf->es, isf->fs, isf->gs);
		sysDbgPrint("ss: %x\tesp: %x\tcs: %x\teip: %x\n", isf->ss, isf->esp, isf->cs, isf->eip);
		sysDbgPrint("eflags: %x\n", isf->eflags);
		// Kill task
		Task *task = current_thread->thread.task;
		while (task->threadcount > 0)
		{
			Thread *thread0 = task->threads[0];
			sysRemoveThread(thread0);
			halDestroyThread(thread0);
		}
		halDestroyTask(task);
	}
	
}

inline void halSyscall(void)
{
	IntStackFrame *isf = (IntStackFrame*)current_thread->kernelstack;
	sysSyscall(&current_thread->thread, isf->eax, &isf->ebx, &isf->ecx, &isf->edx, &isf->esi, &isf->edi);
}

void (*irq_handler[16])(void);

void *halIntHandler(IntStackFrame *isf)
{
	if (current_thread)
	{
		current_thread->kernelstack = (uintptr_t)isf;
	}
	if (isf->intno < 0x20) {
		// Exception
		halExceptionHandler(isf);
	} else if (isf->intno >= 0x30) {
		if (isf->intno == 0x30) {
			if (isf->cs != 0x8) {
				halSyscall();
			} else {
				halSyscall();
			}
		}
	} else {
		// Call handler
		
		if (irq_handler[isf->intno - 0x20]) {
			irq_handler[isf->intno - 0x20]();
		}
		if (irq_handler_user[isf->intno - 0x20].entry && irq_handler_user[isf->intno - 0x20].thread)
		{
			halThreadCallFunction(irq_handler_user[isf->intno - 0x20].thread, irq_handler_user[isf->intno - 0x20].entry, 1, isf->intno - 0x20);
			halScheduleThread(irq_handler_user[isf->intno - 0x20].thread);
		}
		else if (!irq_handler[isf->intno - 0x20])
		{
			sysDbgPrint("Interrupt (%d) without handler!\n", isf->intno - 0x20);
		}
		
		// Send EOI
		if (isf->intno >= 0x28) {
			halPortWriteByte(0xA0, 0x20);
		}
		halPortWriteByte(0x20, 0x20);
	}
	if (!current_thread) return isf;
	tss.esp0 = current_thread->kernelstack + 19 * 4;
	return (void*)current_thread->kernelstack;
}

void halRegisterIRQ(uint32_t irqno, void (*handler)(void))
{
	irq_handler[irqno] = handler;
	if (handler && !irq_handler_user[irqno].entry) {
		if (irqno < 8) {
			halPortWriteByte(0x21, halPortReadByte(0x21) & ~(0x1 << irqno));
		} else {
			halPortWriteByte(0xa1, halPortReadByte(0xa1) & ~(0x1 << (irqno - 8)));
		}
	} else if (!irq_handler_user[irqno].entry) {
		if (irqno < 8) {
			halPortWriteByte(0x21, halPortReadByte(0x21) | (0x1 << irqno));
		} else {
			halPortWriteByte(0xa1, halPortReadByte(0xa1) | (0x1 << irqno));
		}
	}
}
void halRegisterUserIRQ(uint32_t irqno, Thread *thread, uintptr_t entry)
{
	irq_handler_user[irqno].thread = thread;
	irq_handler_user[irqno].entry = entry;
	thread->rpccaller = 0;
	
	if (thread && entry && !irq_handler[irqno]) {
		if (irqno < 8) {
			halPortWriteByte(0x21, halPortReadByte(0x21) & ~(0x1 << irqno));
		} else {
			halPortWriteByte(0xa1, halPortReadByte(0xa1) & ~(0x1 << (irqno - 8)));
		}
	} else if (!irq_handler[irqno]) {
		if (irqno < 8) {
			halPortWriteByte(0x21, halPortReadByte(0x21) | (0x1 << irqno));
		} else {
			halPortWriteByte(0xa1, halPortReadByte(0xa1) | (0x1 << irqno));
		}
	}

}

