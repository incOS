/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "hal/x86/thread.h"
#include "hal/x86/task.h"
#include "hal/phys.h"
#include "hal/x86/paging.h"
#include "hal/x86/pic.h"

X86Thread *idle_thread = 0;
X86Thread *current_thread = 0;

void halpIdleThread(void)
{
	while (1)
	{
		asm volatile("hlt");
	}
}

void halpCreateIdleThread(void)
{
	X86Thread *thread = rtlAlloc(sizeof(X86Thread));
	// Setup thread struct
	thread->thread.task = 0;
	thread->thread.status = THREAD_STATUS_RUNNING;
	thread->thread.timeout = 0;
	thread->thread.rpchandler = 0;
	thread->thread.rpccaller = 0;
	// Setup user stack
	uintptr_t userstack_virt = halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	uintptr_t userstack_phys = halAllocPhysPage();
	halMapPage(kernel_directory, userstack_virt, userstack_phys, 0x3);
	thread->userstack = userstack_virt;
	
	// Setup kernel stack
	uintptr_t kernelstack_virt = halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	uintptr_t kernelstack_phys = halAllocPhysPage();
	halMapPage(kernel_directory, kernelstack_virt, kernelstack_phys, 0x3);
	thread->kernelstack_bottom = kernelstack_virt;
	uint32_t *stack = (uint32_t*)kernelstack_virt;
	stack += 1024;
	*(--stack) = 0x10;
	*(--stack) = thread->userstack + 0x1000;
	*(--stack) = 0x202;
	*(--stack) = 0x08;
	*(--stack) = (uint32_t)halpIdleThread;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	thread->kernelstack = (uintptr_t)stack;
	
	idle_thread = thread;
}
void halpCloneThread(X86Task *target, X86Thread *thread, uint32_t entry)
{
	X86Thread *newthread = rtlAlloc(sizeof(X86Thread));
	
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, target->pagedir, 0x3);
	// Setup thread struct
	newthread->thread.task = &target->task;
	newthread->thread.status = THREAD_STATUS_RUNNING;
	newthread->thread.timeout = 0;
	newthread->thread.rpchandler = 0;
	newthread->thread.rpccaller = 0;
	// Setup user stack
	target->laststack = thread->userstack - (thread->userstack % 0x400000);
	/*uintptr_t userstack_virt = x86task->laststack;
	uintptr_t userstack_phys = halAllocPhysPage();
	halMapPage(page_dir, thread->thread.userstack, userstack_phys, 0x7);*/
	
	// Setup kernel stack
	uintptr_t kernelstack_virt = halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	uintptr_t kernelstack_phys = halAllocPhysPage();
	halMapPage(kernel_directory, kernelstack_virt, kernelstack_phys, 0x3);
	rtlCopyMemory((void*)kernelstack_virt, (void*)thread->kernelstack_bottom, 0x1000);
	newthread->kernelstack_bottom = kernelstack_virt;
	newthread->kernelstack = kernelstack_virt + (thread->kernelstack - thread->kernelstack_bottom);
	
	uint32_t *stack = (uint32_t*)newthread->kernelstack;
	stack[14] = entry;
	
	// Clean up
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
	
	// Add thread to list
	target->task.threadcount++;
	target->task.threads = rtlRealloc(target->task.threads, target->task.threadcount * sizeof(Thread*));
	target->task.threads[target->task.threadcount - 1] = &newthread->thread;
}

Thread *halCreateThread(Task *task, uintptr_t entry, uint32_t paramcount, ...)
{
	uint32_t *params = &paramcount + 1;
	
	X86Task *x86task = (X86Task*)task;
	X86Thread *thread = rtlAlloc(sizeof(X86Thread));
	
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, x86task->pagedir, 0x3);
	// Setup thread struct
	thread->thread.task = task;
	thread->thread.status = THREAD_STATUS_RUNNING;
	thread->thread.timeout = 0;
	thread->thread.rpchandler = 0;
	thread->thread.rpccaller = 0;
	// Setup user stack
	x86task->laststack -= 0x400000;
	uintptr_t userstack_virt = x86task->laststack;
	uintptr_t userstack_phys = halAllocPhysPage();
	halMapPage(page_dir, userstack_virt + 0x3FF000, userstack_phys, 0x7);
	
	// Push params onto user stack
	uint32_t *stack = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uintptr_t)stack, userstack_phys, 0x3);
	uint32_t i;
	for (i = 0; i < paramcount; i++)
	{
		stack[1024 - paramcount + i] = params[i];
	}
	stack[1024 - paramcount - 1] = 0;
	halMapPage(kernel_directory, (uintptr_t)stack, 0, 0x0);
	
	thread->userstack = userstack_virt;
	
	
	// Setup kernel stack
	uintptr_t kernelstack_virt = halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	uintptr_t kernelstack_phys = halAllocPhysPage();
	halMapPage(kernel_directory, kernelstack_virt, kernelstack_phys, 0x3);
	thread->kernelstack_bottom = kernelstack_virt;
	stack = (uint32_t*)kernelstack_virt;
	stack += 1024;
	*(--stack) = 0x23;
	*(--stack) = thread->userstack + 0x400000 - paramcount * 4 - 4;
	*(--stack) = 0x232;
	*(--stack) = 0x1b;
	*(--stack) = entry;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	thread->kernelstack = (uintptr_t)stack;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	
	// Clean up
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
	
	// Add thread to list
	task->threadcount++;
	task->threads = rtlRealloc(task->threads, task->threadcount * sizeof(Thread*));
	task->threads[task->threadcount - 1] = &thread->thread;
	return &thread->thread;
}
void halDestroyThread(Thread *thread)
{
	if (&current_thread->thread == thread) current_thread = 0;
	
	Task *task = thread->task;
	X86Task *x86task = (X86Task*)task;
	X86Thread *x86thread = (X86Thread*)thread;
	
	// Map task page dir
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, x86task->pagedir, 0x3);
	
	// Search thread in thread list
	uint32_t i;
	for (i = 0; i < task->threadcount; i++)
	{
		if (task->threads[i] == thread)
		{
			// Delete thread from list
			for (; i < task->threadcount - 1; i++)
			{
				task->threads[i] = task->threads[i + 1];
			}
			task->threadcount--;
			task->threads = rtlRealloc(task->threads, task->threadcount * sizeof(Thread*));
			break;
		}
	}
		
	// Unmap user stack
	for (i = 0; i < 0x400; i++)
	{
		uintptr_t paddr;
		int status = halResolveVaddr(page_dir, (uintptr_t)x86thread->userstack + i * 0x1000, &paddr);
		if (status)
		{
			halDropPage(paddr / 4096);
			halMapPage(page_dir, (uintptr_t)x86thread->userstack + i * 0x1000, 0, 0);
		}
	}
	
	// FIXME: We currently leak kernel stacks, but we cannot delete them here as
	//        they are in use.
	// Unmap kernel stack
	/*uintptr_t paddr;
	int status = halResolveVaddr(kernel_directory, (uintptr_t)x86thread->kernelstack_bottom, &paddr);
	if (status)
	{
		halDropPage(paddr);
		halMapPage(kernel_directory, (uintptr_t)x86thread->kernelstack_bottom, 0, 0);
	}*/
	
	// Clean up
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
	
	rtlFree(thread);
}

void halThreadCallFunction(Thread *thread, uintptr_t entry, uint32_t paramcount, ...)
{
	uint32_t *params = &paramcount + 1;
	X86Thread *x86thread = (X86Thread*)thread;
	X86Task *x86task = (X86Task*)thread->task;
	
	// Map task page dir
	uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)page_dir, x86task->pagedir, 0x3);
	
	// Add frame to kernel stack
	uint32_t *stack = (uint32_t*)x86thread->kernelstack;
	uint32_t *oldstack = stack;
	uintptr_t userstack = stack[17];
	uintptr_t userstack_phys;
	int status = halResolveVaddr(page_dir, userstack & ~0xFFF, &userstack_phys);
	if (!status) return;
	
	// Push params onto user stack
	uint32_t *userstack_virt = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uintptr_t)userstack_virt, userstack_phys, 0x3);
	userstack_virt = (uint32_t*)((uintptr_t)userstack_virt + (userstack & 0xFFF));
	userstack_virt -= paramcount + 1;
	uint32_t i;
	for (i = 0; i < paramcount; i++)
	{
		userstack_virt[i + 1] = params[i];
	}
	userstack_virt[0] = 0;
	halMapPage(kernel_directory, (uintptr_t)userstack_virt & ~0xFFF, 0, 0x0);
	
	stack--;
	stack--;
	stack--;
	*(--stack) = 0x23;
	*(--stack) = userstack - paramcount * 4 - 4;
	*(--stack) = 0x232;
	*(--stack) = 0x1b;
	*(--stack) = entry;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	x86thread->kernelstack = (uintptr_t)stack;
	*(--stack) = (uint32_t)oldstack;
	*(--stack) = thread->timeout;
	*(--stack) = thread->status;
	
	// Clean up
	halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
}
void halThreadRemoveFunctionCall(Thread *thread)
{
	X86Thread *x86thread = (X86Thread*)thread;
	uint32_t *stack = (uint32_t*)x86thread->kernelstack;
	stack-= 3;
	x86thread->kernelstack = stack[2];
	thread->status = stack[0];
	thread->timeout = stack[1];
}

void halScheduleThread(Thread *thread)
{
	if (!thread)
	{
		thread = &idle_thread->thread;
	}
	
	X86Thread *oldthread = current_thread;
	current_thread = (X86Thread*)thread;
	X86Task *x86task = (X86Task*)thread->task;
	
	// Don't change page directory if not necessary
	if (oldthread && oldthread->thread.task && (oldthread->thread.task == &x86task->task)) return;
	if (!x86task) return;
	
	// Switch to new page directory
	asm volatile("mov %0, %%eax\n"
		"mov %%eax, %%cr3\n" :: "m" (x86task->pagedir));
}

void halThreadSetParam(Thread *thread, uint8_t index, uint32_t value)
{
	X86Thread *x86thread = (X86Thread*)thread;
	IntStackFrame *isf = (void*)x86thread->kernelstack;
	switch (index)
	{
		case 0:
			isf->eax = value;
			break;
		case 1:
			isf->ebx = value;
			break;
		case 2:
			isf->ecx = value;
			break;
		case 3:
			isf->edx = value;
			break;
		case 4:
			isf->esi = value;
			break;
		case 5:
			isf->edi = value;
			break;
		default:
			break;
	}
}
uint32_t halThreadGetParam(Thread *thread, uint8_t index)
{
	X86Thread *x86thread = (X86Thread*)thread;
	IntStackFrame *isf = (void*)x86thread->kernelstack;
	switch (index)
	{
		case 0:
			return isf->eax;
		case 1:
			return isf->ebx;
		case 2:
			return isf->ecx;
		case 3:
			return isf->edx;
		case 4:
			return isf->esi;
		case 5:
			return isf->edi;
		default:
			return 0;
	}
}

