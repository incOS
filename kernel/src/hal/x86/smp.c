
#include "hal/x86/smp.h"
#include "hal/x86/paging.h"
#include "hal/x86/cpu.h"
#include "hal/x86/apic.h"
#include "hal/x86/pic.h"
#include "sys/debug.h"

void halSMPEntry(void)
{
	halInitPIC2();
	char *vidmem = (char*)0xE00B8000;
	vidmem += 80 * 2 * 24;
	char text[] = {'2', 0x07, 'n', 0x07, 'd', 0x07, ' ', 0x07, 'C', 0x07, 'P', 0x07, 'U', 0x07 };
	rtlCopyMemory(vidmem, text, 7 * 2);
	while (1)
	{
		vidmem[1]++;
	}
}

void halInitSMP(void)
{
	// Find smp info
	FloatingPointerStruct *fps = findFloatingPointerStruct();
	if (!fps)
	{
		sysDbgPrint("No SMP!\n");
		return;
	}
	else
	{
		sysDbgPrint("SMP!\n");
		sysDbgPrint("Configuration table: %X\n", fps->configtable);
		if (fps->configtable == 0)
		{
			sysDbgPrint("Error: Default SMP configurations not supported.\n");
			halUnmapPage(kernel_directory, (uint32_t)fps & ~0xFFF);
			return;
		}
		
		// Load configuration table
		SMPConfigTable *config = (SMPConfigTable*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
		halMapPage(kernel_directory, (uint32_t)config, fps->configtable & ~0xFFF, 0x3);
		config = (SMPConfigTable*)((char*)config + (fps->configtable & 0xFFF));
		
		sysDbgPrint("Table entries: %d\n", config->entrycount);
		
		// TODO: This still can crash when we step upon the next page!
		// TODO: Save information
		uint8_t *entry = (uint8_t*)config + sizeof(SMPConfigTable);
		uint32_t i;
		for (i = 0; i < config->entrycount; i++)
		{
			switch (*entry)
			{
				case 0:
				{
					// CPU
					SMPProcessorEntry *processor = (SMPProcessorEntry*)entry;
					if (processor->flags & SMP_PROCESSOR_ENABLED)
					{
						sysDbgPrint("Processor.\n");
					}
					entry = entry + 20;
					break;
				}
				default:
					entry = entry + 8;
			}
		}
		
		// Start processors
		extern void *_binary_smpstart_o_start;
		extern void *_binary_smpstart_o_end;
		
		halMapPage(kernel_directory, 0x2000, 0x2000, 0x3);
		
		sysDbgPrint("start: %x, end: %x\n", &_binary_smpstart_o_start, &_binary_smpstart_o_end);
		sysDbgPrint("Copying %d bytes from %x to %x\n", (uintptr_t)&_binary_smpstart_o_end - (uintptr_t)&_binary_smpstart_o_start, &_binary_smpstart_o_start, 0x2000);
		rtlCopyMemory((void*)0x2000, &_binary_smpstart_o_start, (uintptr_t)&_binary_smpstart_o_end - (uintptr_t)&_binary_smpstart_o_start);
		sysDbgPrint("Copied memory.\n");
		
		*((volatile void**)0x2003) = halSMPEntry;
		*((volatile void**)0x2007) = (uintptr_t*)((uintptr_t)kernel_directory - 0xE0000000);
		
		halAPICWriteStartup(0x2000);
		for (i = 0; i < 100000000; i++)
		{
			*((volatile void**)0x2003) = halSMPEntry;
		}
		halAPICWriteStartup(0x2000);
		//halAPICWriteStartup(0x2000);
		
		// Clean up
		halUnmapPage(kernel_directory, (uint32_t)config & ~0xFFF);
		halUnmapPage(kernel_directory, (uint32_t)fps & ~0xFFF);
	}
}

static uintptr_t findFloatingPointerStructRange(uintptr_t addr, uintptr_t size)
{
	// Map memory
	halMapPageRange(kernel_directory, addr & ~0xFFF, addr & ~0xFFF, 0x3, ((addr & 0xFFF) + size) / 4096);
	// Loop through possible addresses
	uintptr_t currentaddr = addr;
	currentaddr = ((currentaddr + 15) / 16) * 16;
	uintptr_t fps_phys = 0;
	while (currentaddr <= (addr + size - 16))
	{
		FloatingPointerStruct *currentfps = (FloatingPointerStruct*)currentaddr;
		// Signature
		if (!rtlCompareMemory(currentfps->signature, "_MP_", 4))
		{
			// Checksum
			uint32_t i;
			uint8_t checksum = 0;
			for (i = 0; i < 16; i++)
			{
				checksum += ((char*)currentaddr)[i];
			}
			if (checksum == 0)
			{
				fps_phys = (uintptr_t)currentfps;
				break;
			}
		}
		currentaddr += 16;
	}
	
	// Unmap memory
	halUnmapPageRange(kernel_directory, addr & ~0xFFF, ((addr & 0xFFF) + size) / 4096);
	
	return fps_phys;
}

FloatingPointerStruct *findFloatingPointerStruct(void)
{
	// Find EBDA address
	halMapPage(kernel_directory, 0x1000, 0x0, 0x3);
	uint16_t ebdasegment = *(uint16_t*)(0x1400 + 0x0E);
	halUnmapPage(kernel_directory, 0x1000);
	sysDbgPrint("EDBA: 0x%X\n", ((uint32_t)ebdasegment) << 4);
	
	uintptr_t fps_phys = 0;
	if (ebdasegment)
	{
		// Search in EDBA
		fps_phys = findFloatingPointerStructRange(((uint32_t)ebdasegment) << 4, 1024);
	}
	else
	{
		// Search in last kB of main memory
		fps_phys = findFloatingPointerStructRange(0x9FC00, 1024);
	}
	if (!fps_phys)
	{
		// Search in BIOS area
		fps_phys = findFloatingPointerStructRange(0xE0000, 0x20000);
	}
	if (!fps_phys) return 0;
	
	FloatingPointerStruct *fps = (FloatingPointerStruct*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)fps, fps_phys & ~0xFFF, 0x3);
	fps = (FloatingPointerStruct*)((char*)fps + (fps_phys & 0xFFF));
	return fps;
}

