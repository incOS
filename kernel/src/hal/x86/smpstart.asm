
[BITS 16]
ORG 0x2000

jmp start

; Entry function
entry:
	dd 0
; Page directory
page_directory:
	dd 0

start:
	mov ax, 0xB800
	mov ds, ax
	mov di, 0x400
	mov word [ds:di], 0xFFFF
	
	mov ax, 0x0
	mov ds, ax
	lgdt [gdtptr]
	mov eax, cr0
	or eax, 1
	mov cr0, eax
	
	; Enter protected mode
	jmp 0x08:pm
[BITS 32]
pm:
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	
	mov esp, 0x3000
	
	mov ecx, 0x1
	mov ecx, 240
	mov edi, 0xB8300
paintscreen:
	mov word [edi], 0xFFFF
	add edi, 2
	loop paintscreen
	
	; Activate paging
	mov ecx, 0x2
	mov eax, [page_directory]
	mov cr3, eax
	mov eax, cr0
	or  eax, 0x80000000
	mov cr0, eax
	
	mov ecx, 0x3
	mov eax, [entry]
	jmp eax

; GDT for startup
gdtptr:
	dw gdtend - gdt - 1
	dd gdt

gdt:
	dd 0x00000000, 0x00000000
	dd 0x0000ffff, 0x00cf9a00
	dd 0x0000ffff, 0x00cf9200
gdtend:

