
[section .setup]
; Multiboot info
MULTIBOOT_PAGE_ALIGN	equ 1<<0
MULTIBOOT_MEMORY_INFO	equ 1<<1
MULTIBOOT_HEADER_MAGIC	equ 0x1BADB002
MULTIBOOT_HEADER_FLAGS	equ MULTIBOOT_PAGE_ALIGN | MULTIBOOT_MEMORY_INFO
MULTIBOOT_CHECKSUM	equ -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)

[BITS 32]
; Kernel entry
[global start]
; Function to reload GDT
[global halFlushGDT]
[global startup_page_table]
[global kernel_stack]
; C function to be called
[extern halInit]
[extern gp]

; Multiboot header
ALIGN 4
multiboot_header:
	dd MULTIBOOT_HEADER_MAGIC
	dd MULTIBOOT_HEADER_FLAGS
	dd MULTIBOOT_CHECKSUM

; Kernel entry
start:
	mov ecx, 240
	mov edi, 0xB8000
paintscreen:
	mov word [edi], 0xFFFF
	add edi, 2
	loop paintscreen

	; Fill page table
	mov edi, startup_page_table
	mov eax, 0x3
	mov ecx, 1024
setptab:
	mov dword [edi], eax
	add edi, 4
	add eax, 0x1000
	loop setptab
	mov dword [startup_page_table], 0
	
	; Map table into directory
	mov eax, startup_page_table
	or eax, 0x3
	mov edi, page_directory
	mov dword [edi], eax
	add edi, 3584
	mov dword [edi], eax
	
	; Activate paging
	mov eax, page_directory
	mov cr3, eax
	mov eax, cr0
	or  eax, 0x80000001
	mov cr0, eax
	
	jmp high
	
[section .text]
high:
	; Setup stack
	mov esp, kernel_stack
	; Call C kernel
	push dword 0
	push dword 0
	mov ebp, esp
	push dword ebx
	push dword eax
	call halInit
	cli
	hlt

; Sets a new GDT
halFlushGDT:
	lgdt [gp]
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	jmp 0x08:halFlushGDT2
halFlushGDT2:
	ret

; This needs to have low addresses
[section .setup]
; Paging data
ALIGN 4096
page_directory:
%rep 64
dd 0
%endrep

ALIGN 4096
startup_page_table:
%rep 1024
dd 0
%endrep

[section .bss]
; Kernel stack
resb 0x4000
kernel_stack:

