/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/elf.h"
#include "sys/debug.h"
#include "hal/paging.h"

int sysElfIsValid(uintptr_t paddr, uintptr_t size)
{
	// TODO
	return 1;
}
uintptr_t sysElfMapProgram(Task *task, uintptr_t paddr, uintptr_t size)
{
	uintptr_t start = paddr & ~0xFFF;
	uintptr_t end = (paddr + size + 4095) & ~0xFFF;
	sysDbgPrint("sysElfMapProgram (%X - %X).\n", start, end);
	uint32_t pagecount = (end - start) / 4096;
	
	// Load data into kernel address space
	char *data = halReserveKernelPages(pagecount);
	uint32_t i;
	for (i = 0; i < pagecount; i++)
	{
		halMapKernelPage(data + i * 4096, start + i * 4096);
	}
	data += paddr & 0xFFF;
	
	// Load program info
	ElfHeader header;
	rtlCopyMemory(&header, data, sizeof(ElfHeader));
	
	// Load sections into task address space
	ElfSectionHeader *sheaders = (ElfSectionHeader*)(data + header.e_shoff);
	sysDbgPrint("%d headers.\n", header.e_shnum);
	for (i = 0; i < header.e_shnum; i++)
	{
		if (sheaders[i].sh_flags & SHF_ALLOC)
		{
			// FIXME: Totally broken.
			if (sheaders[i].sh_type == SHT_NOBITS)
				rtlZeroMemory(data + sheaders[i].sh_offset, sheaders[i].sh_size);
			
			uintptr_t secstart = sheaders[i].sh_addr & ~0xFFF;
			uintptr_t secend = (sheaders[i].sh_addr + sheaders[i].sh_size + 4095) & ~0xFFF;
			uintptr_t secpagecount = (secend - secstart) / 4096;
			uint32_t j;
			for (j = 0; j < secpagecount; j++)
			{
				halMapTaskPage(task, (sheaders[i].sh_addr & ~0xFFF) + j * 4096, ((paddr + sheaders[i].sh_offset) & ~0xFFF) + j * 4096);
			}
		}
	}
	
	// Remove data from kernel address space
	data = (void*)((uintptr_t)data & ~0xFFF);
	for (i = 0; i < pagecount; i++)
	{
		halUnmapKernelPage(data + i * 4096);
	}
	
	return header.e_entry;
}

