/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/debug.h"
#include "hal/ports.h"

#define SERIAL 0x3f8

void sysDbgPrintList(const char *fmt, int ** args);

static char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";
static char digits_cap[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

void sysDbgInit(void)
{
	halPortWriteByte(SERIAL + 1, 0); // Disable interrupts
	halPortWriteByte(SERIAL + 3, 0x80); // Set baud rate
	halPortWriteByte(SERIAL, 0x3); // High byte, 38400 baud
	halPortWriteByte(SERIAL + 1, 0); // Low byte
	halPortWriteByte(SERIAL + 3, 0x3); // 8 bits, no parity bit, one stop bit
	halPortWriteByte(SERIAL + 2, 0xC7); // Enable FIFO
	halPortWriteByte(SERIAL + 4, 0x0B); // Enable interrupts
}

void sysDbgPrintChar(char c)
{
	while ((halPortReadByte(SERIAL + 5) & 0x20) == 0);
	halPortWriteByte(SERIAL, c);
}
void sysDbgPrintString(const char *s)
{
	while (*s)
	{
		sysDbgPrintChar(*s);
		s++;
	}
}

void sysDbgPrintInt(int32_t value, uint32_t base, int capital_letters)
{
	if (value < 0) {
		sysDbgPrintChar('-');
		value = -value;
	}
	if (value >= (int32_t)base) {
		sysDbgPrintInt(value / base, base, capital_letters);
	}
	int digit = value % base;
	if (capital_letters) {
		sysDbgPrintChar(digits_cap[digit]);
	} else {
		sysDbgPrintChar(digits[digit]);
	}
}
void sysDbgPrintUInt(uint32_t value, uint32_t base, int capital_letters)
{
	if (value >= base) {
		sysDbgPrintInt(value / base, base, capital_letters);
	}
	int digit = value % base;
	if (capital_letters) {
		sysDbgPrintChar(digits_cap[digit]);
	} else {
		sysDbgPrintChar(digits[digit]);
	}
}

void sysDbgPrint(const char *fmt, ...)
{
	int *args = ((int*)&fmt) + 1;
	sysDbgPrintList(fmt, &args);
}
void sysDbgPrintList(const char *fmt, int **args)
{
	while (*fmt) {
		switch (*fmt)
		{
			case '%':
				switch(*(fmt+1))
				{
					case '%':
						sysDbgPrintChar('%');
						break;
					case 'c':
						sysDbgPrintChar(*((char*)(*args)));
						(*args)++;
						break;
					case 'd':
					case 'i':
						sysDbgPrintInt(*((int32_t*)(*args)), 10, 0);
						(*args)++;
						break;
					case 'u':
						sysDbgPrintUInt(*((uint32_t*)(*args)), 10, 0);
						(*args)++;
						break;
					case 'x':
						sysDbgPrintUInt(*((uint32_t*)(*args)), 16, 0);
						(*args)++;
						break;
					case 'X':
						sysDbgPrintUInt(*((uint32_t*)(*args)), 16, 1);
						(*args)++;
						break;
					case 's':
						sysDbgPrintString(*((char**)(*args)));
						(*args)++;
						break;
				}
				fmt++;
				break;
			case '\n':
				sysDbgPrintChar('\n');
				break;
			case '\t':
				sysDbgPrintChar('\t');
				break;
			default:
				sysDbgPrintChar(*fmt);
				break;
		}
		fmt++;
	}
}

