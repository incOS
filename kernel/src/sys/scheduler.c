/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/scheduler.h"
#include "sys/debug.h"
#include "sys/time.h"
#include "sys/syscall.h"

uint32_t threadcount = 0;
Thread **threads = 0;

uint32_t currentthread = 0;


void sysAddThread(Thread *thread)
{
	threadcount++;
	threads = rtlRealloc(threads, threadcount * sizeof(Thread*));
	threads[threadcount - 1] = thread;
}
void sysRemoveThread(Thread *thread)
{
	if (threadcount <= 1)
	{
		sysDbgPrint("Error: No thread left!\n");
		while(1);
	}
	// Search for thread
	uint32_t i;
	for (i = 0; i < threadcount; i++)
	{
		if (threads[i] == thread)
		{
			// Schedule out of thread
			if (i == currentthread)
			{
				threads[i]->status = THREAD_STATUS_SLEEP;
				threads[i]->timeout = 0xFFFFFFFF;
				sysSchedule();
			}
			// Delete thread from list
			if (currentthread > i) currentthread--;
			for (; i < threadcount - 1; i++)
			{
				threads[i] = threads[i + 1];
			}
			threadcount--;
			threads = rtlRealloc(threads, threadcount * sizeof(Thread*));
			return;
		}
	}
}

void sysSchedule(void)
{
	if (threadcount > 0)
	{
		uint32_t threads_passed = 0;
		while (1)
		{
			currentthread++;
			currentthread %= threadcount;
			if (threads[currentthread]->status == THREAD_STATUS_RUNNING)
			{
				break;
			}
			else
			{
				if (threads[currentthread]->timeout <= sysGetTime())
				{
					threads[currentthread]->status = THREAD_STATUS_RUNNING;
					threads[currentthread]->timeout = 0;
					break;
				}
				if (threads[currentthread]->status == THREAD_STATUS_WAITRPCTARGET)
				{
					Thread *target = sysGetRPCTarget(halThreadGetParam(threads[currentthread], 1));
					if (target)
					{
						sysSendRPC(threads[currentthread], target);
						return;
					}
				}
			}
			threads_passed++;
			if (threads_passed >= threadcount)
			{
				currentthread = 0xFFFFFFFF;
				halScheduleThread(0);
				return;
			}
		}
		halScheduleThread(threads[currentthread]);
	}
}

