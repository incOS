/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/start.h"
#include "hal/task.h"
#include "hal/thread.h"
#include "hal/ports.h"
#include "hal/syscall.h"
#include "hal/paging.h"
#include "hal/phys.h"
#include "sys/scheduler.h"
#include "sys/debug.h"
#include "sys/elf.h"
#include "sys/time.h"

char *spinner = "-/|\\";
int spinnerindex = 0;

void timerirq(void)
{
	unsigned char *vidmem = (unsigned char*)0xE00B8000;
	spinnerindex++;
	vidmem[0] = spinner[(spinnerindex / 10) % 4];
	vidmem[1] = 0x7;
	sysTick(10);
	sysSchedule();
}

void sysStart(ModuleInfo *info)
{
	sysDbgPrint("\n");
	sysDbgPrint("incOS 0.0.1 starting.\n");
	sysDbgPrint("=====================\n");
	sysDbgPrint("\n");
	// Initialize timer
	halRegisterIRQ(0, timerirq);
	uint16_t divisor = (uint16_t)(1193180 / 100);
	halPortWriteByte(0x43, 0x34);
	halPortWriteByte(0x40, divisor & 0xFF);
	halPortWriteByte(0x40, divisor >> 8);
	
	// Register syscalls
	// TODO
	
	// Create init task
	Task *init = halCreateTask();
	// Map program
	if (!sysElfIsValid(info->modules[0].modulestart, info->modules[0].modulesize))
	{
		sysDbgPrint("Error: Module not valid.\n");
		while (1);
	}
	uintptr_t entry = sysElfMapProgram(init, info->modules[0].modulestart, info->modules[0].modulesize);
	
	// Create list for programs
	uintptr_t proginfo_phys = halAllocPhysPage();
	uintptr_t proginfo_task = halReserveTaskPages(init, 1);
	uintptr_t *proginfo_kernel = (uintptr_t*)halReserveKernelPages(1);
	halMapKernelPage(proginfo_kernel, proginfo_phys);
	halMapTaskPage(init, proginfo_task, proginfo_phys);
	proginfo_kernel[0] = info->modulecount;
	
	uintptr_t progpath_phys = halAllocPhysPage();
	uintptr_t progpath_task = halReserveTaskPages(init, 1);
	char *progpath_kernel = (char*)halReserveKernelPages(1);
	halMapKernelPage(progpath_kernel, progpath_phys);
	halMapTaskPage(init, progpath_task, progpath_phys);
	
	// Map other programs
	uint32_t i;
	for (i = 0; i < info->modulecount; i++)
	{
		uintptr_t start = info->modules[i].modulestart & ~0xFFF;
		uintptr_t end = (info->modules[i].modulestart + info->modules[i].modulesize + 4095) & ~0xFFF;
		sysDbgPrint("Mapping additional program (%X - %X).\n", start, end);
		sysDbgPrint("Program path: %s\n", info->modules[i].path);
		uint32_t pagecount = (end - start) / 4096;
		
		uintptr_t vaddr = halReserveTaskPages(init, pagecount);
		uint32_t j;
		for (j = 0; j < pagecount; j++)
		{
			halMapTaskPage(init, vaddr + j * 4096, start + j * 4096);
		}
		rtlCopyMemory(progpath_kernel + i * 64, info->modules[i].path, 64);
		
		// Set list entry
		proginfo_kernel[1 + i * 3] = vaddr + (info->modules[i].modulestart & 0xFFF);
		proginfo_kernel[2 + i * 3] = info->modules[i].modulesize;
		proginfo_kernel[3 + i * 3] = progpath_task + i * 64;
	}
	
	// Start program
	Thread *initthread = halCreateThread(init, entry, 1, proginfo_task);
	sysAddThread(initthread);
	
	halUnmapKernelPage(proginfo_kernel);
	halUnmapKernelPage(progpath_kernel);
	
	sysDbgPrint("Enabling interrupts.\n");
	asm("sti");
	
	unsigned short *vidmem = (unsigned short*)0xE00B8000;
	vidmem[0] = 0x700;
	while (1);
}

