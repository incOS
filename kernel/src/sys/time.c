
#include "sys/time.h"

uint32_t currenttime = 0;

void sysTick(uint32_t msecs)
{
	currenttime += msecs;
}
uint32_t sysGetTime(void)
{
	return currenttime;
}

