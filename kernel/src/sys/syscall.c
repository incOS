/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/syscall.h"
#include "sys/debug.h"
#include "hal/paging.h"
#include "sys/scheduler.h"
#include "hal/thread.h"
#include "sys/time.h"
#include "hal/phys.h"
#include "hal/syscall.h"

void sysSyscall(Thread *thread, uintptr_t index, uintptr_t *param1,
	uintptr_t *param2, uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	//sysDbgPrint("Syscall: %d\n", index);
	switch (index)
	{
		case SYSCALL_EXIT:
			{
				Task *task = thread->task;
				while (task->threadcount > 0)
				{
					Thread *thread0 = task->threads[0];
					sysRemoveThread(thread0);
					halDestroyThread(thread0);
				}
				halDestroyTask(task);
			}
			break;

		case SYSCALL_CREATE_THREAD:
			{
				Thread *newthread = halCreateThread(thread->task, *param1, 0);
				sysAddThread(newthread);
			}
			break;
		case SYSCALL_STOP_THREAD:
			{
				Task *task = thread->task;
				sysRemoveThread(thread);
				halDestroyThread(thread);
				if (task->threadcount == 0)
				{
					halDestroyTask(task);
				}
			}
			break;

		case SYSCALL_CREATE_TASK:
			*param1 = sysCreateTask();
			break;
		case SYSCALL_MAP_TASK_MEMORY:
			{
				int id = *param1;
				uintptr_t src = *param2;
				uintptr_t dest = *param3;
				uint32_t count = *param4;
				*param1 = sysMapTaskMemory(thread, id, src, dest, count);
			}
			break;
		case SYSCALL_START_TASK:
			*param1 = sysStartTask(*param1, *param2);
			break;
		case SYSCALL_CLONE_TASK:
			*param1 = sysCloneTask(*param1, *param2, *param3);
			break;
		case SYSCALL_GET_TASK_INFO:
			*param1 = thread->task->id;
			*param2 = thread->id;
			break;

		case SYSCALL_SLEEP:
			thread->status = THREAD_STATUS_SLEEP;
			thread->timeout = sysGetTime() + *param1;
			sysSchedule();
			break;

		case SYSCALL_WAIT_FOR_RPC:
			thread->status = THREAD_STATUS_WAITRPC;
			if (!*param1)
			{
				thread->timeout = 0xFFFFFFFF;
			}
			else
			{
				if ((uint64_t)sysGetTime() + *param1 > 0xFFFFFFFF)
					thread->timeout = 0xFFFFFFFF;
				else
					thread->timeout = sysGetTime() + *param1;
			}
			sysSchedule();
			break;
		case SYSCALL_SEND_RPC:
			{
				// Set timeout first
				if ((uint64_t)sysGetTime() + *param2 > 0xFFFFFFFF)
					thread->timeout = 0xFFFFFFFF;
				else
					thread->timeout = sysGetTime() + *param2;
				// Search for task
				Thread *target = sysGetRPCTarget(*param1);
				if (!target)
				{
					thread->status = THREAD_STATUS_WAITRPCTARGET;
					sysSchedule();
					break;
				}
				sysSendRPC(thread, target);
				sysSchedule();
			}
			break;
		case SYSCALL_RPC_ENDED:
			halThreadRemoveFunctionCall(thread);
			if ((thread->rpccaller) && (thread->rpccaller->status == THREAD_STATUS_WAITRPCANSWER))
			{
				// Set return value
				halThreadSetParam(thread->rpccaller, 2, *param1);
				halThreadSetParam(thread->rpccaller, 1, 1);
				// Wake up thread
				thread->rpccaller->status = THREAD_STATUS_RUNNING;
				thread->rpccaller->timeout = 0;
				thread->rpccaller = 0;
			}
			sysSchedule();
			break;
		case SYSCALL_IRQ_ENDED:
			halThreadRemoveFunctionCall(thread);
			break;
		case SYSCALL_REGISTER_RPC_HANDLER:
			thread->rpchandler = *param1;
			break;
		case SYSCALL_REGISTER_IRQ_HANDLER:
			sysDbgPrint("SYSCALL_REGISTER_IRQ_HANDLER: %x => %x\n", *param1, *param2);
			halRegisterUserIRQ(*param1, thread, *param2);
			*param1 = 1;
			break;

		case SYSCALL_ALLOC_MEMORY:
			*param1 = sysAllocMemory(thread->task, *param1, param2, param3, param4);
			break;
		case SYSCALL_FREE_MEMORY:
			*param1 = sysFreeMemory(thread->task, *param1, *param2);
			break;

		case SYSCALL_DEBUG:
			sysDbgPrint("%s", *param1);
			break;
		
		case SYSCALL_42:
			*param1 = 42;
			break;
		
		default:
			break;
	}
}

uint32_t sysAllocMemory(Task *task, uint32_t flags, uintptr_t *paddr, uintptr_t *vaddr, uint32_t *size)
{
	if ((flags & 0x2) == 0)
	{
		*vaddr = halReserveTaskPages(task, *size);
		if (!*vaddr) return 0;
	}
	if (flags & 0x1)
	{
		// TODO: Check that only special or unused pages are allocated
		// Map physical pages
		uint32_t i;
		for (i = 0; i < *size; i++)
		{
			halMapTaskPage(task, *vaddr + i * 4096, (*paddr & ~0xFFF) + i * 4096);
		}
		return 1;
	}
	else if (flags & 0x10)
	{
		// Allocate DMA memory
		*paddr = halAllocPhysPagesDMA(*size);
		uint32_t i;
		for (i = 0; i < *size; i++)
		{
			halMapTaskPage(task, *vaddr + i * 4096, (*paddr & ~0xFFF) + i * 4096);
		}
		return 1;
	}
	else if (flags & 0x8)
	{
		if (flags & 0x4)
		{
			// TODO: Allocate low memory in one piece
		}
		else
		{
			// Map pages
			uint32_t i;
			for (i = 0; i < *size; i++)
			{
				uintptr_t page = halGetFreePhysPageLow();
				if (!i) *paddr = page;
				halMapTaskPage(task, *vaddr + i * 4096, page);
			}
		}
		return 0;
	}
	else
	{
		// Map pages
		uint32_t i;
		for (i = 0; i < *size; i++)
		{
			uintptr_t page = halGetFreePhysPage();
			halMapTaskPage(task, *vaddr + i * 4096, page);
		}
		return 1;
	}
}
uint32_t sysFreeMemory(Task *task, uintptr_t vaddr, uint32_t size)
{
	// Unmap pages
	uint32_t i;
	for (i = 0; i < size; i++)
	{
		uintptr_t page = halTaskGetPhysPage(task, vaddr + i * 4096);
		halDropPage(page / 0x1000);
		halUnmapTaskPage(task, vaddr + i * 4096);
	}
	return 1;
}

int sysCreateTask(void)
{
	Task *task = halCreateTask();
	return task->id;
}
int sysCloneTask(uint32_t id, uint32_t thread, uintptr_t entry)
{
	sysDbgPrint("sysCloneTask\n");
	Task *task = halGetTask(id);
	if (!task) return 0;
	Task *newtask = halCloneTask(task, thread, entry);
	if (!newtask) return 0;
	sysAddThread(newtask->threads[0]);
	return newtask->id;
}
int sysStartTask(int id, uintptr_t entry)
{
	Task *task = halGetTask(id);
	if (!task) return 0;
	
	Thread *thread = halCreateThread(task, entry, 0);
	sysAddThread(thread);
	return 1;
}
int sysMapTaskMemory(Thread *thread, int id, uintptr_t src, uintptr_t dest, uint32_t count)
{
	Task *task = halGetTask(id);
	if (!task)
	{
		sysDbgPrint("sysMapTaskMemory: Task not found.\n");
		return 0;
	}
	uint32_t i;
	for (i = 0; i < count; i++)
	{
		uintptr_t page = halTaskGetPhysPage(thread->task, src + i * 4096);
		halGrabPage(page / 0x1000);
		halMapTaskPage(task, dest + i * 4096, page);
	}
	
	halTaskGetPhysPage(task, dest);
	
	return 1;
}

Thread *sysGetRPCTarget(uint32_t taskid)
{
	// Get task
	Task *task = halGetTask(taskid);
	if (!task) return 0;
	
	// Look for waiting thread
	uint32_t i;
	for (i = 0; i < task->threadcount; i++)
	{
		if ((task->threads[i]->status == THREAD_STATUS_WAITRPC) && (task->threads[i]->rpchandler != 0))
		{
			return task->threads[i];
		}
	}
	
	return 0;
}

void sysSendRPC(Thread *source, Thread *target)
{
	// Map RPC memory
	uintptr_t addr = halThreadGetParam(source, 4);
	uintptr_t size = halThreadGetParam(source, 5);
	if ((addr != 0) && (size != 0))
	{
		uint32_t pagecount = (size + 4095) / 0x1000;
		uintptr_t destaddr = halReserveTaskPages(target->task, pagecount);
		uint32_t i;
		for (i = 0; i < pagecount; i++)
		{
			uintptr_t page = halTaskGetPhysPage(source->task, addr + i * 4096);
			halGrabPage(page / 0x1000);
			halMapTaskPage(target->task, destaddr + i * 4096, page);
		}
		// Add rpc frame on the other thread
		halThreadCallFunction(target, target->rpchandler, 4, source->task->id, halThreadGetParam(source, 3), destaddr, size);
	}
	else
	{
		// Add rpc frame on the other thread
		halThreadCallFunction(target, target->rpchandler, 4, source->task->id, halThreadGetParam(source, 3), 0, 0);
	}
	// Disable this thread
	source->status = THREAD_STATUS_WAITRPCANSWER;
	halThreadSetParam(source, 1, 2);
	target->status = THREAD_STATUS_RUNNING;
	target->timeout = 0;
	target->rpccaller = source;
}

