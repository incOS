/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "rtl/rtl.h"
#include "hal/paging.h"

void rtlZeroMemory(void *data, int length)
{
	// TODO: Optimize this
	int i;
	for (i = 0; i < length; i++)
	{
		((char*)data)[i] = 0;
	}
}

void rtlSetMemory(void *data, unsigned char value, int length)
{
	// TODO: Optimize this
	int i;
	for (i = 0; i < length; i++)
	{
		((unsigned char*)data)[i] = value;
	}
}
void rtlSetMemoryW(void *data, unsigned short value, int length)
{
	int i;
	for (i = 0; i < length; i++)
	{
		((unsigned short*)data)[i] = value;
	}
}
void rtlCopyMemory(void *dest, void *src, int length)
{
	// TODO: Optimize this
	int i;
	for (i = 0; i < length; i++)
	{
		((unsigned char*)dest)[i] = ((unsigned char*)src)[i];
	}
}
int rtlCompareMemory(void *dest, void *src, int length)
{
	int i;
	for (i = 0; i < length; i++)
	{
		int diff = (int)((unsigned char*)dest)[i] - (int)((unsigned char*)src)[i];
		if (diff)
			return diff;
	}
	return 0;
}

void *rtlAlloc(uint32_t size)
{
	uint32_t *data = halAllocKernelPages((size + 4 + 4095) / 4096);
	data[0] = size;
	return &data[1];
}
void *rtlRealloc(void *data, uint32_t size)
{
	// Free memory
	if (size == 0)
	{
		if (data)
		{
			rtlFree(data);
		}
		return 0;
	}
	// Allocate memory if not already done
	if (!data)
	{
		data = rtlAlloc(size);
		rtlZeroMemory(data, size);
		return data;
	}
	
	uint32_t *tmp = ((uint32_t*)data) - 1;
	uint32_t neededpages = (size + 4 + 4095) / 4096;
	uint32_t reservedpages = (tmp[0] + 4 + 4095) / 4096;
	if (neededpages == reservedpages)
	{
		// Don't change number of allocated pages
		if (size > tmp[0])
		{
			rtlZeroMemory(&((char*)data)[tmp[0]], size - tmp[0]);
		}
		tmp[0] = size + 4;
		return data;
	}
	else
	{
		// Change number of allocated pages
		void *newdata = rtlAlloc(size);
		if (size > tmp[0])
		{
			rtlCopyMemory(newdata, data, tmp[0]);
			rtlZeroMemory(&((char*)newdata)[tmp[0]], size - tmp[0]);
		}
		else
		{
			rtlCopyMemory(newdata, data, size);
		}
		rtlFree(data);
		return newdata;
	}
}
void rtlFree(void *data)
{
	uint32_t *tmp = ((uint32_t*)data) - 1;
	halFreeKernelPages(tmp, (tmp[0] + 4 + 4095) / 4096);
}

