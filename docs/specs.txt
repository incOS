Name:
			IncOS
License:
			GPL/LGPL, MIT

Common Goals:
			readable, well commented Code
			
Specifications:
	Architecture:	MicroKernel, Posix
	Platform:	Multi (HAL)
	
Coding style:
	Columns:         <= 80
	Brackets:        BSD style
	Identation:      Tabs for identation, spaces for formatting
	Types:           SomeStruct, integer from stdint (int8_t, uint32_t, intptr_t)
	Variable names:  lowchar_with_underscores
	Declarations:    on top
	Function names:  camelCase()
	Comments:	 Always over commented code
	
		   One lined: // Starting with high Char
		   Doxygen: 	 /**
			 	   * Doxygen comment (description)
			 	   * can go over multiple lines
			 	   * \param Parameter 1
			 	   * \param Parameter 2
			 	   * \return Return value (if needed)
			 	   */
	
Driver Overview:
	Network:         rtl8139
	Graphics:        VGA
	Input:           Keyboard 
	
Misc Overview:
        Bootloader:      Grub
        GDT:             Own
        
Docs:
	automated:       yes -> Doxygen
	online:          lowlevel-wiki

Project:
        Versionmgmt:     git
        Devel-Environm.: gcc, (fasm?)
        MultiEditing:    gobby (atm foonative.org)


libc:
	List:
		assert
		ctype		nico
		errno
		float
		limits
		locale
		setjmp
		signal
		stdarg
		stddef
		time
		stdint		nico
		stdio 		nico
		stdlib		nico
		math		kai,phil
		string		mathias
		stddef


TODO (global):
	- Subset of libc (kernel)
	- Several ASM-Codes (Switching Pmode / gdt etc)
	- Paging
	- Physical memory manager
	- Virtual memory manager (kernel)
		*MILESTONE v1*


