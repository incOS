# determine the compiler to use for ASM using Intel syntax

SET(ASM_DIALECT "-INTEL")
SET(CMAKE_ASM${ASM_DIALECT}_COMPILER_INIT ${_CMAKE_TOOLCHAIN_PREFIX}nasm)
INCLUDE(CMakeDetermineASMCompiler)
SET(ASM_DIALECT)
