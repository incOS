
cmake_minimum_required(VERSION 2.6)

project(incOSC C ASM-ATT ASM-INTEL)

set(PROGRAMS)
macro(add_program name)
	set(PROGRAMS ${PROGRAMS} ${name} PARENT_SCOPE)
	set(PROGRAMS ${PROGRAMS} ${name})
endmacro(add_program name)

add_subdirectory(kernel/src)
add_subdirectory(programs)

# Image
message(${PROGRAMS})
add_custom_command(OUTPUT build/floppy.img COMMAND sh build/makeimage.sh && echo "Programs:" && echo ${PROGRAMS} DEPENDS inc32.krn ${PROGRAMS})
add_custom_target(image ALL DEPENDS build/floppy.img)

